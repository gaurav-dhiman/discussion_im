import React, { Component } from 'react';
import io from 'socket.io-client';
import feathers from '@feathersjs/feathers';
import socketio from '@feathersjs/socketio-client';
import auth from '@feathersjs/authentication-client';
import service from '../../server/services/definitions';

let isServer = process && process.env && process.env.NODE_ENV && true || false;
let isProd = isServer && process.env.NODE_ENV == 'production' || false;

export const WEB_SERVER_PROTOCOL = !isServer ? window.location.protocol + '//' : 'https://';
export const  WEB_SERVER_HOST = !isServer ? window.location.host : ((isProd) ? 'discussion.im' : 'localhost');

export const API_SERVER_PROTOCOL = !isServer ? window.location.protocol + '//' : 'https://';
export const API_SERVER_HOST =  !isServer ? window.location.host : ((isProd) ? 'discussion.im' : 'localhost');
export const API_SERVER_BASE_URL = API_SERVER_PROTOCOL + API_SERVER_HOST;

export const SKIP_RECORDS = 0;
export const LIMIT_RECORDS = 10;
export { service };

const isLocalhost = API_SERVER_HOST === 'localhost' || API_SERVER_HOST === 'lvh.me' || API_SERVER_HOST.indexOf('192.') === 0 || API_SERVER_HOST.indexOf('10.') === 0;

let clientApp;
export class App extends Component {
  static _isError = obj => obj.type === 'FeathersError';

  constructor(props) {
    super(props);
    if (clientApp) return;
    const options = {
      rejectUnauthorized: (isLocalhost) ? false : true
    };

    clientApp = feathers();
    clientApp.configure(socketio(io(API_SERVER_BASE_URL, options)));
    clientApp.isError = App._isError;
  }

  componentDidMount() {
    if (clientApp && !clientApp.authenticate) {
      clientApp.configure(
        auth({
          service: service.users,
          storage: localStorage
        })
      );
    }
  }

  static get apiInstance() {
    return clientApp;
  }

  render() {
    return <div />;
  }
}

new App();
export default App.apiInstance;
