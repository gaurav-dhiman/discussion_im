import emailTypes from '../../server/services/auth-management/notification-email-types';
import { sendVerifyAccount, hideActionModal } from '../redux/actions';

export function getModalDetails(modalName, data = {}) {
  const { user = {} } = data;
  switch (modalName) {
    case emailTypes.SEND_VERIFY_ACCOUNT:
      return {
        title: 'Verify your email address.',
        message: 'Your account is not yet verified. When you signed-up, an email was sent to you with instructions to verify your account. Click below button to re-send the verification email again.',
        buttons: [
          { label: 'Resend Verification Email', variant: 'raised', action: sendVerifyAccount({ action: emailTypes.SEND_VERIFY_ACCOUNT, value: { email: user.email } }) },
          { label: 'Close', action: hideActionModal() }
        ]
      }
  }
}