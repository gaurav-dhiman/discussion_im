import { showSnackBarNotification } from '../../redux/actions';

function base64UrlToUint8Array(base64UrlData) {
  const padding = '='.repeat((4 - base64UrlData.length % 4) % 4);
  const base64 = (base64UrlData + padding)
    .replace(/\-/g, '+')
    .replace(/_/g, '/');
  const rawData = atob(base64);
  const buffer = new Uint8Array(rawData.length);
  for (let i = 0; i < rawData.length; ++i) {
    buffer[i] = rawData.charCodeAt(i);
  }
  return buffer;
}

export const getPushSubscriptionObj = async (dispatch) => {
  let permission, serviceWorkerRegistration, subscription;
  // check if browser supports notifications
  if (!("Notification" in window)) {
    const mesg = 'Can not subscribe to notifications, as this system / browser does not support web push notification feature.'
    if (dispatch) dispatch(showSnackBarNotification(mesg), { type: 'info' })
    else alert(mesg);
    return null;
  } else if (Notification.permission === "granted") { //check if notification permissions has already been granted
    serviceWorkerRegistration = await navigator.serviceWorker.ready;
    subscription = await serviceWorkerRegistration.pushManager.getSubscription();
    return subscription;
  } else { //prompt user to ask for notification permissions
    permission = await Notification.requestPermission();

    if (permission === "granted") {
      //vapid public key that you generated when creating the push server
      const publicVapidKey = base64UrlToUint8Array(window.publicVapidKey || '');

      serviceWorkerRegistration = await navigator.serviceWorker.ready;
      //create the browser identifier for your notifications feed
      subscription = await serviceWorkerRegistration.pushManager.subscribe({
        userVisibleOnly: true,
        applicationServerKey: publicVapidKey
      });
      return subscription;
    }
  }
}