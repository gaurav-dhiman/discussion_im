import api, { service } from '../api';
import { getPushSubscriptionObj } from './push-subscription';
import { setPushSubscriptionId } from '../../redux/actions';


export const subscribe = async (dispatch, userUUID, subscribeTo) => {
  let subscription = await getPushSubscriptionObj(dispatch);
  if (subscription) {
    subscription = await api.service(service.webPushSubscribe).create({
      subscription,
      subscribeTo,
      userUUID
    });
    dispatch(setPushSubscriptionId(subscription.subscriptionId))
  }

  return subscription;
}