import api, { service } from '../api';
import { getPushSubscriptionObj } from './push-subscription';

export const unsubscribe = async (dispatch, userUUID, unsubscribeFrom) => {
  if (!unsubscribeFrom) return;

  if (userUUID) {
    // if user is logged in, remove all his subscriptions across devices for given entity
    await api.service(service.webPushSubscribe).remove(
      null,
      {
        query: {
          userUUID,
          subscribeTo: unsubscribeFrom
        }
      }
    );
  } else {
    // if user is not logged in, just remove subscription for this device and given entiry.
    const subscription = await getPushSubscriptionObj(dispatch);
    if (subscription)
      await api.service(service.webPushSubscribe).remove(
        null, // ID 
        {
          query: {
            subscription: subscription,
            subscribeTo: unsubscribeFrom
          }
        });
  }
}