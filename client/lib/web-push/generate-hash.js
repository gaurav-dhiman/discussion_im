const hexString = async (buffer) => {
  const byteArray = Array.from(new Uint8Array(buffer));

  const hashHex = byteArray.map(b => ('00' + b.toString(16)).slice(-2)).join('');
  return hashHex;
}

const digestMessage = async (message) => {
  const msgBuffer = await new TextEncoder('utf-8').encode(message);
  return await window.crypto.subtle.digest('SHA-256', msgBuffer);
}

export const sha256Hash = async (text) => {
  const hashBuffer = await digestMessage(text);
  return await hexString(hashBuffer);
}
