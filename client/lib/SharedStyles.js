// These styles are specific to home page.
// TODO: Move these styles to page/home.js
const styleBigAvatar = {
  width: '80px',
  height: '80px',
  margin: '0px auto 15px',

};

const styleRaisedButton = {
  margin: '15px 15px 30px 15px'
};

const styleToolbar = {
  background: '#FFF',
  height: '64px',
  paddingRight: '20px',
};

const styleLoginButton = {
  borderRadius: '2px',
  textTransform: 'none',
  fontWeight: '400',
  letterSpacing: '0.01em',
  color: 'white',
  backgroundColor: '#DF4930',
};

const styleTextField = {
  color: '#222',
  fontWeight: '300',
  fontFamily: '"Roboto", "Helvetica", "Arial", sans-serif'
};

const styleForm = {
  margin: '7% auto',
  width: '360px',
};

const styleGrid = {
  margin: '0px auto',
  color: '#222',
  fontWeight: '300',
  lineHeight: '1.5em',
  fontFamily: '"Roboto", "Helvetica", "Arial", sans-serif'
};

const styleH1 = {
  textAlign: 'center',
  fontWeight: '400',
  lineHeight: '45px',
  fontFamily: '"Roboto", "Helvetica", "Arial", sans-serif'
};

const styleHomepageFeature = {
  textAlign: 'center',
  padding: '10px 25px',
  fontFamily: '"Roboto", "Helvetica", "Arial", sans-serif',
  border: '#424242 1px solid',
  margin: '10px',
  borderTopWidth: '20px'
};

const evenSection = {
  backgroundColor: "#f0f0f0",
  color: "#424242",
  overflow: "auto",
  padding: "35px 0px"
}

const oddSection = {
  backgroundColor: "#424242",
  color: "#f0f0f0",
  overflow: "auto",
  padding: "35px 0px"
}

const mainSection = {
  backgroundColor: "#424242",
  color: "#ffffff",
  overflow: "auto",
  height: "100vh",
  fontFamily: '"Roboto", "Helvetica", "Arial", sans-serif'
}

const withLoveSection = {
  marginTop: '48px',
  marginBottom: '32px',
  color: '#0000008a',
  textAlign: 'center',
  fontSize: '0.875rem',
  fontFamily: '"Roboto", "Helvetica", "Arial", sans-serif',
  fontWeight: '400',
  lineHeight: '1.5',
  letterSpacing: '0.01071em',
}

module.exports = {
  styleBigAvatar,
  styleRaisedButton,
  styleToolbar,
  styleLoginButton,
  styleTextField,
  styleForm,
  styleGrid,
  styleH1,
  styleHomepageFeature,
  evenSection,
  oddSection,
  mainSection,
  withLoveSection
};  