import React, { Component } from 'react';
import { compose } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import withRoot from '../../lib/with-root';
import Dotdotdot from 'react-dotdotdot'

import Typography from '@material-ui/core/Typography';
import Hidden from '@material-ui/core/Hidden';
import SearchableList from '../../components/searchable-list';
import TopicList from '../../components/topic-list';
import Appbar from '../../components/app-bar';
import DiscussionRoom from '../../components/discussion-room';
import TopicThread from '../../components/topic-thread';
import { topicsSelector } from '../../redux/selectors';
import InfoContainer from '../../components/info-container';
import Button from '@material-ui/core/Button';
import SearchIcon from '@material-ui/icons/Search';
import UserProfile from '../../components/user-profile';
import { DRAWER_WIDTH } from '../../components/sliding-drawer';
import Link from 'next/link';

class TopicsScreen extends Component {

  render() {
    const {
      classes,
      topics,
      topic,
      topicMessages,
      topicMessage,
      topicMessageReplies,
      infoUser
    } = this.props;
    const showInfoContainer = !!infoUser || !!topicMessage;

    return (
      <Hidden smDown implementation="css">
        <Appbar>
          <Typography
            className={classes.flex}
            variant="subheading"
            color="inherit"
          >
            <Dotdotdot clamp={1}>{topic.title || 'Discussion Topics'}</Dotdotdot>
          </Typography>

          <Link as={'/topics/explore'} href={'/topics?topicUUID=explore'} prefetch={false} passHref >
            <Button color='secondary' variant="raised" className={classes.button} >
              Add / Explore Topics
              <SearchIcon className={classes.rightIcon} />
            </Button>
          </Link>
        </Appbar>

        <div className={classes.workarea}>
          <div className={classes.toolbar} />
          <div className={classes.mainContainer}>
            <SearchableList
              showSearchBox
              scrollable
              items={topics}
              searchPlaceholder='Search topics in my list...'
              onInfiniteLoad={this._fetchOlderTopics}
            >
              <TopicList topics={topics} />
            </SearchableList>

            <DiscussionRoom topic={topic} topicMessages={topicMessages} />

            {showInfoContainer &&
              <InfoContainer
                drawerOpen={showInfoContainer}
                handleDrawerToggle={this._handleDrawerToggle}
                drawerWidth={DRAWER_WIDTH}
                variant={'persistent'}
                anchor={'right'}
              >
                {!!infoUser && <UserProfile user={infoUser} />}
                {!!topicMessage &&
                  <TopicThread
                    message={topicMessage}
                    replies={topicMessageReplies}
                    invalidTopicMessage='This topic message does not exist. Go to home page to see all topics.'
                  />
                }
              </InfoContainer>
            }

          </div>
        </div>
      </Hidden>
    );
  }
}

const styles = theme => ({
  fab: {
    position: 'fixed',
    bottom: theme.spacing.unit * 2,
    right: theme.spacing.unit * 2
  },
  workarea: {
    display: 'flex',
    flexDirection: 'column',
    height: '100vh'
  },
  mainContainer: {
    flex: '1 1 auto',
    display: 'flex',
    height: `calc(100% - 64px)`
  },
  toolbar: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: '0 8px',
    ...theme.mixins.toolbar
  },
  flex: {
    flex: '1 1 auto'
  },
  drawer: {
    width: DRAWER_WIDTH,
    flexShrink: 0,
  },
  button: {
    margin: theme.spacing.unit,
  },
  rightIcon: {
    marginLeft: theme.spacing.unit,
  }
});

const mapStateToProps = (state, props) => {
  return {
    topics: props.topics || topicsSelector(state),
    topic: props.topic || topicsSelector(state, { uuid: state.currentTopicUUID }),
    topicMessages: props.topicMessages
  };
};

export default compose(
  connect(
    mapStateToProps
  ),
  withRoot,
  withStyles(styles, { withTheme: true })
)(TopicsScreen);
