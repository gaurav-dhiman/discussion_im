import React, { Component } from 'react';
import { compose } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import withRoot from '../../lib/with-root';
import Dotdotdot from 'react-dotdotdot'
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import Appbar from '../../components/app-bar';
import Button from '@material-ui/core/Button';
import StarFilledIcon from '@material-ui/icons/StarOutlined';
import SearchBox from '../../components/search-box';
import TopicCardsGrid from '../../components/topic-cards-grid';
import { showAddTopicModal, getTopics } from '../../redux/actions';
import { topicsSelector } from '../../redux/selectors';
import DeepLink from '../../components/deep-link';

class ExploreTopicsScreen extends Component {
  state = {
    searchText: ''
  };

  _handleSearchTextChange = searchText => {
    this.setState({ ...this.state, searchText });
    this.props.getTopics({
      query: {
        title: { $regex: searchText, $options: 'im' },
        description: { $regex: searchText, $options: 'im' }
      },
      $limit: 1000
    });
  }

  render() {
    const {
      classes,
      openAddTopicModal
    } = this.props;
    const { searchText } = this.state;
    const searchKeywords = searchText.split(' ').map(kw => kw.trim());

    let { topics } = this.props;
    topics = topics.filter(topic => {
      let found = false;
      searchKeywords.forEach((kw) => {
        if (topic.title.toLowerCase().includes(kw.toLowerCase()) || topic.description.toLowerCase().includes(kw.toLowerCase())) found = true;
      });
      return found;
    });

    return (
      <React.Fragment>
        <Appbar >
          <Typography
            className={classes.flex}
            variant="subheading"
            color="inherit"
          >
            <Dotdotdot clamp={1}>Explore Topics</Dotdotdot>
          </Typography>
          <SearchBox searchPlaceholder='Search communities or topics...' handleSearchTextChange={this._handleSearchTextChange} />
        </Appbar>

        <div className={classes.workarea}>
          <div className={classes.toolbar} />
          {!searchText &&
            <Paper className={classes.intro} elevation={0}>
              <Typography variant="subheading" component="h2">
                Search the communities or topics you are interested in and then add them to your list.
            </Typography>
              <Typography variant="subheading" component="h2">
                If you don't find any, then add a new one and start the discussion.
            </Typography>
              <Typography variant="subheading" component="h2">
                Search results will appear below as you type.
            </Typography>

              <div className={classes.filler} />
            </Paper>
          }

          {!!searchText &&
            <React.Fragment>
              <Paper className={classes.intro} elevation={0}>
                <Typography variant="subheading" component="h2">
                  Star (<StarFilledIcon fontSize="small" className={classes.iconSmall} />) the topic to add it to your list of interested topics.
                </Typography>
                <div className={classes.filler} />
              </Paper>

              <div style={{ height: '100%' }}>
                {!!topics.length &&
                  <TopicCardsGrid topics={topics} />
                }

                <Paper className={classes.intro} elevation={0}>
                  {!topics.length &&
                    <Typography variant="headline" component="h2">
                      No Topic found. Please add a new topic.
                    </Typography>
                  }
                </Paper>

                <div className={classes.buttonsBlock}>
                  <Button variant="contained" size='large' color='primary' className={classes.button} onClick={() => openAddTopicModal(searchText)}>
                    Add New Topic
                  </Button>
                  <DeepLink type='topic'>
                    <Button variant="contained" size='large' color='primary' className={classes.button} >
                      Show My Topics
                    </Button>
                  </DeepLink>
                </div>
              </div>
            </React.Fragment>
          }
        </div>
      </React.Fragment>
    );
  }
}

const styles = theme => ({
  intro: {
    ...theme.mixins.gutters(),
    paddingTop: theme.spacing.unit * 2,
    paddingBottom: theme.spacing.unit * 2,
    backgroundColor: 'unset',
    textAlign: 'center'
  },
  buttonsBlock: {
    display: 'flex',
    justifyContent: 'center'
  },
  button: {
    margin: '10px'
  },
  iconSmall: {
    fontSize: 12,
  },
  workarea: {
    display: 'flex',
    flexDirection: 'column',
    height: '100vh',
    overflowY: 'auto'
  },
  toolbar: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: '0 8px',
    ...theme.mixins.toolbar
  },
  flex: {
    flex: '1 1 auto'
  },
  filler: {
    height: '20px'
  }
});

const mapStateToProps = (state, props) => {
  let topics = topicsSelector(state);
  return {
    topics
  };
};

const mapDispatchToProps = dispatch => {
  return {
    openAddTopicModal: (searchText) => dispatch(showAddTopicModal(searchText)),
    getTopics: (queryObj, options) => dispatch(getTopics(queryObj, options))
  };
};

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  withRoot,
  withStyles(styles, { withTheme: true })
)(ExploreTopicsScreen);
