import React, { Component } from 'react';
import { compose } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import withRoot from '../../lib/with-root';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import Hidden from '@material-ui/core/Hidden';
import IconButton from '@material-ui/core/IconButton';
import EditIcon from '@material-ui/icons/Edit';
import CancelIcon from '@material-ui/icons/Clear';
import UserProfile from '../../components/user-profile';
import Appbar from '../../components/app-bar';

class UserProfileScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showEditUserProfileForm: false
    };
  }

  _handleTextChange = name => event => {
    this.setState({
      [name]: event.target.value
    });
  };

  _editUserProfile = () => {
    this.setState({ showEditUserProfileForm: true });
  }

  _cancelEdit = () => {
    this.setState({ showEditUserProfileForm: false });
  }

  render() {
    const { classes, user, loggedInUserUUID } = this.props;
    const { showEditUserProfileForm } = this.state;
    const showEditButton = user && (user.uuid === loggedInUserUUID) && !showEditUserProfileForm;
    const showCancelButton = user && (user.uuid === loggedInUserUUID) && showEditUserProfileForm;

    return (
      <Hidden mdUp implementation="css">
        <div className={classes.workarea}>
          <Appbar>
            <Typography
              className={classes.flex}
              variant="subheading"
              color="inherit"
            >
              User Profile
            </Typography>

            {showEditButton &&
              <IconButton
                className={classes.rightMostAppBarButton}
                color="inherit"
                aria-label="Edit Profile"
                onClick={this._editUserProfile}
              >
                <EditIcon />
              </IconButton>
            }

            {showCancelButton &&
              <IconButton
                className={classes.rightMostAppBarButton}
                color="inherit"
                aria-label="Cancel Edit"
                onClick={this._cancelEdit}
              >
                <CancelIcon />
              </IconButton>
            }
          </Appbar>
          <div className={classes.toolbar} />
          {!user &&
            <Paper className={classes.noMessage}>
              <Typography
                className={classes.blankPagePadding}
                variant={'headline'}
              >
                User does not exist.
                </Typography>
            </Paper>
          }
          {user && <UserProfile user={user} showEditUserProfileForm={showEditUserProfileForm} onSave={this._cancelEdit} />}
        </div>
      </Hidden>
    );
  }
}

const styles = theme => ({
  flex: {
    flex: '1 1 auto'
  },
  rightMostAppBarButton: {
    marginLeft: 20,
    marginRight: -12
  },
  workarea: {
    display: 'flex',
    flexDirection: 'column',
    height: '100vh'
  },
  toolbar: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: '0 8px',
    ...theme.mixins.toolbar
  },
  noMessage: {
    flex: '1 1 auto',
    display: 'flex',
    alignItems: 'center',
    alignItems: 'stretch',
    justifyContent: 'center',
    boxShadow: 'unset'
  },
  blankPagePadding: {
    padding: '10%',
    textAlign: '-webkit-center',
    alignSelf: 'center'
  }
});

const mapStateToProps = state => {
  return {
    loggedInUserUUID: state.loggedInUserUUID
  };
};

export default compose(
  connect(
    mapStateToProps
  ),
  withRoot,
  withStyles(styles, { withTheme: true })
)(UserProfileScreen);
