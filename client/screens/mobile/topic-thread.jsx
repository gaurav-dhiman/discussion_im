import React, { Component } from 'react';
import { compose } from 'redux';
import { connect } from 'react-redux';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import withRoot from '../../lib/with-root';
import Typography from '@material-ui/core/Typography';
import Hidden from '@material-ui/core/Hidden';
import Link from 'next/link';
import IconButton from '@material-ui/core/IconButton';
import SearchIcon from '@material-ui/icons/Search';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';

import TopicThread from '../../components/topic-thread';
import Appbar from '../../components/app-bar';
import SearchBar from '../../components/search-bar';

import { withRouter } from 'next/router'

class TopicThreadScreen extends Component {
  state = {
    showSearchBar: false,
    searchText: ''
  };

  _handleSearchTextChange = searchText => this.setState({ ...this.state, searchText });

  _removeSearchBar = () => this.setState({ ...this.state, showSearchBar: false });

  render() {
    const { classes, message, replies, router } = this.props;
    const { showSearchBar, searchText } = this.state;
    const topicUUID = message.topicUUID || router.query.topicUUID;
    const backLink = {
      as: `/topics/${topicUUID}`,
      href: `/topics?topicUUID=${topicUUID}`
    };
    if (message.parentTopicMessageUUID) {
      backLink.as += `/messages/${message.parentTopicMessageUUID}`;
      backLink.href += `&topicMessageUUID=${message.parentTopicMessageUUID}`;
    }

    return (
      <Hidden mdUp implementation="css">
        <div className={classes.workarea}>
          {!showSearchBar &&
            <Appbar>
              {<Link as={backLink.as} href={backLink.href} prefetch passHref>
                <IconButton
                  className={classes.leftMostAppBarButton}
                  color="inherit"
                  aria-label="Search"
                >
                  <ArrowBackIcon />
                </IconButton>
              </Link>}
              <Typography
                className={classes.flex}
                variant="subheading"
                color="inherit"
              >
                Topic Thread
            </Typography>
              <IconButton
                className={classes.rightMostAppBarButton}
                color="inherit"
                aria-label="Search"
                onClick={() => this.setState({ ...this.state, showSearchBar: true })}
              >
                <SearchIcon />
              </IconButton>
            </Appbar>
          }

          {showSearchBar &&
            <SearchBar
              handleSearchTextChange={this._handleSearchTextChange}
              onBackButtonClick={this._removeSearchBar}
              searchPlaceholder={'Search Topic Messages ...'}
            />
          }

          <div className={classes.toolbar} />

          <TopicThread searchText={searchText} message={message} replies={replies} invalidTopicMessage='This topic message does not exist. Go to home page to see all topics.' />
        </div>
      </Hidden>
    );
  }
}

const styles = theme => ({
  flex: {
    flex: '1 1 auto'
  },
  leftMostAppBarButton: {
    marginLeft: -25
  },
  rightMostAppBarButton: {
    marginLeft: 20,
    marginRight: -12
  },
  workarea: {
    display: 'flex',
    flexDirection: 'column',
    height: '100vh'
  },
  toolbar: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: '0 8px',
    ...theme.mixins.toolbar
  }
});

const mapStateToProps = state => {
  return {};
};

export default compose(
  connect(
    mapStateToProps
  ),
  withRoot,
  withRouter,
  withStyles(styles, { withTheme: true })
)(TopicThreadScreen);
