import React, { Component } from 'react';
import { compose } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import withRoot from '../../lib/with-root';
import Typography from '@material-ui/core/Typography';
import Hidden from '@material-ui/core/Hidden';
import Link from 'next/link';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';

import IconButton from '@material-ui/core/IconButton';
import SearchIcon from '@material-ui/icons/Search';

import Appbar from '../../components/app-bar';
import SearchBar from '../../components/search-bar';
import TopicList from '../../components/topic-list';
import { topicsSelector } from '../../redux/selectors';

class TopicsScreen extends Component {
  state = {
    showSearchBar: false,
    searchText: ''
  };

  _handleSearchTextChange = searchText => this.setState({ ...this.state, searchText });

  _removeSearchBar = () => this.setState({ ...this.state, showSearchBar: false });

  render() {
    const { classes, topics, openAddTopicModal } = this.props;
    const { showSearchBar } = this.state;

    return (
      <Hidden mdUp implementation="css">
        {!showSearchBar &&
          <Appbar>
            <Typography
              className={classes.flex}
              variant="subheading"
              color="inherit"
            >
              Topics
            </Typography>
            <IconButton
              className={classes.rightMostAppBarButton}
              color="inherit"
              aria-label="Search"
              onClick={() => this.setState({ ...this.state, showSearchBar: true })}
            >
              <SearchIcon />
            </IconButton>
          </Appbar>
        }

        {!!showSearchBar &&
          <SearchBar
            handleSearchTextChange={this._handleSearchTextChange}
            onBackButtonClick={this._removeSearchBar}
            searchPlaceholder={'Search Topics ...'}
          />
        }

        <div className={classes.toolbar} />

        <TopicList
          searchText={this.state.searchText}
          topics={topics}
        />

        <Link as={'/topics/explore'} href={'/topics?topicUUID=explore'} prefetch={false} passHref >
          <Fab color="primary" aria-label="add" className={classes.fab}>
            <AddIcon />
          </Fab>
        </Link>
      </Hidden>
    );
  }
}

const styles = theme => ({
  fab: {
    position: 'fixed',
    bottom: theme.spacing.unit * 2,
    right: theme.spacing.unit * 2
  },
  flex: {
    flex: '1 1 auto'
  },
  rightMostAppBarButton: {
    marginLeft: 20,
    marginRight: -12
  },
  toolbar: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: '0 8px',
    ...theme.mixins.toolbar
  }
});

const mapStateToProps = (state, props) => {
  return {
    topics: props.topics || topicsSelector(state)
  };
};

const mapDispatchToProps = dispatch => {
  return {};
};

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  withRoot,
  withStyles(styles, { withTheme: true })
)(TopicsScreen);
