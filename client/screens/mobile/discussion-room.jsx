import React, { Component } from 'react';
import { compose } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Link from 'next/link';
import Dotdotdot from 'react-dotdotdot'
import { withStyles } from '@material-ui/core/styles';
import withRoot from '../../lib/with-root';
import Typography from '@material-ui/core/Typography';
import Hidden from '@material-ui/core/Hidden';

import IconButton from '@material-ui/core/IconButton';
import SearchIcon from '@material-ui/icons/Search';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import MenuItem from '@material-ui/core/MenuItem';

import DiscussionRoom from '../../components/discussion-room';
import SearchBar from '../../components/search-bar';
import Appbar from '../../components/app-bar';
import { topicSelector } from '../../redux/selectors';
import MoreOptionsButton from '../../components/more-options-button';
import { starTopic, unstarTopic, subscribeToTopic, unsubscribeFromTopic } from '../../redux/actions';

class DiscussionRoomScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showSearchBar: false,
      applyPinFilter: false,
      searchText: ''
    };
  }

  _handleSearchTextChange = searchText => this.setState({ searchText });

  _removeSearchBar = () => this.setState({ ...this.state, showSearchBar: false });

  _starTopic = () => {
    this.props.topic && this.props.starTopic(this.props.topic.uuid);
  }

  _unstarTopic = () => {
    this.props.topic && this.props.unstarTopic(this.props.topic.uuid);
  }

  _subscribeToTopic = () => {
    this.props.topic && this.props.subscribe(this.props.topic.uuid);
  }

  _unsubscribeFromTopic = () => {
    this.props.topic && this.props.unsubscribe(this.props.topic.uuid);
  }

  _applyPinFilter = () => this.setState({ ...this.state, applyPinFilter: true });

  _removePinFilter = () => this.setState({ ...this.state, applyPinFilter: false });

  render() {
    const { classes, title, topic, topicMessages, loggedInUserUUID, pushSubscriptionId } = this.props;
    const { showSearchBar, applyPinFilter } = this.state;
    topic.staredByUsersUUIDs = topic.staredByUsersUUIDs || {};
    topic.webPushSubscriptionIds = topic.webPushSubscriptionIds || {};
    const stared = topic.staredByUsersUUIDs[loggedInUserUUID];
    const subscribed = topic.webPushSubscriptionIds[pushSubscriptionId] || false;

    return (
      <Hidden mdUp implementation="css">
        <div className={classes.fullScreen}>
          {!showSearchBar &&
            <Appbar>
              {<Link href={`/topics`} prefetch passHref>
                <IconButton
                  className={classes.leftMostAppBarButton}
                  color="inherit"
                  aria-label="Search"
                >
                  <ArrowBackIcon />
                </IconButton>
              </Link>}
              <Typography
                className={classes.flex}
                variant="subheading"
                color="inherit"
              >
                <Dotdotdot clamp={1}>
                  {title || (topic && topic.title) || 'Topic Title'}
                </Dotdotdot>

              </Typography>
              <IconButton
                className={classes.rightMostAppBarButton}
                color="inherit"
                aria-label="Search"
                onClick={() => this.setState({ ...this.state, showSearchBar: true })}
              >
                <SearchIcon />
              </IconButton>
              <MoreOptionsButton>
                {!stared && <MenuItem onClick={this._starTopic}>Star to add topic to list</MenuItem>}
                {!!stared && <MenuItem onClick={this._unstarTopic}>Unstar to remove topic from list</MenuItem>}
                {!subscribed && <MenuItem onClick={this._subscribeToTopic}>Subscribe to notification</MenuItem>}
                {!!subscribed && <MenuItem onClick={this._unsubscribeFromTopic}>Unsubscribe from notification</MenuItem>}
                {!applyPinFilter && <MenuItem onClick={this._applyPinFilter}>Show only pinned messages</MenuItem>}
                {!!applyPinFilter && <MenuItem onClick={this._removePinFilter}>Show all messages</MenuItem>}
              </MoreOptionsButton>
            </Appbar>
          }

          {!!showSearchBar &&
            <SearchBar
              handleSearchTextChange={this._handleSearchTextChange}
              onBackButtonClick={this._removeSearchBar}
              searchPlaceholder={'Search topic messages ...'}
            />
          }

          <div className={classes.toolbar} />

          <DiscussionRoom
            searchText={this.state.searchText}
            topic={topic}
            topicMessages={topicMessages}
            applyPinFilter={applyPinFilter}
            invalidTopicMessage='This topic does not exist. Go to home page to see all topics.'
          />
        </div>
      </Hidden>
    );
  }
}

const styles = theme => ({
  flex: {
    flex: '1 1 auto'
  },
  fullScreen: {
    height: '100vh',
    display: 'flex',
    flexDirection: 'column'
  },
  leftMostAppBarButton: {
    marginLeft: -25
  },
  rightMostAppBarButton: {
    marginLeft: 20,
    marginRight: -12
  },
  toolbar: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: '0 8px',
    ...theme.mixins.toolbar
  }
});

const mapStateToProps = (state, props) => {
  return {
    topic: props.topic || topicSelector(state, { uuid: state.currentTopicUUID }) || {},
    loggedInUserUUID: state.loggedInUserUUID,
    pushSubscriptionId: state.pushSubscriptionId
  };
};

const mapDispatchToProps = dispatch => {
  return {
    starTopic: topicUUID => dispatch(starTopic(topicUUID)),
    unstarTopic: topicUUID => dispatch(unstarTopic(topicUUID)),
    subscribe: (topicUUID) => dispatch(subscribeToTopic(dispatch, topicUUID)),
    unsubscribe: (topicUUID) => dispatch(unsubscribeFromTopic(dispatch, topicUUID))
  };
};

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  withRoot,
  withStyles(styles, { withTheme: true })
)(DiscussionRoomScreen);
