import { createStore, applyMiddleware } from 'redux';
// import { createOffline } from '@redux-offline/redux-offline';
// import offlineConfig from '@redux-offline/redux-offline/lib/defaults';
// import persist from '@redux-offline/redux-offline/lib/defaults/persist';
import { composeWithDevTools } from 'redux-devtools-extension';
import { createLogger } from 'redux-logger';
import createSagaMiddleware, { END } from 'redux-saga';

import reducer from '../reducers';
import defaultInitialState from './initial-state';
import saga from '../../sagas';

const sagaMiddleware = createSagaMiddleware();

const loggerMiddleware = createLogger({
  level: 'info',
  collapsed: true
});

const middlewareList = [loggerMiddleware, sagaMiddleware];

// const {
//   middleware: offlineMiddleware,
//   enhanceReducer,
//   enhanceStore
// } = createOffline(offlineConfig);

// const middleware = applyMiddleware(...middlewareList, offlineMiddleware);
const middleware = applyMiddleware(...middlewareList);

// export default function configureStore(initialState = defaultInitialState) {
//   const store = createStore(
//     enhanceReducer(reducer),
//     initialState,
//     composeWithDevTools(enhanceStore, middleware)
//   );

export default function configureStore(initialState = defaultInitialState) {
  const store = createStore(
    reducer,
    initialState,
    composeWithDevTools(middleware)
  );

  /**
   * next-redux-saga depends on `runSagaTask` and `sagaTask` being attached to the store.
   *
   *   `runSagaTask` is used to rerun the rootSaga on the client when in sync mode (default)
   *   `sagaTask` is used to await the rootSaga task before sending results to the client
   *
   */
  store.runSagaTask = () => {
    // Avoid running twice
    if (store.sagaTask) return;
    store.sagaTask = sagaMiddleware.run(saga);
  };

  store.stopSaga = async () => {
    // Avoid running twice
    if (!store.saga) return;
    store.dispatch(END);
    await store.saga.done;
    store.saga = null;
  };

  store.execSagaTasks = async (isServer, tasks) => {
    // run saga
    store.runSagaTask();
    // dispatch saga tasks
    tasks(store.dispatch);
    // Stop running and wait for the tasks to be done
    await store.stopSaga();
    // Re-run on client side
    if (!isServer) {
      store.runSagaTask();
    }
  };

  store.runSagaTask();

  // if (typeof window != 'undefined')
  //   window.purge = () => {
  //     return new Promise(async (resolve, reject) => {
  //       // Purge RAM cached reducer states
  //       store.dispatch({ type: 'RESET' });

  //       // Purge disk cached reducer states
  //       const persistor = persist(store, {}, err => {
  //         if (err) reject(err);
  //         resolve();
  //       });
  //        await persistor.purge(); // v5 returns a promise, might want to await
  //     });
  //   };
  return store;
}
