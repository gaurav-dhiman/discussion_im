import { createStore, applyMiddleware, compose } from 'redux';
import { createOffline } from '@redux-offline/redux-offline';
import offlineConfig from '@redux-offline/redux-offline/lib/defaults';
import createSagaMiddleware from 'redux-saga';

import reducer from '../reducers';
import defaultInitialState from './initial-state';

import saga from '../../sagas';

const sagaMiddleware = createSagaMiddleware();

const middlewareList = [
  /* other middleware here */
  sagaMiddleware
];

// const {
//   middleware: offlineMiddleware,
//   enhanceReducer,
//   enhanceStore
// } = createOffline(offlineConfig);

// const middleware = applyMiddleware(...middlewareList, offlineMiddleware);
const middleware = applyMiddleware(...middlewareList);

// Redux store initialization
// export default function configureStore(initialState = defaultInitialState) {
//   const store = createStore(
//     enhanceReducer(reducer),
//     initialState,
//     compose(
//       enhanceStore,
//       middleware
//     )
//   );

export default function configureStore(initialState = defaultInitialState) {
  const store = createStore(
    reducer,
    initialState,
    middleware
  );

  /**
   * next-redux-saga depends on `runSagaTask` and `sagaTask` being attached to the store.
   *
   *   `runSagaTask` is used to rerun the rootSaga on the client when in sync mode (default)
   *   `sagaTask` is used to await the rootSaga task before sending results to the client
   *
   */
  store.runSagaTask = () => {
    if (store.sagaTask) return;
    store.sagaTask = sagaMiddleware.run(saga);
  };

  store.stopSaga = async () => {
    // Avoid running twice
    if (!store.saga) return;
    store.dispatch(END);
    await store.saga.done;
    store.saga = null;
  };

  store.execSagaTasks = async (isServer, tasks) => {
    // run saga
    store.runSagaTask();
    // dispatch saga tasks
    tasks(store.dispatch);
    // Stop running and wait for the tasks to be done
    await store.stopSaga();
    // Re-run on client side
    if (!isServer) {
      store.runSagaTask();
    }
  };

  store.runSagaTask();
  return store;
}
