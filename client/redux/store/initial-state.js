import orm from '../orm-models';
const initialORMState = orm.getEmptyState();

const defaultInitialState = {
  orm: orm.session(initialORMState).state,
  loggedInUserUUID: '',
  currentTopicUUID: '',
  currentTopicMessageUUID: '',
  highlightCurrentTopicMessage: false,
  ui: {
    addTopicModal: {
      show: false,
      searchText: ''
    },
    // showAddTopicModal: false,
    showAuthModal: false,
    snackbarNotifications: [],
    actionModal: {
      show: false,
      title: '',
      message: '',
      buttons: []
    }
  },
  postLoginActions: []
};

export default defaultInitialState;