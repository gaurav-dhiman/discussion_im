import { actionTypes } from '../actions';

export default function currentTopic(state = '', action) {
  switch (action.type) {
    case actionTypes.SET_CURRENT_TOPIC_UUID:
      return action.topicUUID || '';
    default:
      return state;
  }
}
