import { actionTypes } from '../actions';

export function highlightCurrentTopicMessage(state = false, action) {
  switch (action.type) {
    case actionTypes.HIGHLIGHT_CURRENT_TOPIC_MESSAGE:
      return action.value || false;
    default:
      return state;
  }
}

export function currentTopicMessageUUID(state = '', action) {
  switch (action.type) {
    case actionTypes.SET_CURRENT_TOPIC_MESSAGE_UUID:
      return action.topicMessageUUID || '';
    default:
      return state;
  }
}
