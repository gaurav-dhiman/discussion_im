import { actionTypes } from '../../actions';

export default function modals(state = false, action) {
  switch (action.type) {
    case actionTypes.UI_SHOW_ADD_TOPIC_MODAL:
      return {
        show: true,
        searchText: action.searchText
      };
    case actionTypes.UI_HIDE_ADD_TOPIC_MODAL:
      return {
        show: false,
        searchText: ''
      };
    case actionTypes.UI_SHOW_AUTH_MODAL:
      return true;
    case actionTypes.UI_HIDE_AUTH_MODAL:
      return false;
    case actionTypes.UI_SHOW_ACTION_MODAL:
      return {
        show: true,
        title: action.title,
        message: action.message,
        buttons: action.buttons
      }
    case actionTypes.UI_HIDE_ACTION_MODAL:
      return {
        show: false,
        title: '',
        message: '',
        buttons: []
      }
    default:
      return state;
  }
}
