import { actionTypes } from '../../actions';
import modals from './modals';
import notifications from './notifications';

export default function ui(state = {}, action) {
  let ui;
  switch (action.type) {
    case actionTypes.UI_SHOW_ADD_TOPIC_MODAL:
    case actionTypes.UI_HIDE_ADD_TOPIC_MODAL:
      ui = { ...state };
      ui.addTopicModal = modals(state.addTopicModal, action);
      return ui;
    case actionTypes.UI_SHOW_AUTH_MODAL:
    case actionTypes.UI_HIDE_AUTH_MODAL:
      ui = { ...state };
      ui.showAuthModal = modals(state.showAuthModal, action);
      return ui;
    case actionTypes.UI_SHOW_ACTION_MODAL:
    case actionTypes.UI_HIDE_ACTION_MODAL:
      ui = { ...state };
      ui.actionModal = modals(state.actionModal, action);
      return ui;
    case actionTypes.SHOW_NOTIFICATION_SNACKBAR:
    case actionTypes.REMOVE_NOTIFICATION_SNACKBAR:
      ui = { ...state };
      ui.snackbarNotifications = notifications(state.snackbarNotifications, action);
      return ui;
    default:
      return state;
  }
}
