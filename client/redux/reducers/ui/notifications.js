import { actionTypes } from '../../actions';

export default function notifications(state = [], action) {
  action.options = action.options || {};
  const { type = 'info' } = action.options;
  let newState;

  switch (action.type) {
    case actionTypes.SHOW_NOTIFICATION_SNACKBAR:
      newState = state.map(obj => obj) // make a copy of state
      newState.push({ ...action.options, displayMessage: action.displayMessage });
      return newState;
    case actionTypes.REMOVE_NOTIFICATION_SNACKBAR:
      newState = state.filter((notification) => {
        if (notification.id == action.notificationId) return false;
        return true;
      });
      return newState;
    default:
      return state;
  }
}
