import { actionTypes } from '../actions';

export default function pushSubscriptionId(state = '', action) {
  switch (action.type) {
    case actionTypes.SET_PUSH_SUBSCRIPTION_ID:
      return action.subscriptionId || '';
    default:
      return state;
  }
}
