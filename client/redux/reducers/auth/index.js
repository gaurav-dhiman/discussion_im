import { actionTypes } from '../../actions';

function showForm(state, formName) {
  state.showAskEmailIdForm = false;
  state.showLoginForm = false;
  state.showSignupForm = false;
  state.showForgotPasswordForm = false;
  state[formName] = true;
  return state;
}

export default function auth(state = {}, action) {
  let auth;
  const { user = {} } = action;
  switch (action.type) {
    case actionTypes.UI_SHOW_AUTH_MODAL:
      auth = {};
      showForm(auth, 'showAskEmailIdForm');
      return auth;
    case actionTypes.UI_HIDE_AUTH_MODAL:
      auth = {};
      return auth;
    case actionTypes.AUTH_SHOW_LOGIN_FORM:
      auth = { ...state };
      auth.user = {
        firstName: user.firstName || '',
        lastName: user.lastName || '',
        displayName: user.displayName || '',
        email: user.email || '',
        googleId: user.googleId,
        facebookId: user.facebookId,
        twitterId: user.twitterId,
        githubId: user.githubId,
        profileImageURL: user.profileImageURL,
        verifyToken: user.verifyToken
      };
      showForm(auth, 'showLoginForm');
      return auth;
    case actionTypes.AUTH_SHOW_SIGNUP_FORM:
      auth = { ...state };
      auth.user = {
        email: user.email
      }
      showForm(auth, 'showSignupForm');
      return auth;
    case actionTypes.AUTH_SHOW_FORGOT_PASSWORD_FORM:
      auth = { ...state };
      showForm(auth, 'showForgotPasswordForm');
      return auth;
    default:
      return state;
  }
}
