/**
 * Reducers are functions that take the current state as well as action object and returns new state.
 * This file includes the Root reducer that invokes other sub-reducers to prepare the new state.
 */

import { combineReducers } from 'redux';
import { createReducer } from 'redux-orm';
import orm from '../orm-models';
import loggedInUserUUID from './loggedin-user';
import currentTopicUUID from './current-topic';
import { highlightCurrentTopicMessage, currentTopicMessageUUID } from './current-topic-message';
import pushSubscriptionId from './web-push';
import ui from './ui';
import auth from './auth';
import postLoginActions from './post-login-action';

export default combineReducers({
  orm: createReducer(orm),
  auth,
  loggedInUserUUID,
  currentTopicUUID,
  currentTopicMessageUUID,
  highlightCurrentTopicMessage,
  pushSubscriptionId,
  ui,
  postLoginActions
});
