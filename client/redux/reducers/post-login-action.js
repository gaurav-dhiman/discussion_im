import { actionTypes } from '../actions';

export default function postLoginActions(state = [], action) {
  let newState;
  switch (action.type) {
    case actionTypes.ADD_POST_LOGIN_ACTION:
      if (!action.action) return state;
      newState = state.map(action => action);
      newState.push(action.action);
      return newState;
    case actionTypes.UI_HIDE_AUTH_MODAL:
      newState = state.map(action => action);
      newState.pop(action.action);
      return newState;
    case actionTypes.CLEAR_POST_LOGIN_ACTIONS:
      return [];
    default:
      return state;
  }
}
