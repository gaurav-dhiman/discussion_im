import { actionTypes } from '../actions';

export default function loggedInUserUUID(state = '', action) {
  switch (action.type) {
    case actionTypes.UI_SHOW_AUTH_MODAL:
      // as we are showing the login modal, so the user details in state are not current - remove them.
      return '';
    case actionTypes.SET_LOGGEDIN_USER_UUID:
      return action.userUUID || '';
    case actionTypes.UNSET_LOGGEDIN_USER_UUID:
      return '';
    default:
      return state;
  }
}
