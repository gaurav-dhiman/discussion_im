import { Model, many, fk, attr } from 'redux-orm';
import PropTypes from 'prop-types';
import propTypesMixin from 'redux-orm-proptypes';

import { actionTypes } from '../actions';

const ValidatingModel = propTypesMixin(Model);

export default class Topics extends ValidatingModel {
  static reducer(action) {
    const { type, topics, skipUpdatingMetaData } = action;

    switch (type) {
      case actionTypes.ADD_TOPICS_TO_REDUX_STORE:
        this.parseAndStore(topics, !skipUpdatingMetaData);
        break;
    }
  }

  static areMultipleEntries(obj) {
    if (obj.data && obj.total.toString() && obj.skip.toString() && obj.limit.toString()) return true;
    return false;
  }

  static parseAndStore(objs, saveMeta) {
    const { Topics, TopicMessages, Users, MetaDatas } = this.session;
    if (!objs) return;
    // objs = { ...objs }

    if (this.areMultipleEntries(objs)) {
      if (saveMeta) {
        const metaData = {
          modelUUID: objs.modelUUID || '',
          modelType: objs.modelType || 'all',
          skip: objs.skip || 0,
          limit: objs.limit || 0,
          total: objs.total || 0,
          metaDataFor: objs.metaDataFor || 'topics'
        };
        MetaDatas.upsert(metaData);
      }
      objs = objs.data || [];
    } else objs = [objs];

    objs.forEach(obj => {
      if (!obj) return;
      const { creator } = obj;
      let { messages } = obj;
      delete obj.creator;
      delete obj.messages;

      if (messages) {
        messages = { ...messages, modelUUID: obj.uuid, modelType: 'topic', metaDataFor: 'messages' };
        TopicMessages.parseAndStore(messages, saveMeta);
      }

      if (creator) Users.parseAndStore(creator);

      Topics.upsert(obj);
    });
  }
}
Topics.modelName = 'Topics';
Topics.options = {
  idAttribute: 'uuid',
};
Topics.fields = {
  uuid: attr(),
  title: attr(),
  description: attr(),
  // tags: many('Tags', 'topics'),
  creator: fk('Users', 'topics')
};

// Topics.propTypes = {
//   uuid: PropTypes.string.isRequired,
//   title: PropTypes.string.isRequired,
//   description: PropTypes.string.isRequired,
//   creator: PropTypes.oneOfType([
//     PropTypes.instanceOf(User),
//     PropTypes.number,
//   ]).isRequired,
//   tags: PropTypes.arrayOf(PropTypes.oneOfType([
//     PropTypes.instanceOf(Tag),
//     PropTypes.string,
//   ])).isRequired
// };
Topics.defaultProps = {
  // done: false,
};