import { ORM as REDUX_ORM } from 'redux-orm';
import MetaDatas from './meta-data';
import Topics from './topic';
import TopicMessages from './topic-messages';
import Users from './user';

// const orm = new REDUX_ORM();
// orm.register(MetaData, Topic);

class ORMClass extends REDUX_ORM {
  static orm;
  constructor() {
    super();
    if(ORMClass.orm) return ORMClass.orm;
  }
};

const orm = new ORMClass();
orm.register(MetaDatas, Topics, TopicMessages, Users);

export default orm;