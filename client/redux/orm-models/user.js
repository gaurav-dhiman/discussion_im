import { Model, many, fk, attr } from 'redux-orm';
import PropTypes from 'prop-types';
import propTypesMixin from 'redux-orm-proptypes';

import { actionTypes } from '../actions';

const ValidatingModel = propTypesMixin(Model);

export default class Users extends ValidatingModel {
  static reducer(action) {
    const { type, users, skipUpdatingMetaData } = action;

    switch (type) {
      case actionTypes.ADD_USERS_TO_REDUX_STORE:
        this.parseAndStore(users, !skipUpdatingMetaData);
        break;
    }
  }

  static areMultipleEntries(obj) {
    if (obj.data && obj.total.toString() && obj.skip.toString() && obj.limit.toString()) return true;
    return false;
  }

  static parseAndStore(objs, saveMeta) {
    const { Users, Topics, MetaDatas } = this.session;
    if (!objs) return;

    if (this.areMultipleEntries(objs)) {
      if (saveMeta) {
        const metaData = {
          modelUUID: objs.modelUUID || '',
          modelType: objs.modelType || 'all',
          skip: objs.skip || 0,
          limit: objs.limit || 0,
          total: objs.total || 0,
          metaDataFor: objs.metaDataFor || 'topics'
        };
        MetaDatas.upsert(metaData);
      }
      objs = objs.data || [];
    } else objs = [objs];

    objs.forEach(obj => {
      if (!obj) return;
      let { topics, staredTopics } = obj;
      delete obj.topics;
      delete obj.staredTopics;

      if (topics) {
        topics = { ...obj.topics, modelUUID: obj.uuid, modelType: 'user', metaDataFor: 'topics' };
        Topics.parseAndStore(topics, saveMeta);
      }

      if (staredTopics) {
        staredTopics = { ...obj.staredTopics, modelUUID: obj.uuid, modelType: 'user', metaDataFor: 'stared-topics' };
        Topics.parseAndStore(staredTopics, saveMeta);
      }

      Users.upsert(obj);
    });
  }
}
Users.modelName = 'Users';
Users.options = {
  idAttribute: 'uuid',
};
Users.fields = {
  uuid: attr(),
  firstName: attr(),
  lastName: attr(),
  displayName: attr(),
  handle: attr()
  // topics: many('Tags', 'topics'),
  // creator: fk('User', 'topics'),
};

Users.defaultProps = {
  // done: false,
};