import { Model } from 'redux-orm';
import PropTypes from 'prop-types';
import propTypesMixin from 'redux-orm-proptypes';

import { actionTypes } from '../actions';

const ValidatingModel = propTypesMixin(Model);

export default class MetaDatas extends ValidatingModel {
  // static reducer(action, Topic, session) {
  //   const { type, payload } = action;
  //   const { MetaDatas } = session;
  //   const { topics, isOlder, skipUpdatingMetaData } = payload || {};
  //   const { ADD_TOPICS_TO_REDUX_STORE } = actionTypes;
  //   switch (type) {
  //     case ADD_TOPICS_TO_REDUX_STORE:
  //       topics.data.forEach(topic => Topics.create(topic));
  //       if (!skipUpdatingMetaData) {
  //         const metaData = {
  //           modelUUID: '',
  //           modelType: 'all',
  //           skip: topics.skip,
  //           limit: topics.limit,
  //           total: topics.total,
  //           metaDataFor: 'topics'
  //         };
  //         MetaDatas.upsert(metaData);
  //       }
  //       break;
  //     case ADD_TOPIC_TO_REDUX_STORE:
  //       break;
  //   }
  // }

  static upsert(obj) {
    let _id = obj.metaDataFor.toLowerCase() + '-of-' + obj.modelType.toLowerCase();
    _id = _id + (obj.modelUUID && '-id-' + obj.modelUUID);
    obj = { ...obj, _id };
    super.upsert(obj);
  }
}
MetaDatas.modelName = 'MetaDatas';
MetaDatas.options = {
  idAttribute: '_id',
};
MetaDatas.fields = {
  // tags: many('TopicMessage', 'topics'),
  // creator: fk('User', 'topics'),
};

// MetaDatas.propTypes = {
//   modelUUID: PropTypes.string.isRequired,
//   modelType: PropTypes.string.isRequired,
//   skip: PropTypes.number.isRequired,
//   limit: PropTypes.number.isRequired,
//   total: PropTypes.number.isRequired,
//   metaDataFor: PropTypes.string.isRequired
//   // creator: PropTypes.oneOfType([
//   //     PropTypes.instanceOf(User),
//   //     PropTypes.number,
//   // ]).isRequired,
//   // tags: PropTypes.arrayOf(PropTypes.oneOfType([
//   //     PropTypes.instanceOf(Tag),
//   //     PropTypes.string,
//   // ])).isRequired
// };
MetaDatas.defaultProps = {
  // done: false,
};