import { Model, many, fk, attr } from 'redux-orm';
import PropTypes from 'prop-types';
import propTypesMixin from 'redux-orm-proptypes';

import { actionTypes } from '../actions';

const ValidatingModel = propTypesMixin(Model);

export default class TopicMessages extends ValidatingModel {
  static reducer(action) {
    const { type, topicMessages, skipUpdatingMetaData } = action;

    switch (type) {
      case actionTypes.ADD_TOPIC_MESSAGES_TO_REDUX_STORE:
        this.parseAndStore(topicMessages, !skipUpdatingMetaData)
        break;
    }
  }

  static areMultipleEntries(obj) {
    if (obj.data && obj.total.toString() && obj.skip.toString() && obj.limit.toString()) return true;
    return false;
  }

  static parseAndStore(objs, saveMeta) {
    const { Topics, TopicMessages, Users, MetaDatas } = this.session;
    if (!objs) return;
    // objs = { ...objs };

    if (this.areMultipleEntries(objs)) {
      if (saveMeta) {
        const metaData = {
          modelUUID: objs.modelUUID || '',
          modelType: objs.modelType || 'all',
          skip: objs.skip || 0,
          limit: objs.limit || 0,
          total: objs.total || 0,
          metaDataFor: objs.metaDataFor || 'messages'
        };
        MetaDatas.upsert(metaData);
      }
      objs = objs.data || [];
    } else objs = [objs];

    objs.forEach(obj => {
      if (!obj) return;
      const { topic, sender } = obj;
      let { replies } = obj;
      delete obj.topic;
      delete obj.sender;
      delete obj.replies;

      if (replies) {
        replies = { ...replies, modelUUID: obj.uuid, modelType: 'topic-message', metaDataFor: 'replies' };
        TopicMessages.parseAndStore(replies, saveMeta);
      }

      if (sender) Users.parseAndStore(sender);
      
      if (topic) Topics.parseAndStore(topic);

      TopicMessages.upsert(obj);
    });
  }

  // /**
  //  * 
  //  * @param {String} fetchPlan This string will tell what deep elements from JSOn tree needs to be fetch for a query on ORM
  //  * E.g. fetch plan: "messages.sender,messages.replies.sender,topic.messages"
  //  */
  // toJSON(fetchPlan) {
  //   fetchPlan = fetchPlan || '';
  //   const queryMapping = [
  //     { field: 'replies', model: 'TopicMessages', refField: 'parentTopicMessageUUID', isArray: 'yes' },
  //     { field: 'sender', model: 'Users', refField: '', isArray: 'no' }
  //   ]
  //   const fp = [];
  //   fetchPlan = fetchPlan.trim().split(',').map(ele => {
  //     ele.trim().split('.').map(ele => {
  //       fp.push(ele.shift());
  //       ele.join('.');
  //     }).join(',');
  //   });
  //   const obj = { ...this.ref };
  //   fp.forEach(field => {
  //     const mappingObj = queryMapping.filter(mapping => mapping.field === field)[0];
  //     if(!mappingObj) return;
  //     let query ={}
  //     query[mappingObj[refField]] = obj[this.idAttribute];
  //     const childArr = this.session[mappingObj[model]].getQuerySet(query).toModelArray().map((child.toJSON(fetchPlan)));
  //     if(mappingObj.isArray) {
  //       // get related meta data
  //       query = {modelUUID: obj.uuid, metaDataFor: mappingObj[field]}
  //       const metaData =  this.session.MetaDatas.getQuerySet(query).toRefArray()[0];
  //       obj[field] = {...metaData, data: childArr};
  //     } else {
  //       obj[field] = childArr[0];
  //     }
  //   });

  //   // const data = {
  //   //   // Include all fields from the plain data object
  //   //   ...this.ref,
  //   //   // As well as serialized versions of all the relations we know about
  //   //   // commander: this.commander.toJSON(),
  //   //   // pilots: this.pilots.withModels.map(pilot => pilot.toJSON()),
  //   //   // mechs: this.mechs.withModels.map(mech => mech.toJSON())
  //   // };

  //   return obj;
  // }
}
TopicMessages.modelName = 'TopicMessages';
TopicMessages.options = {
  idAttribute: 'uuid',
};
TopicMessages.fields = {
  uuid: attr(),
  createdAt: attr(),
  text: attr(),
  // topicUUID: fk('Topics', 'messages'),
  topic: fk('Topics', 'messages'),
  // tags: many('TopicMessage', 'topics'),
  creator: fk('Users', 'topicMessages')
};

// Topics.propTypes = {
//   uuid: PropTypes.string.isRequired,
//   title: PropTypes.string.isRequired,
//   description: PropTypes.string.isRequired,
//   // creator: PropTypes.oneOfType([
//   //     PropTypes.instanceOf(User),
//   //     PropTypes.number,
//   // ]).isRequired,
//   // tags: PropTypes.arrayOf(PropTypes.oneOfType([
//   //     PropTypes.instanceOf(Tag),
//   //     PropTypes.string,
//   // ])).isRequired
// };
TopicMessages.defaultProps = {
  // done: false,
};