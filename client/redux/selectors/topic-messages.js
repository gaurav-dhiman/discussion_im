import { createSelector } from 'redux-orm';
import orm from '../orm-models';
import _ from 'lodash';

export const ormSelector = state => state.orm;

export const topicMessagesSelector = createSelector(
  orm,
  ormSelector,
  (state, query) => query,
  (session, query) => {
    try {
      return session.TopicMessages.getQuerySet().filter(query).orderBy('createdAt').toModelArray().map(topicMessage => topicMessage.ref);
    } catch (error) {
      console.log('Error in topicMessagesSelector(): ', error);
      return [];
    }
  }
);

export const topicMessageSelector = createSelector(
  orm,
  ormSelector,
  (state,  query) => query,
  (session, query) => {
    try {
      return (session.TopicMessages.getQuerySet().filter(query).orderBy('createdAt').toModelArray().map(topicMessage => topicMessage.ref))[0];
    } catch (error) {
      console.log('Error in topicMessageSelector(): ', error);
      return null;
    }
  }
);
