import { createSelector } from 'redux-orm';
import orm from '../orm-models';
import _ from 'lodash';

export const ormSelector = state => state.orm;

export const metaDataSelector = createSelector(
  orm,
  ormSelector,
  (state, query) => query,
  (session, query) => {
    try {
      return session.MetaDatas.getQuerySet().filter(query).toModelArray().map(metaData => metaData.ref)[0];
    } catch (error) {
      console.log('Error in metaDataSelector(): ', error);
      return {};
    }
  }
);
