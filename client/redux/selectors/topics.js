import { createSelector } from 'redux-orm';
import orm from '../orm-models';
import _ from 'lodash';

export const ormSelector = state => state.orm;

export const topicsSelector = createSelector(
  orm,
  ormSelector,
  (state, query) => query,
  (session, query) => {
    try {
      return session.Topics.getQuerySet().filter(query).orderBy('createdAt').toModelArray().map(topic => topic.ref);
    } catch (error) {
      console.log('Error in topicsSelector(): ', error);
      return [];
    }
  }
);

export const topicSelector = createSelector(
  orm,
  ormSelector,
  (state, query) => query,
  (session, query) => {
    try {
      return (session.Topics.getQuerySet().filter(query).orderBy('createdAt').toModelArray().map(topic => topic.ref) || [])[0];
    } catch (error) {
      console.log('Error in topicSelector(): ', error);
      return null;
    }
  }
);
