import { createSelector } from 'redux-orm';
import orm from '../orm-models';
import _ from 'lodash';

export const ormSelector = state => state.orm;

export const usersSelector = createSelector(
  orm,
  ormSelector,
  (state, query) => query,
  (session, query) => {
    try {
      return session.Users.getQuerySet().filter(query).orderBy('createdAt').toModelArray().map(user => user.ref);
    } catch (error) {
      console.log('Error in usersSelector(): ', error);
      return [];
    }
  }
);

export const userSelector = createSelector(
  orm,
  ormSelector,
  (state, query) => query,
  (session, query) => {
    try {
      return (session.Users.getQuerySet().filter(query).orderBy('createdAt').toModelArray().map(user => user.ref) || [])[0];
    } catch (error) {
      console.log('Error in userSelector(): ', error);
      return null;
    }
  }
);
