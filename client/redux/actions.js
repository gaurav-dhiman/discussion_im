/**
 * This file defines the all redux actions at app level.
 */

export const actionTypes = {
  GET_PUBLIC_VAPID_KEY: 'GET_PUBLIC_VAPID_KEY',

  SIGNUP: 'SIGNUP',
  LOGIN: 'LOGIN',
  LOGOUT: 'LOUGOUT',
  AUTH_GET_USER_BY_EMAIL_ID: 'AUTH_GET_USER_BY_EMAIL_ID',
  AUTH_SHOW_LOGIN_FORM: 'AUTH_SHOW_LOGIN_FORM',
  AUTH_SHOW_SIGNUP_FORM: 'AUTH_SHOW_SIGNUP_FORM',
  AUTH_SHOW_FORGOT_PASSWORD_FORM: 'AUTH_SHOW_FORGOT_PASSWORD_FORM',
  GET_LOGGEDIN_USER: 'GET_LOGGEDIN_USER',
  SET_LOGGEDIN_USER_UUID: 'SET_LOGGEDIN_USER_UUID',
  UNSET_LOGGEDIN_USER_UUID: 'UNSET_LOGGEDIN_USER_UUID',

  SEND_RESET_PASSWORD: 'SEND_RESET_PASSWORD',
  RESET_PASSWORD: 'RESET_PASSWORD',
  SEND_VERIFY_ACCOUNT: 'SEND_VERIFY_ACCOUNT',
  VERIFY_ACCOUNT: 'VERIFY_ACCOUNT',

  GET_GLOBAL_TOPIC_SEARCH_RESULTS: 'GET_GLOBAL_TOPIC_SEARCH_RESULTS',
  SET_GLOBAL_TOPIC_SEARCH_RESULTS: 'SET_GLOBAL_TOPIC_SEARCH_RESULTS',

  CREATE_TOPIC: 'CREATE_TOPIC',
  SET_CURRENT_TOPIC_UUID: 'SET_CURRENT_TOPIC_UUID',
  GET_TOPICS: 'GET_TOPICS',
  ADD_TOPICS_TO_REDUX_STORE: 'ADD_TOPICS_TO_REDUX_STORE',
  STAR_TOPIC: 'STAR_TOPIC',
  UNSTAR_TOPIC: 'UNSTAR_TOPIC',
  PIN_TOPIC_MESSAGE: 'PIN_TOPIC_MESSAGE',
  UNPIN_TOPIC_MESSAGE: 'UNPIN_TOPIC_MESSAGE',
  LIKE_TOPIC_MESSAGE: 'LIKE_TOPIC_MESSAGE',
  UNLIKE_TOPIC_MESSAGE: 'UNLIKE_TOPIC_MESSAGE',
  SUBSCRIBE_TO_TOPIC: 'SUBSCRIBE_TO_TOPIC',
  UNSUBSCRIBE_FROM_TOPIC: 'UNSUBSCRIBE_FROM_TOPIC',
  SET_PUSH_SUBSCRIPTION_ID: 'SET_PUSH_SUBSCRIPTION_ID',

  SEND_TOPIC_MESSAGE: 'SEND_TOPIC_MESSAGE',
  SET_CURRENT_TOPIC_MESSAGE_UUID: 'SET_CURRENT_TOPIC_MESSAGE_UUID',
  HIGHLIGHT_CURRENT_TOPIC_MESSAGE: 'HIGHLIGHT_CURRENT_TOPIC_MESSAGE',
  GET_TOPIC_MESSAGES: 'GET_TOPIC_MESSAGES',
  ADD_TOPIC_MESSAGES_TO_REDUX_STORE: 'ADD_TOPIC_MESSAGES_TO_REDUX_STORE',

  UI_SHOW_ADD_TOPIC_MODAL: 'UI_SHOW_ADD_TOPIC_MODAL',
  UI_HIDE_ADD_TOPIC_MODAL: 'UI_HIDE_ADD_TOPIC_MODAL',
  UI_SHOW_AUTH_MODAL: 'UI_SHOW_AUTH_MODAL',
  UI_HIDE_AUTH_MODAL: 'UI_HIDE_AUTH_MODAL',
  UI_SHOW_ACTION_MODAL: 'UI_SHOW_ACTION_MODAL',
  UI_HIDE_ACTION_MODAL: 'UI_HIDE_ACTION_MODAL',
  SHOW_NOTIFICATION_SNACKBAR: 'SHOW_NOTIFICATION_SNACKBAR',
  REMOVE_NOTIFICATION_SNACKBAR: 'REMOVE_NOTIFICATION_SNACKBAR',

  ADD_POST_LOGIN_ACTION: 'ADD_POST_LOGIN_ACTION',
  CLEAR_POST_LOGIN_ACTIONS: 'CLEAR_POST_LOGIN_ACTIONS',

  GET_USER: 'GET_USER',
  ADD_USERS_TO_REDUX_STORE: 'ADD_USERS_TO_REDUX_STORE',
  SAVE_USER_PROFILE_IMAGE: 'SAVE_USER_PROFILE_IMAGE',
  SAVE_USER_PROFILE: 'SAVE_USER_PROFILE',
  SEND_NOTIFICATION_EMAIL_TO_USER: 'SEND_NOTIFICATION_EMAIL_TO_USER'
};

export const getPublicVapidKey = () => {
  return { type: actionTypes.GET_PUBLIC_VAPID_KEY };
};

/**
 * Authentication related action creators
 */
export const getUserByEmailId = (email) => {
  return { type: actionTypes.AUTH_GET_USER_BY_EMAIL_ID, params: { query: { email } } };
};

export const signUp = (user) => {
  return { type: actionTypes.SIGNUP, user };
};

export const login = (loginCred) => {
  return { type: actionTypes.LOGIN, loginCred };
};

export const logout = () => {
  return { type: actionTypes.LOGOUT };
};

export const showLoginForm = (user) => {
  return { type: actionTypes.AUTH_SHOW_LOGIN_FORM, user };
};

export const showSignupForm = (email) => {
  return { type: actionTypes.AUTH_SHOW_SIGNUP_FORM, user: { email } };
};

export const showForgotPasswordForm = () => {
  return { type: actionTypes.AUTH_SHOW_FORGOT_PASSWORD_FORM };
};

export const addPostLoginAction = (action) => {
  return { type: actionTypes.ADD_POST_LOGIN_ACTION, action };
};

export const clearPostLoginActions = () => {
  return { type: actionTypes.CLEAR_POST_LOGIN_ACTIONS };
};

export const getLoggedInUser = userId => {
  return { type: actionTypes.GET_LOGGEDIN_USER, userId };
};

export const setLoggedInUserUUID = userUUID => {
  return { type: actionTypes.SET_LOGGEDIN_USER_UUID, userUUID };
};

export const unsetLoggedInUserUUID = () => {
  return { type: actionTypes.UNSET_LOGGEDIN_USER_UUID };
};

export const sendResetPassword = payload => {
  return { type: actionTypes.SEND_RESET_PASSWORD, payload };
};

export const resetPassword = payload => {
  return { type: actionTypes.RESET_PASSWORD, payload };
};

export const sendVerifyAccount = payload => {
  return { type: actionTypes.SEND_VERIFY_ACCOUNT, payload };
};

export const verifyAccount = payload => {
  return { type: actionTypes.VERIFY_ACCOUNT, payload };
};


/**
 * UI related action creators
 */
export const showAddTopicModal = (searchText) => {
  return { type: actionTypes.UI_SHOW_ADD_TOPIC_MODAL, searchText };
};

export const hideAddTopicModal = () => {
  return { type: actionTypes.UI_HIDE_ADD_TOPIC_MODAL };
};

export const showAuthModal = () => {
  return { type: actionTypes.UI_SHOW_AUTH_MODAL };
};

export const hideAuthModal = () => {
  return { type: actionTypes.UI_HIDE_AUTH_MODAL };
};

export const showActionModal = (modalDetails = {}) => {
  let { title = 'Title', message = 'Message', buttons } = modalDetails
  if (!buttons.length)
    buttons = [
      { label: 'Close', action: hideActionModal() }
    ]
  return { type: actionTypes.UI_SHOW_ACTION_MODAL, title, message, buttons };
};

export const hideActionModal = () => {
  return { type: actionTypes.UI_HIDE_ACTION_MODAL };
};

export const showSnackBarNotification = (displayMessage, options) => {
  return { type: actionTypes.SHOW_NOTIFICATION_SNACKBAR, displayMessage, options: { id: Date.now(), timeout: 6000, ...options } };
};

export const removeSnackbarNotification = (notificationId) => {
  return { type: actionTypes.REMOVE_NOTIFICATION_SNACKBAR, notificationId };
};


/**
 * All Server Communication (API Fetch and Post) related Action Creators
 */
export const getTopics = (query, options) => {
  return { type: actionTypes.GET_TOPICS, query, ...options };
};

export const addTopicsToReduxStore = (topics, skipUpdatingMetaData) => {
  return { type: actionTypes.ADD_TOPICS_TO_REDUX_STORE, topics, skipUpdatingMetaData }
};

export const createTopic = topic => {
  return { type: actionTypes.CREATE_TOPIC, topic };
};

export const setCurrentTopicUUID = topicUUID => {
  return { type: actionTypes.SET_CURRENT_TOPIC_UUID, topicUUID };
};

export const setPushSubscriptionId = subscriptionId => {
  return { type: actionTypes.SET_PUSH_SUBSCRIPTION_ID, subscriptionId };
};

export const starTopic = topicUUID => {
  return { type: actionTypes.STAR_TOPIC, data: { operation: 'star' }, params: { query: { uuid: topicUUID } } };
};

export const unstarTopic = topicUUID => {
  return { type: actionTypes.UNSTAR_TOPIC, data: { operation: 'unstar' }, params: { query: { uuid: topicUUID } } };
};

export const pinTopic = topicMessageUUID => {
  return { type: actionTypes.PIN_TOPIC_MESSAGE, data: { operation: 'pin' }, params: { query: { uuid: topicMessageUUID } } };
};

export const unpinTopic = topicMessageUUID => {
  return { type: actionTypes.UNPIN_TOPIC_MESSAGE, data: { operation: 'unpin' }, params: { query: { uuid: topicMessageUUID } } };
};

export const likeTopicMessage = topicMessageUUID => {
  return { type: actionTypes.LIKE_TOPIC_MESSAGE, data: { operation: 'like' }, params: { query: { uuid: topicMessageUUID } } };
};

export const unlikeTopicMessage = topicMessageUUID => {
  return { type: actionTypes.UNLIKE_TOPIC_MESSAGE, data: { operation: 'unlike' }, params: { query: { uuid: topicMessageUUID } } };
};

export const subscribeToTopic = (dispatch, topicUUID) => {
  return { type: actionTypes.SUBSCRIBE_TO_TOPIC, dispatch, subscribeTo: { type: 'topic', uuid: topicUUID } };
};

export const unsubscribeFromTopic = (dispatch, topicUUID) => {
  return { type: actionTypes.UNSUBSCRIBE_FROM_TOPIC, dispatch, unsubscribeFrom: { type: 'topic', uuid: topicUUID } };
};

/**
 * Topic messessages related action methods 
 */
export const getTopicMessages = (query, options) => {
  return { type: actionTypes.GET_TOPIC_MESSAGES, query, ...options };
};

export const addTopicMessagesToReduxStore = (topicMessages, skipUpdatingMetaData) => {
  return { type: actionTypes.ADD_TOPIC_MESSAGES_TO_REDUX_STORE, topicMessages, skipUpdatingMetaData };
};

export const sendTopicMessage = topicMessage => {
  return { type: actionTypes.SEND_TOPIC_MESSAGE, topicMessage };
};

export const setCurrentTopicMessageUUID = topicMessageUUID => {
  return { type: actionTypes.SET_CURRENT_TOPIC_MESSAGE_UUID, topicMessageUUID };
};

export const hightlightCurrentTopicMessage = value => {
  return { type: actionTypes.HIGHLIGHT_CURRENT_TOPIC_MESSAGE, value };
};



/**
 * User related actions
 */
export const getUser = (query) => {
  return { type: actionTypes.GET_USER, query };
};

export const addUsersToReduxStore = (users, skipUpdatingMetaData) => {
  return { type: actionTypes.ADD_USERS_TO_REDUX_STORE, users, skipUpdatingMetaData };
};

export const saveUserProfileImage = (image) => {
  return { type: actionTypes.SAVE_USER_PROFILE_IMAGE, data: { uri: image } };
};

export const saveUserProfile = (profile, userUUID) => {
  return { type: actionTypes.SAVE_USER_PROFILE, profile, params: { query: { uuid: userUUID } } };
};
