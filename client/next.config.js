const withOffline = require('next-offline');

// const withManifest = require('next-manifest')
// const withPlugins = require('next-compose-plugins');

// module.exports = withPlugins([
//   {
//     plugin: withManifest,
//     configuration: {
//       // all of manifest properties.
//       name: "Immigration Topic",
//       short_name: "Immi Topic",
//       start_url: "/topics",
//       display: "standalone",
//       background_color: "#fff",
//       theme_color: "cyan",
//       // if src value is exist, icon image will be generated from src image, and ovwewritten
//       // icons value exist in the properties. if you want to keep your own icons path? do not pass
//       // src path to this plugin
//       icons: {
//         // source image path, to generate applications icons in 192x192, 512x512 sizes for manifest.
//         src: './public/splash-screen-owl.png',
//         // default is true, cache images until the hash value of source image has changed
//         // if false, generating new icon images while every build time.
//         cache: true
//       }
//     }
//   }, {
//     plugin: withOffline
//   }
// ]);


// module.exports = withManifest({
//   manifest: {
//       name: "Immigration Topic",
//       short_name: "Immi Topic",
//       start_url: "/topics",
//       display: "standalone",
//       background_color: "#000",
//       theme_color: "#00bcd4",
//       // if src value is exist, icon image will be generated from src image, and ovwewritten
//       // icons value exist in the properties. if you want to keep your own icons path? do not pass
//       // src path to this plugin
//       icons: {
//         // source image path, to generate applications icons in 192x192, 512x512 sizes for manifest.
//         src: './public/splash-screen-owl.png',
//         // default is true, cache images until the hash value of source image has changed
//         // if false, generating new icon images while every build time.
//         cache: true
//       }
//   }
// })

module.exports = withOffline({
  workboxOpts: {
    importScripts: ['./static/sw.js']
  }
})