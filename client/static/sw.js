self.addEventListener('push', (event) => {
  const { title, body, url } = event.data && event.data.json() || {};
  const options = { 
    body,
    vibrate: [100, 50, 100],
    data: {
      url
    }
  };

  event.waitUntil(self.registration.showNotification(title, options));
});

self.addEventListener('notificationclick', (e) => {
  var notification = e.notification;
  var url = notification.data.url;
  var action = e.action;

  if (action === 'close') {
    notification.close();
  } else {
    clients.openWindow(url);
    notification.close();
  }
});

workbox.precaching.precacheAndRoute(self.__precacheManifest);