import React, { Component } from 'react';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';

import withRoot from '../lib/with-root';
import ResetPasswordForm from '../components/auth/reset-password-form';
import VerifyAccountForm from '../components/auth/verify-account-form';
import Page from '../components/page';
import { getUser } from '../redux/actions';


class IdentityManagementPage extends Component {
  static async getInitialProps({ store, isServer, query }) {
    const { action, token = '' } = query;
    switch(action) {
      case 'verify':
          token && await store.execSagaTasks(isServer, dispatch => {
            return dispatch(getUser({ query: { verifyToken: token } }));
          });
          break;
    }
    return { action, token };
  }

  render() {
    const { classes, action, token } = this.props;
    let pageTitle = 'Discussion.im: ';
    switch(action) {
      case 'verify':
          pageTitle = ': Verify Account'
          break;
      case 'reset':
        pageTitle = ': Reset Password'
        break;
    }
    return (
      <Page title="Reset Password">
        <div className={classes.container}>
          <ResetPasswordForm action={action} token={token} />
          <VerifyAccountForm action={action} token={token} />
        </div>
      </Page>
    );
  }
}

const styles = theme => ({
  container: {
    minHeight: '100vh',
    display: 'flex',
    background: '#c0f1fa;',
    background: 'url("/static/auth-page-background.jpg") center center / cover no-repeat fixed'
  }
});

const mapStateToProps = (state, props) => {
  return {};
};

const mapDispatchToProps = dispatch => {
  return {};
};

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  withRoot,
  withStyles(styles, { withTheme: true })
)(IdentityManagementPage);
