import React, { Component } from 'react';
import { compose } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import withRoot from '../lib/with-root';

import Page from '../components/page';
import DesktopTopicsScreen from "../screens/desktop/topics";
import DesktopExploreTopicsScreen from "../screens/desktop/explore-topics";
import AddTopicModal from '../components/modals/add-topic';
import MobileTopicsScreen from "../screens/mobile/topics";
import MobileDiscussionRoomScreen from "../screens/mobile/discussion-room";
import MobileTopicThreadScreen from '../screens/mobile/topic-thread';
import MobileUserProfileScreen from "../screens/mobile/user-profile";
import PageNotFound from "../components/404";
import AuthModal from '../components/modals/auth';
import ActionModal from '../components/modals/action-modal';

import {
  topicSelector,
  topicsSelector,
  topicMessageSelector,
  topicMessagesSelector,
  metaDataSelector,
  userSelector,
} from '../redux/selectors';

import {
  getTopics,
  setCurrentTopicUUID,
  getTopicMessages,
  login,
  getUser,
  setCurrentTopicMessageUUID,
  hightlightCurrentTopicMessage
} from '../redux/actions';

class TopicsPage extends Component {
  static async getInitialProps({ store, isServer, query }) {
    const { topicUUID, topicMessageUUID, userUUID } = query;
    const state = store.getState();

    if (topicUUID && (topicUUID.toLowerCase() === 'explore')) return { ...query }

    const currentTopicUUIDChanged = state.currentTopicUUID !== topicUUID;

    currentTopicUUIDChanged && await store.execSagaTasks(isServer, dispatch => {
      return dispatch(setCurrentTopicUUID(topicUUID));
    });

    const topicsPresent = !!(topicsSelector(state).length);
    (!topicsPresent) && await store.execSagaTasks(isServer, dispatch => {
      return dispatch(getTopics({ query: { latestRecords: true } }));
    });

    // Get topic details along with topic messages, sender and replies too.
    if (topicUUID) {
      const topicPresent = !!(topicSelector(state, { uuid: topicUUID }));
      const topicMessagesPresent = !!(topicMessagesSelector(state, { topicUUID }).length);
      (!topicPresent || !topicMessagesPresent) && await store.execSagaTasks(isServer, dispatch => {
        return dispatch(getTopics({ query: { uuid: topicUUID, fetchPlan: 'messages,messages.sender,messages.replies.sender', latestRecords: true } }));
      });
    }

    // Get topic message details long with topic messages, send and replies too.
    if (topicMessageUUID) {
      if (isServer) { // highlight the topic if we are rendering from server.
        await (store.execSagaTasks(isServer, dispatch => {
          return dispatch(hightlightCurrentTopicMessage(true));
        }));
      }
      
      const topicMessagePresent = !!(topicMessageSelector(state, { uuid: topicMessageUUID }));
      const repliesPresent = !!(topicMessagesSelector(state, { parentTopicMessageUUID: topicMessageUUID }).length);
      (!topicMessagePresent || !repliesPresent.length) && await store.execSagaTasks(isServer, dispatch => {
        dispatch(getTopicMessages({ query: { uuid: topicMessageUUID, fetchPlan: 'topic,sender,replies' } }));
      });
      await (store.execSagaTasks(isServer, dispatch => {
        return dispatch(setCurrentTopicMessageUUID(topicMessageUUID));
      }));
    }

    // Get user details whose profile needs to be shown.
    if (userUUID) {
      const userPresent = !!(userSelector(state, { uuid: userUUID }));
      (!userPresent) && await store.execSagaTasks(isServer, dispatch => {
        return dispatch(getUser({ query: { uuid: userUUID } }));
      });
    }

    return { ...query };
  }

  componentDidMount() {
    this.props.getLoggedInUser();
  }

  render() {
    const {
      topicUUID,
      topicMessageUUID,
      userUUID,
      topic,
      topics,
      topicMessages,
      topicMessage,
      topicMessageReplies,
      user
    } = this.props;
    let exploreTopics, showTopics, showTopicMessages, showTopicMessageReplies, showUserProfile, show404Page;

    if ((userUUID && !user) ||
      (topicUUID && (topicUUID !== 'explore') && !topic) ||
      (topicMessageUUID && !topicMessage))
      show404Page = true;

    if (userUUID)
      showUserProfile = true;
    else if (topicUUID)
      if (topicUUID.toLowerCase() === 'explore') {
        exploreTopics = true
      } else if (topicMessageUUID) {
        showTopicMessageReplies = true;
      } else {
        showTopicMessages = true;
      }
    else
      showTopics = true;

    if (show404Page) return (<PageNotFound />)
    return (
      <Page title={`Discussion.im - ${topic && topic.title || 'Topics'}`}>
        {!exploreTopics &&
          <DesktopTopicsScreen
            topics={topics}
            topic={topic}
            topicMessages={topicMessages}
            topicMessage={topicMessage}
            topicMessageReplies={topicMessageReplies}
            infoUser={user}
          />
        }

        {!!exploreTopics && <DesktopExploreTopicsScreen />}

        {showTopics && <MobileTopicsScreen topics={topics} />}
        {showTopicMessages && <MobileDiscussionRoomScreen topic={topic} topicMessages={topicMessages} />}
        {showTopicMessageReplies && <MobileTopicThreadScreen message={topicMessage} replies={topicMessageReplies} />}
        {showUserProfile && <MobileUserProfileScreen user={user} />}

        <AddTopicModal />
        <AuthModal />
        <ActionModal />
      </Page>
    );
  }
}

const mapStateToProps = (state, props) => {
  const { topicUUID, topicMessageUUID, userUUID } = props;
  const { loggedInUserUUID } = state;

  const topic = topicSelector(state, { uuid: topicUUID });

  const topicsMeta = metaDataSelector(state, { modelType: 'all', metaDataFor: 'topics' });
  let topicsFilter = {};
  topicsFilter['staredByUsersUUIDs'] = {};
  topicsFilter['staredByUsersUUIDs'][loggedInUserUUID] = true;
  let topics = topicsSelector(state, topicsFilter);
  topics = { ...topicsMeta, data: topics }

  const messagesMeta = metaDataSelector(state, { modelUUID: topicUUID, metaDataFor: 'messages' });
  let topicMessages = topicMessagesSelector(state, { topicUUID });
  topicMessages = topicMessages.filter((message) => !(message.parentTopicMessageUUID))
  if (topicMessages.length == 0) {
    if (topic) topicMessages = [];
    else topicMessages = undefined;
  }
  topicMessages = { ...messagesMeta, data: topicMessages };

  const topicMessage = topicMessageSelector(state, { uuid: topicMessageUUID });

  const topicMessageReplies = topicMessagesSelector(state, { parentTopicMessageUUID: topicMessageUUID }) || [];

  const user = userSelector(state, { uuid: userUUID })
  return {
    topic,
    topics,
    topicMessages,
    topicMessage,
    topicMessageReplies,
    user
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getLoggedInUser: () => dispatch(login())
  };
};

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  withRoot,
)(TopicsPage);
