import React, { Component } from 'react';
import { compose } from 'redux';
import { connect } from 'react-redux';
import withRoot from '../lib/with-root';
import { withStyles } from '@material-ui/core/styles';

import PropTypes from 'prop-types';
import Link from 'next/link';
import Center from 'react-center';

import {
  Grid,
  Avatar,
  Button,
  Typography
} from '@material-ui/core';

import {
  styleBigAvatar,
  styleRaisedButton,
  styleHomepageFeature,
  styleH1,
  evenSection,
  oddSection,
  mainSection,
  withLoveSection
} from '../lib/SharedStyles';
import Page from '../components/page';

import Particles from 'react-particles-js';
import { particlesOptions } from '../../config/particlesjs-config';

const styleTeamMember = {
  textAlign: 'center',
  padding: '10px 5%',
};

class HomePage extends Component {
  constructor(params) {
    super(params)
    this.state = {
      showParticles: false
    }
  }

  componentDidMount() {
    this.setState({ showParticles: true })
  }

  render() {
    const { classes } = this.props;

    return (
      <Page title="Discussion.im - Home Page">
        {this.state.showParticles && <Particles params={particlesOptions} className={classes.particles} />}

        <div style={{ fontSize: '15px' }}>
          <div style={mainSection}>
            <Center style={{ height: '100vh' }}>
              <Grid container direction="row" justify="space-around" align="flex-start">
                <Grid item sm={12} xs={12} style={{ textAlign: 'center', padding: '0 10px 0 10px' }}>
                  <Typography variant="headline" style={{ fontSize: '50px', color: '#ffffff' }}>
                    Join, Discuss & Build Communities
                  </Typography>

                  <p style={{ fontSize: '25px', color: '#ffffff' }}>
                    Free, blazingly fast & instant messaging (note .im in URL) app for group discussions.
                  </p>
                  <p>
                    Topics like Climate change, Work visa, Gardening, Parenting, Green Card, ReactJS, Machine Learning, Immigration, SAT Exam, Algebra, Painting, Photography and many more.
                  </p>
                  <p>
                    If you don't find your topic, create your own and build community around it. Its free for all !
                  </p>

                  <p style={{ textAlign: 'center' }}>
                    <Link
                      prefetch={false}
                      as="/topics/explore"
                      href="/topics/explore"
                    >
                      <Button variant="outlined" color="secondary" style={styleRaisedButton}>
                        Explore Topics
                      </Button>
                    </Link>
                    <Link
                      prefetch
                      as="/topics"
                      href="/topics"
                    >
                      <Button variant="outlined" color="secondary" style={styleRaisedButton}>
                        Take me to App
                      </Button>
                    </Link>
                  </p>
                </Grid>
              </Grid>
            </Center>
          </div>



          <div style={evenSection}>
            <h1 style={styleH1}>Features</h1>
            <Grid container direction="row" justify="space-around" align="flex-start">

              <Grid item md={3} sm={6} xs={12} style={styleHomepageFeature}>
                <Typography variant="title">
                  No limit on number of members
                </Typography>
                <p>
                  Technically there is no limit on number of members who can be participants in a topic. Any number of participants can be a part of any number of topics, so just feel free to engage in any number of topics.
                </p>
              </Grid>

              <Grid item md={3} sm={6} xs={12} style={styleHomepageFeature}>
                <Typography variant="title">
                  Subscribe without participating
                </Typography>
                <p>
                  Subscribe to get push notifications from topics of your interest, even if you don't participate in those topics. Remain informed of what people are discussing.
                </p>
              </Grid>

              <Grid item md={3} sm={6} xs={12} style={styleHomepageFeature}>
                <Typography variant="title">
                  Discuss with world
                </Typography>
                <p>
                  Topics are open realtime forums, so they are not limited to your friend's or contact list as in case of WhatsApp or FB Messenger. Anyone in the world can read and contribute to the topics, just like Forums.
                </p>
              </Grid>

              <Grid item md={3} sm={6} xs={12} style={styleHomepageFeature}>
                <Typography variant="title">
                  Experience of native app
                </Typography>
                <p>
                  Blazingly fast as native app. Discussion.im is the PWA (<Link href="https://developers.google.com/web/progressive-web-apps/">Progressive Web App</Link>). It will give you an option to install once you access it in your mobile browser. Once installed, it will work like mobile app, not as mobile web.
                </p>
              </Grid>

              <Grid item md={3} sm={6} xs={12} style={styleHomepageFeature}>
                <Typography variant="title">
                  Realtime communication
                </Typography>
                <p>
                  Don't wait for email notifications to have an update about your thread. If you are active in app, you will get the messages in realtime. If you are not active, but subscribed to a topic, you will get realtime push notification messages.
                </p>
              </Grid>

              <Grid item md={3} sm={6} xs={12} style={styleHomepageFeature}>
                <Typography variant="title">
                  Quick access through social logins
                </Typography>
                <p>
                  Login using Google, Facebook, Twitter, Github or your user credentials. If you are just a silent reader and wants to know what people are discussing, you dont even need to login. Topics are open for public to read.
                </p>
              </Grid>
            </Grid>
          </div>



          <div style={oddSection}>
            <h1 style={styleH1}>Get Started</h1>
            <Grid container direction="row" justify="space-around" align="flex-start">
              <Grid item sm={6} xs={12} style={styleHomepageFeature}>
                <Typography variant="title" style={{ color: '#ffffff' }}>
                  Explore Topics
                </Typography>
                <p>
                  You can either search the topics that may be of your interest or you may browse the topics and subscribe them to get push notofication for updates on that topic and relted threads. In case you do not find topic of your interest, feel free to create a new topic.
                </p>
                <p style={{ textAlign: 'center' }}>
                  <Link
                    prefetch={false}
                    as="/topics/explore"
                    href="/topics/explore"
                  >
                    <Button variant="outlined" color="secondary" style={styleRaisedButton}>
                      Search Topics
                      </Button>
                  </Link>
                  <Link
                    prefetch
                    as="/topics"
                    href="/topics"
                  >
                    <Button variant="outlined" color="secondary" style={styleRaisedButton}>
                      Browse Topics
                      </Button>
                  </Link>
                </p>
              </Grid>

              <Grid item sm={6} xs={12} style={styleHomepageFeature}>
                <Typography variant="title" style={{ color: '#ffffff' }}>
                  Add topic to your list
                </Typography>
                <p>
                  Star the topics of your interest to add them to your list. Anytime access the list of your topics from navigation menu. You can unstar to remove the topic from your list.
                </p>
              </Grid>

              <Grid item sm={6} xs={12} style={styleHomepageFeature}>
                <Typography variant="title" style={{ color: '#ffffff' }}>
                  Get notified in realtime
                </Typography>
                <p>
                  Whenever you post to a topic or related thread, you will get notified in case someone responds to you. Even without posting, you can subscribe / unsubscribe to a topic and thread by clicking to bell icon.
                </p>
              </Grid>
            </Grid>
          </div>



          <div>
            <p style={withLoveSection}>
              Built with <span role="img" aria-label="Love">❤️</span> by the <a href="/">Discussion.im</a> team.
          </p>
          </div>



          {/* <div style={evenSection}>
            <h1 style={styleH1}>The Team</h1>
            <div style={{ textAlign: 'center' }}>
              Together, we&apos;ve built
              <a
                href="https://github.com/builderbook/builderbook"
                target="_blank"
                rel="noopener noreferrer"
              >
                {' '}
                Builder Book
              </a>{' '}
              and
              <a href="https://findharbor.com" target="_blank" rel="noopener noreferrer">
                {' '}
                Harbor
              </a>. Stay tuned for
              <a href="https://github.com/async-labs/async-saas" target="_blank" rel="noopener noreferrer">
                {' '}
                Async
              </a>.
            </div>
            <br />
            <Grid container direction="row" justify="space-around" align="flex-start">
              <Grid item sm={4} xs={12} style={styleTeamMember}>
                <Avatar
                  src="https://storage.googleapis.com/builderbook/timur-picture.png"
                  style={styleBigAvatar}
                  alt="Timur Zhiyentayev"
                />
                <p>
                  <a href="https://github.com/tima101" target="_blank" rel="noopener noreferrer">
                    Timur Zhiyentayev
                  </a>
                  <br />
                  Vancouver, WA
                </p>
                <p>
                  Tima is a JavaScript web developer. He likes learning any technology that improves
                  end-user experience.
                </p>
              </Grid>
              <Grid item sm={4} xs={12} style={styleTeamMember}>
                <Avatar
                  src="https://storage.googleapis.com/builderbook/kelly-picture.png"
                  style={styleBigAvatar}
                  alt="Kelly Burke"
                />
                <p>
                  <a href="https://github.com/klyburke" target="_blank" rel="noopener noreferrer">
                    Kelly Burke
                  </a>
                  <br />
                  Vancouver, WA
                </p>
                <p>
                  Kelly is a front-end developer. She likes using React, Material Design, and VS editor
                  and enjoys solving UX problems.
                </p>
              </Grid>
              <Grid item sm={4} xs={12} style={styleTeamMember}>
                <Avatar
                  src="https://storage.googleapis.com/builderbook/delgermurun-picture.png"
                  style={styleBigAvatar}
                  alt="Delgermurun Purevkhuu"
                />
                <p>
                  <a href="https://github.com/delgermurun" target="_blank" rel="noopener noreferrer">
                    Delgermurun Purevkhuu
                  </a>
                  <br />
                  Ulaanbaatar, Mongolia
                </p>
                <p>
                  Del is a back-end developer. He has built many production-ready web apps with JavaScript
                  and Python.
                </p>
              </Grid>
            </Grid>
          </div> */}
        </div>
        {/* <Footer /> */}
      </Page>
    )
  }
}

const styles = theme => ({
  particles: {
    backgroundColor: '#424242',//theme.palette.primary.main
    position: 'absolute',
    width: '100%',
    height: '100%',
    backgroundRepeat: 'no-repeat',
    backgroundSize: 'cover',
    backgroundPosition: '50% 50%',
    opacity: 0.35
  }
});

const mapStateToProps = (state, props) => {
  return {};
};

const mapDispatchToProps = dispatch => {
  return {};
};

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  withStyles(styles, { withTheme: true }),
  withRoot,
)(HomePage);
