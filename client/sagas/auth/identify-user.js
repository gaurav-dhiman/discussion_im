import { call, put } from 'redux-saga/effects';
import { service } from '../../lib/api';
import { showLoginForm, showSignupForm } from '../../redux/actions';
import { handleError } from '../handle-error';
import { find } from '../lib/api';

export function* getUserByEmailId(action) {
  let user;
  try {
    user = yield call(find, service.users, action.params);
    user = user && user.data && user.data[0] || {};
    if(user.uuid) yield put(showLoginForm(user));
    else yield put(showSignupForm(action.params.query.email));
  } catch (error) {
    yield handleError(error, 'Failed to get the user details using email id.', action);
  }
}

