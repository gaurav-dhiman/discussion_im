import { call, put, select, all } from 'redux-saga/effects';

import api from '../../lib/api';
import { getLoggedInUser, showSnackBarNotification, clearPostLoginActions } from '../../redux/actions';
import { handleError } from '../handle-error';

async function loginUser(loginCred) {
  return await api.authenticate(loginCred);
}

async function verifyJWT(accessToken) {
  return await api.passport.verifyJWT(accessToken);
}

function getPostLoginActions(state) {
  return state.postLoginActions;
}

export function* login(action) {
  let res, payload, displayMessage;
  try {
    res = yield call(loginUser, action.loginCred);
    if (res.accessToken) {
      payload = yield call(verifyJWT, res.accessToken);
      if (payload.userId) {
        yield put(getLoggedInUser(payload.userId));
        api.set('lc', action.loginCred);
        // If anything needs to be done after login, fire those actions
        const postLoginActions = yield select(getPostLoginActions);
        if (postLoginActions.length) {
          yield all(postLoginActions.map(action => put(action)));
          yield put(clearPostLoginActions());
        }
      } else {
        let displayMessage = 'Could not identify you with given credentials. Try again.';
        yield put(showSnackBarNotification(displayMessage, { details: res, type: 'error' }));
      }
    } else {
      // no accessToken
      let displayMessage = 'Oops, things did not work. Try again or report an issue.';
      yield put(showSnackBarNotification(displayMessage, { details: res, type: 'error' }));
    }
  } catch (error) {
    switch (error.code) {
      case 401:
        if (!action.loginCred) return;
        displayMessage = 'Invalid login credentials. Try again.';
        yield put(showSnackBarNotification(displayMessage, { details: error, type: 'error' }));
        break;
      default:
        yield handleError(error, 'Failed to login', action);
        break;
    }
  }
}
