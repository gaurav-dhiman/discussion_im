import { call, put } from 'redux-saga/effects';
import { service } from '../../lib/api';
import { handleError } from '../handle-error';
import { showSnackBarNotification } from '../../redux/actions';
import { create } from '../lib/api';

export function* sendVerifyAccount(action) {
  try {
    yield put(showSnackBarNotification('Sending verification email to you. Please check your SPAM folder too, just in case it lands there.', { type: 'info' }));
    yield call(create, service.authManagement, action.payload);
    yield put(showSnackBarNotification('An email has been sent to you. Please follow instruction in it to verify your account.', { type: 'info' }));
  } catch (error) {
    yield handleError(error, 'Failed to send email verification email. Please contact support team.', action);
  }
}

export function* verifyAccount(action) {
  try {
    yield call(create, service.authManagement, action.payload);
    yield put(showSnackBarNotification('Your account has been verified successfully.', { type: 'info' }));
  } catch (error) {
    yield handleError(error, 'Failed to verify your account. Please contact support team.', action);
  }
}
