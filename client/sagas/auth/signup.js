import { call, put } from 'redux-saga/effects';
import { service } from '../../lib/api';
import { login } from '../../redux/actions';
import { handleError } from '../handle-error';
import { create } from '../lib/api';

export function* signUp(action) {
  let user, displayMessage
  try {
    user = yield call(create, service.users, action.user);
    // transparently login the recently signedup user.
    yield put(login({
      email: action.user.email,
      password: action.user.password,
      strategy: 'local'
    }));
  } catch (error) {
    yield handleError(error, 'Failed to sign up', action);
  }
}

