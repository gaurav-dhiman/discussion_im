import { call, put } from 'redux-saga/effects';
import { service } from '../../lib/api';
import { handleError } from '../handle-error';
import { showSnackBarNotification } from '../../redux/actions';
import { create } from '../lib/api';

export function* sendResetPassword(action) {
  try {
    yield put(showSnackBarNotification('Sending email with details to reset password.', { type: 'info' }));
    yield call(create, service.authManagement, action.payload);
    yield put(showSnackBarNotification('An email has been sent to you. Please follow instruction given in it.', { type: 'info' }));
  } catch (error) {
    yield handleError(error, 'Failed to send email for resetting password. Please contact support team.', action);
  }
}

export function* resetPassword(action) {
  try {
    yield call(create, service.authManagement, action.payload);
    yield put(showSnackBarNotification('Your password has been reset successfully.', { type: 'info' }));
  } catch (error) {
    yield handleError(error, 'Failed to reset the password. Please contact support team.', action);
  }
}

