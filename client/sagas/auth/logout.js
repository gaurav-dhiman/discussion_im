import { call, put } from 'redux-saga/effects';
import api from '../../lib/api';
import { unsetLoggedInUserUUID } from '../../redux/actions';
import { handleError } from '../handle-error';
import { showSnackBarNotification } from '../../redux/actions';

async function logoutUser() {
  return await api.logout();
}

export function* logout(action) {
  try {
    yield call(logoutUser);
    yield put(unsetLoggedInUserUUID());
    localStorage.removeItem('feathers-jwt');
    yield put(showSnackBarNotification('You have been logged-out successfully.', { type: 'info' }));
  } catch (error) {
    yield handleError(error, 'Failed to logout', action);
  }
}

