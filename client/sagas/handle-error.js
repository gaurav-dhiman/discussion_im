import { put } from 'redux-saga/effects';
import api from '../lib/api';
import { showSnackBarNotification, addPostLoginAction, login, showAuthModal } from '../redux/actions';

export function* handleError(error, defaultErrorMessage = '', action) {
  let displayMessage;
  switch (error.code) {
    case 401: // Authentication Error
      if (action) yield put(addPostLoginAction(action));
      let loginCred = api.get('lc');
      if (!loginCred) {
        // show login dialog
        yield put(showAuthModal());
        displayMessage = 'Please login to continue.';
        yield put(showSnackBarNotification(displayMessage, { type: 'info' }));
      } else {
        // try to login transparently
        yield put(login(loginCred));
      };
      return;
    case 404: // Not Found Error
      displayMessage = 'Requested resource is not found.';
      break;
    case 408: // Timeout Error
      displayMessage = 'Failed to connect to server. Check your internet connection.';
      break;
    case 406: // Not-Acceptable
      displayMessage = defaultErrorMessage + ' Try again or report an issue.';
      if (error.data && error.data.errSubType === 'user-already-exists')
        displayMessage = error.message || displayMessage;
      break;
    default:
      displayMessage = defaultErrorMessage + ' Try again or report an issue.';
      if (error && error.message) displayMessage += ` Error Message: ${error.message}`;
      break;
  }
  error.action = action;
  yield put(showSnackBarNotification(displayMessage, { details: error, type: 'error' }));
  console.log(error);
}