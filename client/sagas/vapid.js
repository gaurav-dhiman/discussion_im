import { call } from 'redux-saga/effects';
import { service } from '../lib/api';
import { handleError } from './handle-error';
import { find } from './lib/api';

export function* getPublicVapidKey(action) {
  let loggedInUser;
  try {
    window.publicVapidKey = yield call(find, service.vapidKeys, { 'query': { 'type': 'public' } });
  } catch (error) {
    yield handleError(error, 'Failed to get VAPID Key from server.');
  }
}