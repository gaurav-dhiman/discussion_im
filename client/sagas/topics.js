import { call, put, select } from 'redux-saga/effects';
import { service } from '../lib/api';
import { showSnackBarNotification, addTopicsToReduxStore } from '../redux/actions';
import { subscribe as subscribeToWebPush, unsubscribe as unsubscribeFromWebPush } from '../lib/web-push';
import { handleError } from "./handle-error";
import { create, find, patch } from './lib/api';
export const getLoggedInUserUUID = (state) => state.loggedInUserUUID;

export function* getTopics(action) {
  try {
    let topics = yield call(find, service.topics, action.query);
    action.metaContext = action.metaContext || {};
    topics = { ...topics, ...action.metaContext };
    // Update metadata only if we are doing infinite scroll
    if (action.query.query.$text) action.skipUpdatingMetaData = true;
    yield put(addTopicsToReduxStore(topics, action.skipUpdatingMetaData));
  } catch (error) {
    yield handleError(error, 'Failed to get topics', action);
  }
}

export function* createTopic(action) {
  try {
    yield call(create, service.topics, action.topic);
  } catch (error) {
    yield handleError(error, 'Failed to create new topic', action);
  }
}

export function* starTopic(action) {
  try {
    yield call(patch, service.topics, null, action.data, action.params);
  } catch (error) {
    yield handleError(error, 'Failed to star the topic', action);
  }
}

export function* unstarTopic(action) {
  try {
    yield call(patch, service.topics, null, action.data, action.params);
  } catch (error) {
    yield handleError(error, 'Failed to un-star the topic', action);
  }
}

export function* subscribeToTopic(action) {
  const { dispatch, subscribeTo } = action;
  let loggedInUserUUID = yield select(getLoggedInUserUUID);
  const displayMessage = 'Subscribed successfully. You will start receiveing notifications for this topic.'

  try {
    const subscription = yield call(subscribeToWebPush, dispatch, loggedInUserUUID, subscribeTo);
    if (subscription) yield put(showSnackBarNotification(displayMessage, { type: 'info' }));
  } catch (error) {
    yield handleError(error, 'Failed to subscribe to topic', action);
  }
}

export function* unsubscribeFromTopic(action) {
  const { dispatch, unsubscribeFrom } = action;
  let loggedInUserUUID = yield select(getLoggedInUserUUID);
  const displayMessage = 'Unsubscribed successfully. You will not receive notifications for this topic anymore.'

  try {
    yield call(unsubscribeFromWebPush, dispatch, loggedInUserUUID, unsubscribeFrom);
    yield put(showSnackBarNotification(displayMessage, { type: 'info' }));

  } catch (error) {
    yield handleError(error, 'Failed to unsubscribe from topic', action);
  }
}
