import { call, put } from 'redux-saga/effects';
import api, { service } from '../lib/api';
import { setLoggedInUserUUID, hideAuthModal, addUsersToReduxStore } from '../redux/actions';
import { handleError } from './handle-error';
import { create, find, get, patch } from './lib/api';

export function* getLoggedInUser(action) {
  let loggedInUser;
  try {
    loggedInUser = yield call(get, service.users, action.userId);
    yield put(addUsersToReduxStore(loggedInUser));
    yield put(setLoggedInUserUUID(loggedInUser.uuid));
    yield put(hideAuthModal());
  } catch (error) {
    yield handleError(error, 'Failed to get user details from server.');
  }
}

export function* getUser(action) {
  try {
    const users = yield call(find, service.users, action.query);
    yield put(addUsersToReduxStore(users.data[0]));
  } catch (error) {
    yield handleError(error, 'Failed to get user details from server.', action);
  }
}

export function* saveUserProfileImage(action) {
  try {
    const user = yield call(create, service.profileImage, action.data);
    yield put(addUsersToReduxStore(user));
  } catch (error) {
    yield handleError(error, "Failed to save profile image.", action);
  }
}

export function* saveUserProfile(action) {
  try {
    let user = yield call(patch, service.users, null, action.profile, action.params);
    user = user[0];
    yield put(addUsersToReduxStore(user));
  } catch (error) {
    yield handleError(error, "Failed to save profile details.", action);
  }
}
