import { call, put } from 'redux-saga/effects';
import { service } from '../lib/api';
import { addTopicMessagesToReduxStore } from '../redux/actions';
import { handleError } from "./handle-error";
import { create, find, patch } from './lib/api';

export function* getTopicMessages(action) {
  try {
    let topicMessages = yield call(find, service.topicMessages, action.query);
    action.metaContext = action.metaContext || {};
    topicMessages = { ...topicMessages, ...action.metaContext };
    // Update metadata only if we are doing infinite scroll
    if (action.query.query.$text) action.skipUpdatingMetaData = true;
    yield put(addTopicMessagesToReduxStore(topicMessages, action.skipUpdatingMetaData));
  } catch (error) {
    yield handleError(error, 'Failed to get messages', action);
  }
}

export function* sendTopicMessage(action) {
  try {
    const topicMessages = yield call(create, service.topicMessages, action.topicMessage);
    yield put(addTopicMessagesToReduxStore([topicMessages]));
  } catch (error) {
    yield handleError(error, 'Failed to send message', action);
  }
}

export function* pinTopicMessage(action) {
  try {
    yield call(patch, service.topicMessages, null, action.data, action.params);
  } catch (error) {
    yield handleError(error, 'Failed to pin the message', action);
  }
}

export function* unpinTopicMessage(action) {
  try {
    yield call(patch, service.topicMessages, null, action.data, action.params);
  } catch (error) {
    yield handleError(error, 'Failed to un-pin the message', action);
  }
}

export function* likeTopicMessage(action) {
  try {
    yield call(patch, service.topicMessages, null, action.data, action.params);
  } catch (error) {
    yield handleError(error, 'Failed to like the message', action);
  }
}

export function* unlikeTopicMessage(action) {
  try {
    yield call(patch, service.topicMessages, null, action.data, action.params);
  } catch (error) {
    yield handleError(error, 'Failed to remove like from the message', action);
  }
}
