import { actionTypes } from '../redux/actions';
import { takeEvery } from 'redux-saga/effects';

// Saga handlers
import { getTopics, createTopic, starTopic, unstarTopic, subscribeToTopic, unsubscribeFromTopic } from "./topics";
import { getPublicVapidKey } from "./vapid";
import { getUserByEmailId, signUp, login, logout, sendResetPassword, resetPassword, sendVerifyAccount, verifyAccount } from "./auth";
import { getLoggedInUser, getUser, saveUserProfileImage, saveUserProfile } from './user';
import { getTopicMessages, sendTopicMessage, pinTopicMessage, unpinTopicMessage, likeTopicMessage, unlikeTopicMessage } from './topic-messages';

export default function* sagas() {
  yield takeEvery(actionTypes.GET_PUBLIC_VAPID_KEY, getPublicVapidKey);

  yield takeEvery(actionTypes.AUTH_GET_USER_BY_EMAIL_ID, getUserByEmailId);
  yield takeEvery(actionTypes.SIGNUP, signUp);
  yield takeEvery(actionTypes.LOGIN, login);
  yield takeEvery(actionTypes.LOGOUT, logout);

  yield takeEvery(actionTypes.SEND_RESET_PASSWORD, sendResetPassword);
  yield takeEvery(actionTypes.RESET_PASSWORD, resetPassword);
  yield takeEvery(actionTypes.SEND_VERIFY_ACCOUNT, sendVerifyAccount);
  yield takeEvery(actionTypes.VERIFY_ACCOUNT, verifyAccount);

  yield takeEvery(actionTypes.GET_TOPICS, getTopics);
  yield takeEvery(actionTypes.CREATE_TOPIC, createTopic);
  yield takeEvery(actionTypes.STAR_TOPIC, starTopic);
  yield takeEvery(actionTypes.UNSTAR_TOPIC, unstarTopic);
  yield takeEvery(actionTypes.SUBSCRIBE_TO_TOPIC, subscribeToTopic);
  yield takeEvery(actionTypes.UNSUBSCRIBE_FROM_TOPIC, unsubscribeFromTopic);

  yield takeEvery(actionTypes.GET_TOPIC_MESSAGES, getTopicMessages);
  yield takeEvery(actionTypes.SEND_TOPIC_MESSAGE, sendTopicMessage);
  yield takeEvery(actionTypes.PIN_TOPIC_MESSAGE, pinTopicMessage);
  yield takeEvery(actionTypes.UNPIN_TOPIC_MESSAGE, unpinTopicMessage);
  yield takeEvery(actionTypes.LIKE_TOPIC_MESSAGE, likeTopicMessage);
  yield takeEvery(actionTypes.UNLIKE_TOPIC_MESSAGE, unlikeTopicMessage);

  yield takeEvery(actionTypes.GET_LOGGEDIN_USER, getLoggedInUser);
  yield takeEvery(actionTypes.GET_USER, getUser);
  yield takeEvery(actionTypes.SAVE_USER_PROFILE_IMAGE, saveUserProfileImage);
  yield takeEvery(actionTypes.SAVE_USER_PROFILE, saveUserProfile);
}