import api from '../../lib/api';

export async function create(service, data, params) {
  return await api.service(service).create(data, params);
}

export async function find(service, params) {
  return await api.service(service).find(params);
}

export async function get(service, id, params) {
  return await api.service(service).get(id, params);
}

export async function update(service, id, data, params) {
  return await api.service(service).update(id, data, params);
}

export async function patch(service, id, data, params) {
  return await api.service(service).patch(id, data, params);
}

export async function remove(service, id, params) {
  return await api.service(service).remove(id, params);
}
