import React, { Component, Fragment } from 'react';
import { compose } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Avatar from 'react-avatar';
import Grid from '@material-ui/core/Grid';
import ListItem from '@material-ui/core/ListItem';
import SelectImageButton from './select-image-button';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/DeleteOutline';
import SaveIcon from '@material-ui/icons/Save';
import LeftRotateIcon from '@material-ui/icons/RotateLeftOutlined';
import RightRotateIcon from '@material-ui/icons/RotateRightOutlined';

import AvatarEditor from 'react-avatar-editor'
import Slider from '@material-ui/lab/Slider';
import { saveUserProfileImage } from '../../redux/actions';
import imageCompression from 'browser-image-compression';
import { dataURLToBlob, blobToDataURL } from 'blob-util'


class ProfileAvatar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      zoomSilderValue: 0,
      rotationDegree: 0
    };
  }

  _handleFileChange = (dataURI, blob) => {
    this.setState({
      image: dataURI,
      imageBlob: blob
    });
  }

  _zoomInOutImage = (event, zoomSilderValue) => {
    const imageScale = zoomSilderValue / 50 + 1;
    this.setState({
      imageScale,
      zoomSilderValue
    });
  }

  _rotateImage = (rotationDegree) => () => {
    rotationDegree = this.state.rotationDegree + rotationDegree;
    this.setState({
      rotationDegree
    });
  }

  _removeImage = () => {
    this.setState({
      imageBlob: undefined
    })
  }

  _saveImage = () => {

    if (this.editor) this.editor.getImage().toBlob(async (image) => {
      if (image) {
        image = await imageCompression(image, 0.2, 300);
        image = await blobToDataURL(image);

        this.props.saveUserProfileImage(image);
        this.state.imageBlob = undefined;
      }
    });
  }

  render() {
    const { classes, user, loggedInUserUUID } = this.props;
    const { imageScale, zoomSilderValue, rotationDegree, image, imageBlob } = this.state;
    const showProfileImage = !imageBlob && !!user.profileImageURL;
    const showDefaultImage = !imageBlob && !user.profileImageURL;
    const showImageEditor = !!imageBlob;
    let showEditImageButton = (user.uuid === loggedInUserUUID);

    return (
      <Fragment>
        <ListItem className={classes.box}>
          <Grid container justify="center" alignItems="center" className={classes.grid} >
            {showProfileImage &&
              <Avatar name={user.displayName} src={user.profileImageURL} size="300" round={true} />}

            {showDefaultImage &&
              <Avatar name={user.displayName} textSizeRatio={1.5} size="300" round={true} color='lightblue' />}

            {(showProfileImage || showDefaultImage) && showEditImageButton &&
              < SelectImageButton handleFileChange={this._handleFileChange} />}

            {showImageEditor &&
              <Fragment>
                <AvatarEditor
                  ref={(editor) => this.editor = editor}
                  image={image}
                  width={300}
                  height={300}
                  scale={imageScale}
                  border={0}
                  rotate={rotationDegree}
                  borderRadius={200}
                />

                <IconButton className={classes.deleteButton} color={'secondary'} aria-label="Delete" onClick={this._removeImage}>
                  <DeleteIcon />
                </IconButton>

                <IconButton className={classes.saveButton} color={'secondary'} aria-label="Delete" onClick={this._saveImage}>
                  <SaveIcon />
                </IconButton>

                <IconButton className={classes.leftRotateButton} color={'secondary'} aria-label="Delete" onClick={this._rotateImage(-90)}>
                  <LeftRotateIcon />
                </IconButton>

                <IconButton className={classes.rightRotateButton} color={'secondary'} aria-label="Delete" onClick={this._rotateImage(90)}>
                  <RightRotateIcon />
                </IconButton>

              </Fragment>
            }
          </Grid>
        </ListItem>

        {showImageEditor && <Slider
          classes={{ container: classes.slider }}
          value={zoomSilderValue}
          aria-labelledby="avatar-scale-slider"
          onChange={this._zoomInOutImage}
        />}

      </Fragment>
    );
  }
}

ProfileAvatar.propTypes = {
  user: PropTypes.object.isRequired
};

const styles = {
  avatar: {
    margin: 10,
  },
  bigAvatar: {
    position: 'relative',
    width: '90%',
    height: '90%',
  },
  box: {
    background: '#f9f9f9',
    position: 'relative',
    width: '100%',
    padding: '0px'
  },
  grid: {
    height: '339px'
  },
  slider: {
    width: '85%',
    margin: 'auto',
    paddingTop: '20px',
    paddingBottom: '20px'
  },
  deleteButton: {
    position: 'absolute',
    bottom: '20px',
    left: '50px'
  },
  saveButton: {
    position: 'absolute',
    bottom: '20px',
    right: '50px'
  },
  leftRotateButton: {
    position: 'absolute',
    top: '20px',
    left: '50px'
  },
  rightRotateButton: {
    position: 'absolute',
    top: '20px',
    right: '50px'
  }
};

ProfileAvatar.propTypes = {
  classes: PropTypes.object.isRequired,
};

const mapStateToProps = state => {
  return {
    loggedInUserUUID: state.loggedInUserUUID
  };
};

const mapDispatchToProps = dispatch => {
  return {
    saveUserProfileImage: (imageFile) => dispatch(saveUserProfileImage(imageFile))
  };
};

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  withStyles(styles, { withTheme: true })
)(ProfileAvatar);