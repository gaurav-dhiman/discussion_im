import React, { Component, Fragment } from 'react';
import { compose } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';

import ProfileAvatar from './avatar';

class ViewUserProfile extends Component {

  render() {
    const { classes, user, isUserSelf } = this.props;
    const notAvailable = 'Not Avaialable';

    return (
      <div className={classes.scroll}>
        <ProfileAvatar user={user} />
        <Divider />
        <ListItem>
          <ListItemText secondary={user.status || 'No status set'} />
        </ListItem>
        <Divider />
        <ListItem>
          <ListItemText primary={'First Name'} secondary={user.firstName || notAvailable} />
        </ListItem>
        <ListItem>
          <ListItemText primary={'Last Name'} secondary={user.lastName || notAvailable} />
        </ListItem>
        {isUserSelf &&
          <Fragment>
            <ListItem>
              <ListItemText primary={'Email ID'} secondary={user.email || notAvailable} />
            </ListItem>
            <ListItem>
              <ListItemText primary={'Phone No.'} secondary={user.phone || notAvailable} />
            </ListItem>
          </Fragment>
        }
      </div>
    );
  }
}

const styles = theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
    textAign: 'center'
  },
  fullWidth: {
    width: '100%'
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: '90%',
    padding: 'auto'
  },
  button: {
    margin: theme.spacing.unit,
    width: '90%'
  },
  scroll: {
    overflowY: 'auto',
    background: '#f9f9f9',
  }
});

const mapStateToProps = (state, props) => {
  return {
    isUserSelf: state.loggedInUserUUID == props.user.uuid
  };
};

const mapDispatchToProps = dispatch => {
  return {};
};

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  withStyles(styles, { withTheme: true })
)(ViewUserProfile);
