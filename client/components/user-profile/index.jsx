import React, { Component } from 'react';
import { compose } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

import Hidden from '@material-ui/core/Hidden'
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import EditIcon from '@material-ui/icons/Edit';
import CancelIcon from '@material-ui/icons/Clear';
import DeepLink from '../deep-link';
import ViewUserProfile from './view-profile-details';
import EditUserProfile from './edit-profile-details';

// import { withRouter } from 'next/router'

class UserProfile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showEditUserProfileForm: false
    };
  }

  _editUserProfile = () => {
    this.setState({ showEditUserProfileForm: true });
  }

  _cancelEdit = () => {
    this.setState({ showEditUserProfileForm: false });
  }

  render() {
    const { classes, user, loggedInUserUUID, onSave } = this.props;
    const showEditUserProfileForm = this.props.showEditUserProfileForm || this.state.showEditUserProfileForm;
    let showEditButton = (user.uuid === loggedInUserUUID) && !showEditUserProfileForm;
    const showCancelButton = (user.uuid === loggedInUserUUID) && showEditUserProfileForm;

    return (
      <List className={classes.list}>
        <Hidden smDown implementation="css">
          <div className={classes.headerIconDiv}>
            {showEditButton &&
              <IconButton
                color="inherit"
                aria-label="Edit Profile"
                onClick={this._editUserProfile}
              >
                <EditIcon fontSize="small" />
              </IconButton>
            }

            {showCancelButton &&
              <IconButton
                color="inherit"
                aria-label="Cancel Edit"
                onClick={this._cancelEdit}
              >
                <CancelIcon fontSize="small" />
              </IconButton>
            }

            <div className={classes.grow} />

            <DeepLink type='topic'>
              <IconButton
                color="inherit"
                aria-label="Collapse"
              >
                <ChevronRightIcon fontSize="small" />
              </IconButton>
            </DeepLink>
          </div>
          <Divider />
        </Hidden>

        {!showEditUserProfileForm && <ViewUserProfile user={user} />}
        {showEditUserProfileForm && <EditUserProfile user={user} onSave={onSave || this._cancelEdit} />}
      </List>
    );
  }
}

UserProfile.propTypes = {
  user: PropTypes.object.isRequired,
  showEditUserProfileForm: PropTypes.bool,
  onSave: PropTypes.func
};

const styles = theme => ({
  headerIconDiv: {
    display: "flex",
    justifyContent: "space-between",
    backgroundColor: "#a4a4a4"
  },
  grow: {
    flexGrow: 1,
  },
  flex: {
    flex: '1 1 auto'
  },
  rightMostAppBarButton: {
    marginLeft: 20,
    marginRight: -12
  },
  list: {
    paddingTop: '0',
    background: '#f9f9f9',
    display: 'flex',
    flexDirection: 'column',
    height: '100%'
  },
  avatar: {
    padding: '5px'
  },
  editProfileMsg: {
    padding: '3%'
  }
});

const mapStateToProps = state => {
  return {
    loggedInUserUUID: state.loggedInUserUUID
  };
};

const mapDispatchToProps = dispatch => {
  return {};
};

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  // withRouter,
  withStyles(styles, { withTheme: true })
)(UserProfile);
