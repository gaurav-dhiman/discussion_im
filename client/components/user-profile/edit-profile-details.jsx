import React, { Component } from 'react';
import { compose } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { saveUserProfile } from '../../redux/actions';

class EditUserProfile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ...props.user
    };
  }

  _saveProfile = () => {
    const {
      firstName,
      lastName,
      phone,
      status
    } = this.state;
    const profile = {
      firstName,
      lastName,
      phone,
      status
    }
    this.props.saveProfile(profile, this.props.user.uuid);
    this.props.onSave();
  }

  _handleChange = name => event => {
    this.setState({
      [name]: event.target.value,
    });
  };

  render() {
    const { classes } = this.props;
    const { firstName, lastName, phone, status } = this.state;

    return (
      <form className={classes.container} noValidate autoComplete="off">
        <center className={classes.fullWidth}>
          <TextField
            id="first-name"
            label="First Name"
            required
            className={classes.textField}
            value={firstName || ''}
            onChange={this._handleChange('firstName')}
            margin="normal"
          />
          <TextField
            id="last-name"
            label="Last Name"
            required
            className={classes.textField}
            value={lastName || ''}
            onChange={this._handleChange('lastName')}
            margin="normal"
          />
          <TextField
            id="phone-number"
            label="Phone No."
            className={classes.textField}
            value={phone || ''}
            onChange={this._handleChange('phone')}
            margin="normal"
          />
          <TextField
            id="status"
            label="Status"
            className={classes.textField}
            value={status || ''}
            onChange={this._handleChange('status')}
            margin="normal"
          />
          <Button color='primary' variant='contained' className={classes.button} onClick={this._saveProfile}>
            Save
          </Button>
        </center>
      </form>
    );
  }
}

EditUserProfile.propTypes = {
  user: PropTypes.object.isRequired,
  onSave: PropTypes.func.isRequired
};

const styles = theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
    textAign: 'center'
  },
  fullWidth: {
    width: '100%'
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: '90%',
    padding: 'auto'
  },
  button: {
    margin: theme.spacing.unit,
    width: '90%'
  }
});

const mapStateToProps = state => {
  return {};
};

const mapDispatchToProps = dispatch => {
  return {
    saveProfile: (profile, userUUID) => dispatch(saveUserProfile(profile, userUUID))
  };
};

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  withStyles(styles, { withTheme: true })
)(EditUserProfile);
