import React, { Component, Fragment } from 'react';
import { compose } from 'redux';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import ReactDom from "react-dom";

import IconButton from '@material-ui/core/IconButton';
import PhotoCameraIcon from '@material-ui/icons/PhotoCameraOutlined';

class SelectImageButton extends Component {

  _handleFile = event => {
    const reader = new FileReader();
    const file = event.target.files[0];

    if (!file) return;
    // this.props.handleFileChange(file);

    reader.onload = (img) => {
      ReactDom.findDOMNode(this.refs.in).value = '';
      this.props.handleFileChange(img.target.result, file);
    };
    reader.readAsDataURL(file);
  }

  render() {
    const { classes } = this.props;
    const random = Math.random().toString(36).replace(/[^a-z]+/g, '').substr(0, 5);

    return (
      <Fragment>
        <input ref="in" accept="image/*" className={classes.input} id={`icon-button-file-${random}`} type="file" onChange={this._handleFile} />
        <label htmlFor={`icon-button-file-${random}`} onChange={this._handleFile}>
          <IconButton color="primary" className={classes.button} component="span">
            <PhotoCameraIcon />
          </IconButton>
        </label>
      </Fragment>
    );
  }
}

const styles = theme => ({
  button: {
    margin: theme.spacing.unit,
    position: 'absolute',
    bottom: '10px',
    right: '40px'
  },
  input: {
    display: 'none'
  },
});

SelectImageButton.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(SelectImageButton);