import React, { Component } from 'react';
import { compose } from 'redux';
import { connect } from 'react-redux';

import { withStyles } from '@material-ui/core/styles';
import { SnackbarProvider } from 'notistack';

import { App } from '../lib/api';
import { getPublicVapidKey, setPushSubscriptionId } from '../redux/actions';
import { sha256Hash } from '../lib/web-push/generate-hash';
import SnackbarList from './snackbar-list';

class Page extends Component {
  async componentDidMount() {
    // get VAPID public key
    await this.props.getPublicVapidKey();

    const subscription = await this._getPushSubscriptionObj();
    if (subscription) this.props.setPushSubscriptionId(await sha256Hash(subscription.endpoint));
  }

  _getPushSubscriptionObj = async () => {
    if (window && window.Notification && window.Notification.permission === "granted") { //check if notification permissions has already been granted
      const serviceWorkerRegistration = await navigator.serviceWorker.ready;
      const subscription = await serviceWorkerRegistration.pushManager.getSubscription();
      return subscription;
    }
  }

  render() {
    const { classes } = this.props;

    return (
      <div className={classes.fullScreen}>
        <SnackbarProvider maxSnack={3} anchorOrigin={{ vertical: 'top', horizontal: 'center' }} autoHideDuration={6000} preventDuplicate dense>
          <App />
          <title>{this.props.title}</title>
          {this.props.children}

          <SnackbarList />
        </SnackbarProvider>
      </div>
    );
  }
}

const styles = theme => ({
  fullScreen: {
    height: '100vh'
  }
});

const mapStateToProps = (state, props) => {
  return {};
};

const mapDispatchToProps = dispatch => {
  return {
    setPushSubscriptionId: subscriptionId => dispatch(setPushSubscriptionId(subscriptionId)),
    getPublicVapidKey: () => dispatch(getPublicVapidKey()),
  };
};

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  withStyles(styles, { withTheme: true })
)(Page);
