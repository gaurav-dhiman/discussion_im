import React, { Component } from 'react';

import { compose } from 'redux';
import { connect } from 'react-redux';
import memoize from 'memoize-one';

import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';

import TopicMessage from "../topic-message";
import ResponseMessages from './response-messages';
import SendMessageBox from '../send-message-box';
import OptionsBar from './options-bar';

class TopicThread extends Component {
  constructor(props) {
    super(props);
    this.state = {
      searchText: ''
    };
  }

  _handleSearchTextChange = searchText => this.setState({ searchText });

  // Re-run the filter whenever the list array or filter text changes:
  _filter = memoize((list, filterText = '') => {
    if (!filterText) return list;
    return (list || []).filter(item => {
      const regex = new RegExp(filterText, 'i');
      if ((item.text || '').match(regex))
        return item;
    })
  });

  render() {
    const { classes, message } = this.props;
    let { searchText } = this.props;
    searchText = searchText || this.state.searchText;
    let { replies } = this.props;
    replies = this._filter(replies, searchText);

    return (
      <div className={classes.root}>
        <OptionsBar message={message} handleSearchTextChange={this._handleSearchTextChange} />

        {!message && (
          <Paper className={classes.noMessage}>
            <Typography
              className={classes.blankPagePadding}
              variant={'headline'}
            >
              {'Oops, this thread does not exist anymore.'}
            </Typography>
          </Paper>
        )}

        {!!message && (
          <React.Fragment>
            <div className={classes.scroll}>
              <TopicMessage message={message} showRepliesCount={false} />
              <ResponseMessages messages={replies} searchText={searchText} />
            </div>
            <SendMessageBox
              sendToUUID={message.uuid}
              sendToEntity={'topicMessage'}
            />
          </React.Fragment>
        )}
      </div>
    );
  }
}

TopicThread.propTypes = {
  classes: PropTypes.object.isRequired,
  message: PropTypes.object.isRequired,
  replies: PropTypes.array.isRequired,
  invalidTopicMessage: PropTypes.string
};

const styles = theme => ({
  root: {
    flex: '1 1 auto',
    display: 'flex',
    height: '100%',
    flexDirection: 'column',
    backgroundColor: theme.palette.background.paper
  },
  noMessage: {
    flex: '1 1 auto',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    boxShadow: 'unset'
  },
  blankPagePadding: {
    padding: '10%'
  },
  scroll: {
    flex: '1 1 auto',
    padding: '0px',
    overflowY: 'auto',
    overflowX: 'hidden'
  }
});

const mapStateToProps = state => {
  return {};
};

const mapDispatchToProps = dispatch => {
  return {};
};

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  withStyles(styles, { withTheme: true })
)(TopicThread);
