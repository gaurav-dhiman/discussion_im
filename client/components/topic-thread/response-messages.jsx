import React, { Component } from 'react';
import { compose } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import * as moment from 'moment';
import { withStyles } from '@material-ui/core/styles';
import { Typography } from '@material-ui/core';
import Tooltip from '@material-ui/core/Tooltip';
import IconButton from '@material-ui/core/IconButton';
import Stepper from './stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import StepContent from '@material-ui/core/StepContent';
import Avatar from 'react-avatar';
import red from '@material-ui/core/colors/red';
import DeepLink from '../deep-link';
import ActionFooter from '../topic-message/action-footer';

import { userSelector } from '../../redux/selectors';

class TopicMessageResponses extends Component {

  _getAvatar(message) {
    return (
      <DeepLink type='user' val={message.sender.uuid}>
        <IconButton aria-label="Reply">
          <Avatar name={message.sender.displayName} src={message.sender.profileImageURL} size="40" round={true} color='#f44336' />
        </IconButton>
      </DeepLink>
    )
  }

  _getFormattedDate(date) {
    const formatedDate = moment(date).format('MMM D, YYYY | hh:mm A');
    const dateToShow = moment(date).fromNow();
    return (
      <Tooltip title={formatedDate} placement='bottom-start'>
        <Typography>
          {dateToShow}
        </Typography>
      </Tooltip>
    )
  }

  render() {
    const { classes, messages, searchText } = this.props;
    let headerMessage;

    if (messages.length) {
      headerMessage = ((messages.length > 1) ? `${messages.length} Replies` : `${messages.length} Reply`);
    } else if (searchText) {
      headerMessage = `No messages to show. Try with other search text.`;
    } else {
      headerMessage = `Be the first one to reply.`;
    }

    return (
      <div>
        <div className={classes.repliesHeader}>
          <Typography>
            {headerMessage}
          </Typography>
        </div>
        <Stepper orientation="vertical">
          {

            messages.map((message, i) => {
              let textLines = (message.text || '').split('\n');
              textLines = textLines.map((line, i) => {
                return <span key={i}>
                  {line}
                  <br />
                </span>;
              });

              return <Step key={i}>
                <StepLabel icon={this._getAvatar(message)}>{message.sender.displayName}<br />{this._getFormattedDate(message.createdAt)}</StepLabel>
                <StepContent className={classes.stepContent}>
                  <Typography>{message.text}</Typography>
                  <ActionFooter message={message} showRepliesCount={true} />
                </StepContent>
              </Step>
            })}
        </Stepper>
      </div>

    );
  }
}

TopicMessageResponses.propTypes = {
  classes: PropTypes.object.isRequired,
  messages: PropTypes.array.isRequired
};

const mapStateToProps = (state, props) => {
  const { messages } = props;
  messages.forEach(msg => msg.sender = userSelector(state, { uuid: msg.senderUUID }));
  return {
    messages
  };
};

const styles = theme => ({
  flexColumn: {
    flex: '1 1 auto',
    display: 'flex',
    flexDirection: 'column'
  },
  flex: {
    flex: '1 1 auto'
  },
  repliesHeader: {
    padding: '5px',
    backgroundColor: '#e6e6e6'
  },
  stepContent: {
    // marginLeft: '32px'
  },
  avatar: {
    backgroundColor: red[500],
  }
});

export default compose(
  connect(mapStateToProps),
  withStyles(styles, { withTheme: true })
)(TopicMessageResponses);
