import React, { Component } from 'react';
import { compose } from 'redux';
import { connect } from 'react-redux';

import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Hidden from '@material-ui/core/Hidden'
import Divider from '@material-ui/core/Divider';
import FormControl from '@material-ui/core/FormControl';
import InputBase from '@material-ui/core/InputBase';
import InputAdornment from '@material-ui/core/InputAdornment';
import CloseIcon from '@material-ui/icons/Close';
import IconButton from '@material-ui/core/IconButton';
import SearchIcon from '@material-ui/icons/Search';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import DeepLink from '../deep-link';

class OptionsBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showSearchField: false,
      searchText: ''
    };
  }

  _handleSearchTextChange = prop => event => {
    const { value } = event.target;
    this.setState({ ...this.state, [prop]: value });
    this.props.handleSearchTextChange(value);
  };

  _clearSearchText = () => {
    this.setState({
      searchText: ''
    });
    this.props.handleSearchTextChange('');
  };

  _removeSearchBar = async () => {
    await this._clearSearchText();
    this.setState({ ...this.state, showSearchField: false });
  }

  render() {
    const { classes, message } = this.props;
    const { showSearchField } = this.state;

    return (
      <React.Fragment>
        <Hidden smDown implementation="css">
          <div className={classes.headerIconDiv}>
            {!!message.parentTopicMessageUUID &&
              <DeepLink type='topic-message' val={message.parentTopicMessageUUID}>
                <IconButton
                  color="inherit"
                  aria-label="Collapse"
                >
                  <ArrowBackIcon fontSize='small' />
                </IconButton>
              </DeepLink>
            }
            {!showSearchField && <div className={classes.grow} />}
            {!!showSearchField &&
              <FormControl fullWidth className={classes.searchField}>
                <InputBase
                  autoFocus
                  className={classes.inputField}
                  value={this.state.searchText}
                  onChange={this._handleSearchTextChange('searchText')}
                  placeholder={this.props.searchPlaceholder || 'Search message replies'}
                  endAdornment={
                    <InputAdornment position="start">
                      <CloseIcon fontSize='small' color='primary' onClick={this._removeSearchBar} />
                    </InputAdornment>
                  }
                />
              </FormControl>
            }
            {!showSearchField &&
              <IconButton
                className={classes.rightMostAppBarButton}
                color="inherit"
                aria-label="Search"
                onClick={() => this.setState({ ...this.state, showSearchField: true })}
              >
                <SearchIcon fontSize='small' />
              </IconButton>
            }
            <DeepLink type='topic'>
              <IconButton
                color="inherit"
                aria-label="Collapse"
              >
                <ChevronRightIcon fontSize='small' />
              </IconButton>
            </DeepLink>
          </div>
          <Divider />
        </Hidden>
      </React.Fragment>
    );
  }
}

OptionsBar.propTypes = {
  classes: PropTypes.object.isRequired,
  message: PropTypes.object.isRequired,
  handleSearchTextChange: PropTypes.func.isRequired
};

const styles = theme => ({
  searchField: {
    margin: '6px',
    position: 'relative',
    top: '0',
    width: '95%',
    color: theme.palette.primary.main
  },
  inputField: {
    color: theme.palette.primary.main
  },
  headerIconDiv: {
    display: "flex",
    justifyContent: "space-between",
    backgroundColor: "#a4a4a4"
  },
  grow: {
    flexGrow: 1,
  }
});

const mapStateToProps = state => {
  return {};
};

const mapDispatchToProps = dispatch => {
  return {};
};

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  withStyles(styles, { withTheme: true })
)(OptionsBar);
