import React from 'react';
import { compose } from 'redux';
import { connect } from 'react-redux';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import * as moment from 'moment';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import Tooltip from '@material-ui/core/Tooltip';
import Avatar from 'react-avatar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import {
  Link as LinkIcon,
  Share as ShareIcon
} from '@material-ui/icons';
import { Pin as PinIcon } from 'mdi-material-ui'
import Chip from "@material-ui/core/Chip";

import DeepLink from '../deep-link';
import { CopyToClipboard } from 'react-copy-to-clipboard';
import { userSelector } from '../../redux/selectors';
import { pinTopic, unpinTopic, showSnackBarNotification, hightlightCurrentTopicMessage } from '../../redux/actions';
import { WEB_SERVER_PROTOCOL, WEB_SERVER_HOST } from '../../lib/api';
import ActionFooter from './action-footer';

class TopicMessage extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      cardHighlighted: false,
      shareWidgetAvailable: false
    };
  }

  componentDidMount() {
    if (window.navigator.share) this.setState({ shareWidgetAvailable: true });
  }

  _onCopy = () => {
    this.props.showSnackBarNotification('Link has been copied.', { type: 'info' });
  };

  _showShareoptions = async (message, messageLink) => {
    const senderDisplayName = message.sender.displayName || message.sender.handle;
    if (navigator.share) {
      try {
        await navigator.share({
          title: `Message from ${senderDisplayName}`,
          text: message.text,
          url: messageLink
        });
      } catch (error) {
        console.log('Failed to open share widget. Please report and issue. Error: ', error);
      }
    }
  };

  _pinTopicMessage = () => {
    this.props.message && this.props.pinTopic(this.props.message.uuid);
  }

  _unpinTopicMessage = () => {
    this.props.message && this.props.unpinTopic(this.props.message.uuid);
  }

  _getAvatar() {
    const { message } = this.props;

    return (
      <DeepLink type='user' val={message.sender.uuid}>
        <IconButton aria-label="Reply">
          <Avatar name={message.sender.displayName} src={message.sender.profileImageURL} size="40" round={true} color='#f44336' />
        </IconButton>
      </DeepLink >
    )
  }

  _getFormattedDate(date) {
    const formatedDate = moment(date).format('MMM D, YYYY | hh:mm A');
    const dateToShow = moment(date).fromNow();
    return (
      <Tooltip title={formatedDate} placement='bottom-start'>
        <Typography>
          {dateToShow}
        </Typography>
      </Tooltip>
    )
  }

  render() {
    const { classes, message, loggedInUserUUID, showRepliesCount, highlightCard } = this.props;
    const { shareWidgetAvailable } = this.state;
    message.sender = message.sender || {};

    let textLines = (message.text || '').split('\n');
    textLines = textLines.map((line, i) => {
      return <span key={i}>
        {line}
        <br />
      </span>;
    });
    const senderDisplayName = message.sender.displayName || message.sender.handle;
    message.pinnedByUsersUUID = message.pinnedByUsersUUID || {};
    const pinned = message.pinnedByUsersUUID[loggedInUserUUID];
    const messageLink = `${WEB_SERVER_PROTOCOL}${WEB_SERVER_HOST}/topics/${message.topicUUID}/messages/${message.uuid}`

    return (
      <div ref={(c) => { this.component = c; }}>
        <Card className={classNames(
          classes.card,
          highlightCard && classes.highlightCard
        )}
        >
          <CardHeader
            className={classes.cardHeader}
            avatar={this._getAvatar()}
            action={
              <div>

                {!!pinned &&
                  <Tooltip title='Unpin this message' placement='bottom-start'>
                    <IconButton
                      color="secondary"
                      aria-label="Bookmark"
                      onClick={this._unpinTopicMessage}
                    >
                      <PinIcon fontSize="small" color="secondary" />
                    </IconButton>
                  </Tooltip>
                }

                {!pinned &&
                  <Tooltip title='Pin it for quick access' placement='bottom-start'>
                    <IconButton
                      color="inherit"
                      aria-label="Bookmark"
                      onClick={this._pinTopicMessage}
                    >
                      <PinIcon fontSize="small" color="action" />
                    </IconButton>
                  </Tooltip>
                }

                {!shareWidgetAvailable &&
                  <Tooltip title='Copy Link' placement='bottom-start'>
                    <CopyToClipboard onCopy={this._onCopy} text={messageLink}>
                      <IconButton aria-label="Link">
                        <LinkIcon fontSize="small" />
                      </IconButton>
                    </CopyToClipboard>
                  </Tooltip>
                }

                {shareWidgetAvailable &&
                  <Tooltip title='Share' placement='bottom-start'>
                    {/* <CopyToClipboard onCopy={this._onCopy} text={messageLink}> */}
                    <IconButton aria-label="Link" onClick={() => this._showShareoptions(message, messageLink)}>
                      <ShareIcon fontSize="small" />
                    </IconButton>
                    {/* </CopyToClipboard> */}
                  </Tooltip>
                }
              </div>
            }
            title={senderDisplayName}
            subheader={this._getFormattedDate(message.createdAt)}
          />

          <CardContent className={classes.cardContent}>
            <Typography component="p">
              {textLines}
            </Typography>
          </CardContent>

          <ActionFooter message={message} showRepliesCount={showRepliesCount} />
        </Card>
      </div>

    );
  }
}

TopicMessage.propTypes = {
  classes: PropTypes.object.isRequired,
  message: PropTypes.object.isRequired,
  loggedInUserUUID: PropTypes.string.isRequired,
  showRepliesCount: PropTypes.bool
};

const styles = theme => ({
  card: {
    width: '100%',
    borderRadius: 0,
    boxShadow: 'none',
    paddingTop: '10px',
    paddingBottom: '10px',
    backgroundColor: '#f7f9fa',
    borderStyle: 'solid',
    borderColor: '#dddddd',
    borderWidth: '0px 0px 1px 0px',
    transition: theme.transitions.create(
      ['background-color'],
      { duration: theme.transitions.duration.complex }
    )
  },
  cardHeader: {
    padding: '5px 10px'
  },
  cardContent: {
    padding: '0px 20px 10px'
  },
  highlightCard: {
    backgroundColor: '#fbffe9',
  }
});

const mapStateToProps = (state, props) => {
  const { message } = props;
  message.sender = userSelector(state, { uuid: message.senderUUID });

  return {
    loggedInUserUUID: state.loggedInUserUUID,
    message
  };
};

const mapDispatchToProps = dispatch => {
  return {
    pinTopic: topicUUID => dispatch(pinTopic(topicUUID)),
    unpinTopic: topicUUID => dispatch(unpinTopic(topicUUID)),
    showSnackBarNotification: (displayMessage, options) => dispatch(showSnackBarNotification(displayMessage, options)),
    // unhighlightCurrentTopicMessage: () => dispatch(hightlightCurrentTopicMessage(false))
  };
};

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  withStyles(styles, { withTheme: true })
)(TopicMessage);
