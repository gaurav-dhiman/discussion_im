import React, { Component } from 'react';
import { compose } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import CardActions from '@material-ui/core/CardActions';
import Tooltip from '@material-ui/core/Tooltip';
import {
  Reply as ReplyIcon,
  Favorite as FavoriteIcon
} from '@material-ui/icons';
import Badge from '@material-ui/core/Badge';
// import scrollToComponent from 'react-scroll-to-component';
import { userSelector } from '../../redux/selectors';
import DeepLink from '../deep-link';
import { likeTopicMessage, unlikeTopicMessage } from '../../redux/actions';


class ActionFooter extends Component {
  _likeTopicMessage = () => {
    this.props.message && this.props.likeTopicMessage(this.props.message.uuid);
  }

  _unlikeTopicMessage = () => {
    this.props.message && this.props.unlikeTopicMessage(this.props.message.uuid);
  }

  render() {
    const { classes, message, loggedInUserUUID, showRepliesCount } = this.props;
    message.sender = message.sender || {};

    let textLines = (message.text || '').split('\n');
    textLines = textLines.map((line, i) => {
      return <span key={i}>
        {line}
        <br />
      </span>;
    });
    message.pinnedByUsersUUID = message.pinnedByUsersUUID || {};
    message.likedByUsersUUID = message.likedByUsersUUID || {};
    const liked = message.likedByUsersUUID[loggedInUserUUID];
    const likeCount = Object.keys(message.likedByUsersUUID || {}).length;
    const repliesCount = message.totalRepliesCount || 0;

    return (
      <CardActions className={classes.cardActions}>
        {!!liked &&
          <Tooltip title='Undo like on this message' placement='bottom-start'>
            <Badge className={classes.margin} badgeContent={likeCount} color="secondary" onClick={this._unlikeTopicMessage}>
              <FavoriteIcon color="error" />
            </Badge>
          </Tooltip>
        }

        {!liked &&
          <Tooltip title='Like this message' placement='bottom-start'>
            <Badge className={classes.margin} badgeContent={likeCount} color="secondary" onClick={this._likeTopicMessage}>
              <FavoriteIcon color="action" />
            </Badge>
          </Tooltip>
        }


        {!!showRepliesCount &&
          <Tooltip title='Reply to this message' placement='bottom-start'>
            <DeepLink type='topic-message' val={message.uuid}>
              <Badge className={classes.margin} badgeContent={repliesCount} color="secondary">
                <ReplyIcon color="action" />
              </Badge>
            </DeepLink>
          </Tooltip>
        }
      </CardActions>
    );
  }
}

ActionFooter.propTypes = {
  classes: PropTypes.object.isRequired,
  loggedInUserUUID: PropTypes.string.isRequired,
  message: PropTypes.object.isRequired,
  showRepliesCount: PropTypes.bool
};

const styles = theme => ({
  cardActions: {
    padding: '5px 10px'
  },
  margin: {
    margin: theme.spacing.unit * 2,
  },
  chipLeftMargin: {
    marginLeft: "5px"
  }
});

const mapStateToProps = (state, props) => {
  const { message } = props;
  message.sender = userSelector(state, { uuid: message.senderUUID });

  return {
    loggedInUserUUID: state.loggedInUserUUID,
    message
  };
};

const mapDispatchToProps = dispatch => {
  return {
    likeTopicMessage: topicMessageUUID => dispatch(likeTopicMessage(topicMessageUUID)),
    unlikeTopicMessage: topicMessageUUID => dispatch(unlikeTopicMessage(topicMessageUUID))
  };
};

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  withStyles(styles, { withTheme: true })
)(ActionFooter);
