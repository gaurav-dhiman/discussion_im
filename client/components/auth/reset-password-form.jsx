import React, { Component } from 'react';
import { compose } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Link from 'next/link';

import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

import { userSelector } from '../../redux/selectors';
import { resetPassword, showSnackBarNotification } from '../../redux/actions';
import emailTypes from '../../../server/services/auth-management/notification-email-types';

class ResetPasswordForm extends Component {
  state = {
    resetPassword: {}
  };

  _resetPassword = async () => {
    let { resetPassword } = this.state;
    const { user = {}, showSnackBarNotification } = this.props;
    const email = resetPassword && resetPassword.email;

    if (!user.email)
      return showSnackBarNotification('Password reset link is not valid. Try regenerating the link.', { type: 'info' });

    if (email !== user.email)
      return showSnackBarNotification('You entered an incorrect email id. Enter an email id that is associated with your account.', { type: 'info' });

    const password = resetPassword && resetPassword.newPassword;
    const { token } = this.props;
    resetPassword = {
      action: emailTypes.RESET_PASSWORD + 'Long',
      value: {
        token,
        password,
      },
    };
    this.props.resetPassword(resetPassword);
    this.setState({ resetPassword: {} });
  };

  _handleTextChange = (form, field) => event => {
    if (!form || !field) return;
    const { value } = event.target;
    this.setState({ [form]: { ...this.state[form], [field]: value } });
  };

  render() {
    const { classes, action } = this.props;
    const { resetPassword } = this.state;
    if (action !== 'reset') return <div />;

    const emailRegex = /^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/g;
    const validEmailFormat = resetPassword && resetPassword.email && resetPassword.email.match(emailRegex);

    const isPasswordMismatched = resetPassword
      && resetPassword.newPassword
      && resetPassword.reEnterNewPassword
      && (resetPassword.reEnterNewPassword.length >= resetPassword.newPassword.length)
      && (resetPassword.newPassword !== resetPassword.reEnterNewPassword);
    const canSubmit = resetPassword
      && resetPassword.newPassword
      && resetPassword.reEnterNewPassword
      && (resetPassword.newPassword === resetPassword.reEnterNewPassword)
      && validEmailFormat;
    return (
      <div className={classes.form}>
        <DialogTitle id="form-dialog-title">Set new password.</DialogTitle>
        <DialogContent>
          <TextField
            autoFocus
            margin="dense"
            required
            onChange={this._handleTextChange('resetPassword', 'email')}
            value={resetPassword.email || ''}
            id="emailId"
            label="Email ID"
            type="email"
            fullWidth
          />
          <TextField
            margin="dense"
            onChange={this._handleTextChange('resetPassword', 'newPassword')}
            id="newPassword"
            label="New password"
            type="password"
            value={resetPassword.newPassword || ''}
            required={true}
            fullWidth
          />
          <TextField
            margin="dense"
            onChange={this._handleTextChange('resetPassword', 'reEnterNewPassword')}
            error={isPasswordMismatched}
            helperText={(isPasswordMismatched) ? 'Password not matching. Ensure to have same password in both fields.' : ''}
            id="reEnterNewPassword"
            label="Re-enter new password"
            type="password"
            value={resetPassword.reEnterNewPassword || ''}
            required={true}
            fullWidth
          />
        </DialogContent>
        <DialogActions>
          <Button variant="raised" color="primary" onClick={this._resetPassword} disabled={!canSubmit}>
            Submit
          </Button>
        </DialogActions>
        <hr className={classes.lineLength} />
        <DialogContent>
          <DialogContentText>
            <Link as={'/'} href={'/'} prefetch passHref><span className={classes.link}>Home</span></Link>
            <span> | </span>
            <Link as={'topics'} href={'/topics'} prefetch passHref><span className={classes.link}>Topics</span></Link>
          </DialogContentText>
        </DialogContent>
      </div>
    );
  }
}

const styles = theme => ({
  form: {
    maxWidth: '496px',
    margin: 'auto',
    background: 'white',
    padding: '25px'
  },
  lineLength: {
    width: '100%'
  },
  link: {
    color: '#00bcd4',
    fontSize: '13px',
    cursor: 'pointer'
  }
});

const mapStateToProps = (state, props) => {
  const user = userSelector(state, { verifyToken: props.token });
  return { user };
};

const mapDispatchToProps = dispatch => {
  return {
    resetPassword: passwordObj => dispatch(resetPassword(passwordObj)),
    showSnackBarNotification: (displayMessage, options) => dispatch(showSnackBarNotification(displayMessage, options)),
  };
};

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  withStyles(styles, { withTheme: true })
)(ResetPasswordForm);
