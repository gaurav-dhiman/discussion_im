import React, { Component } from 'react';
import { compose } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Link from 'next/link';

import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

import { userSelector } from '../../redux/selectors';
import { verifyAccount, showSnackBarNotification } from '../../redux/actions';
import emailTypes from '../../../server/services/auth-management/notification-email-types';


class ResetPasswordForm extends Component {
  state = {
    verifyAccount: {}
  };

  _verifyAccount = async () => {
    let { verifyAccount } = this.state;
    const { user = {}, showSnackBarNotification } = this.props;
    const email = verifyAccount && verifyAccount.email;

    if (!user.email)
      return showSnackBarNotification('Either the account verification link is not valid or you account does not need verification.', { type: 'info' });

    if (email !== user.email)
      return showSnackBarNotification('You entered an incorrect email id. Enter an email id that is associated with your account.', { type: 'info' });

    const { token } = this.props;
    verifyAccount = {
      action: emailTypes.VERIFY_ACCOUNT + 'Long',
      value: token
    };
    this.props.verifyAccount(verifyAccount);
    this.setState({ verifyAccount: {} });
  };

  _handleTextChange = (form, field) => event => {
    if (!form || !field) return;
    const { value } = event.target;
    this.setState({ [form]: { ...this.state[form], [field]: value } });
  };

  render() {
    const { classes, action } = this.props;
    const { verifyAccount } = this.state;
    const emailRegex = /^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/g;
    const enableContinueButton = verifyAccount && verifyAccount.email && verifyAccount.email.match(emailRegex);

    if (action !== 'verify') return <div />;
    return (
      <div className={classes.form}>
        <DialogTitle id="form-dialog-title">Enter your email id to verify your account.</DialogTitle>
        <DialogContent>
          <TextField
            autoFocus
            margin="dense"
            required
            onChange={this._handleTextChange('verifyAccount', 'email')}
            value={verifyAccount.email || ''}
            id="emailId"
            label="Email ID"
            type="email"
            fullWidth
          />
        </DialogContent>
        <DialogActions>
          <Button variant="raised" color="primary" onClick={this._verifyAccount} disabled={!enableContinueButton} >
            Submit
          </Button>
        </DialogActions>
        <hr className={classes.lineLength} />
        <DialogContent>
          <DialogContentText>
            <Link as={'/'} href={'/'} prefetch passHref><span className={classes.link}>Home</span></Link>
            <span> | </span>
            <Link as={'topics'} href={'/topics'} prefetch passHref><span className={classes.link}>Topics</span></Link>
          </DialogContentText>
        </DialogContent>
      </div>
    );
  }
}

const styles = theme => ({
  form: {
    maxWidth: '496px',
    margin: 'auto',
    background: 'white',
    padding: '25px'
  },
  lineLength: {
    width: '100%'
  },
  link: {
    color: '#00bcd4',
    fontSize: '13px',
    cursor: 'pointer'
  }
});

const mapStateToProps = (state, props) => {
  const user = userSelector(state, { verifyToken: props.token });
  return { user };
};

const mapDispatchToProps = dispatch => {
  return {
    verifyAccount: payload => dispatch(verifyAccount(payload)),
    showSnackBarNotification: (displayMessage, options) => dispatch(showSnackBarNotification(displayMessage, options)),
  };
};

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  withStyles(styles, { withTheme: true })
)(ResetPasswordForm);
