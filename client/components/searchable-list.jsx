import React, { Component } from 'react';
import { compose } from 'redux';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

import Paper from '@material-ui/core/Paper';
import FormControl from '@material-ui/core/FormControl';
import Input from '@material-ui/core/Input';
import InputAdornment from '@material-ui/core/InputAdornment';
import List from '@material-ui/core/List';
import SearchIcon from '@material-ui/icons/Search';
import CloseIcon from '@material-ui/icons/Close';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import * as Infinite from 'react-infinite';

class SearchableList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      searchText: ''
    };
    this.noop = undefined; // no-operation dummy method to disable fetching older records temp while searching list
  }

  componentDidMount() {
    const listContainerHeight =
      this.listContainerDOM && this.listContainerDOM.clientHeight;
    const listItemHeight = this.listItemDOM && this.listItemDOM.clientHeight;
    this.setState({ listContainerHeight });
  }

  _handleSearchTextChange = prop => event => {
    const { value } = event.target;
    if (!value) this.noop = undefined;
    else this.noop = () => { };
    this.setState({ ...this.state, [prop]: event.target.value });
  };

  _clearSearchText = () => {
    this.setState({
      searchText: ''
    });
  };

  _closeModal = () => {
    this.props.onClose();
  };

  // Add search text to child component's props
  _getChildren = () =>
    React.Children.map(this.props.children, child =>
      React.cloneElement(child, { searchText: this.state.searchText })
    );

  render() {
    const { classes } = this.props;
    return (
      <div className={classes.root}>
        {/* Staic search box, over list*/}
        <Paper elevation={4} className={classes.searchBox}>
          {this.props.showSearchBox && (
            <FormControl fullWidth className={classes.searchField}>
              <Input
                id="adornment-amount"
                value={this.state.searchText}
                onChange={this._handleSearchTextChange('searchText')}
                placeholder={this.props.searchPlaceholder || 'Search'}
                endAdornment={
                  <InputAdornment position="start">
                    {this.state.searchText && (
                      <CloseIcon onClick={this._clearSearchText} />
                    )}
                    {!this.state.searchText && <SearchIcon />}
                  </InputAdornment>
                }
              />
            </FormControl>
          )}
        </Paper>

        {/* List of Items */}
        <List className={classes.list}>
          <div
            className={classes.listDiv}
            ref={r => (this.listContainerDOM = r)}
          >
            {this.props.scrollable && (
              <Infinite
                className={classes.autoScroll}
                containerHeight={this.state.listContainerHeight || 600}
                elementHeight={this.state.listItemHeight || 68}
                displayBottomUpwards={this.props.displayBottomUp || false}
                infiniteLoadBeginEdgeOffset={10}
                onInfiniteLoad={this.noop || this.props.onInfiniteLoad}
              >
                {this._getChildren()}
              </Infinite>
            )}
            {!this.props.scrollable && this._getChildren()}
          </div>
        </List>
      </div>
    );
  }
}

const styles = theme => ({
  searchBox: {
    borderRight: '1px solid rgba(0, 0, 0, 0.12)',
    borderRadius: 0
  },
  searchField: {
    margin: theme.spacing.unit,
    marginTop: theme.spacing.unit * 3,
    position: 'relative',
    top: '0',
    width: '95%'
  },
  list: {
    flex: '1 1 auto',
    display: 'flex',
    overflow: 'auto',
    padding: '0px',
    minWidth: '300px',
    [theme.breakpoints.up('md')]: {
      width: '385px'
    }
  },
  listDiv: {
    flex: '1 1 auto'
  },
  root: {
    display: 'flex',
    flexDirection: 'column',
  },
  autoScroll: {
    overflowY: 'auto !important'
  }
});

SearchableList.propTypes = {
  classes: PropTypes.object.isRequired,
  fetchOlderEntries: PropTypes.func
};

export default compose(
  withStyles(styles, { withTheme: true })
)(SearchableList);
