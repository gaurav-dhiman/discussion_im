import React, { Component } from 'react';
import { compose } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import memoize from 'memoize-one';

import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';

import { withStyles } from '@material-ui/core/styles';
import TopicMessage from '../topic-message';
import { getTopicMessages } from '../../redux/actions';

class TopicMessages extends Component {
  constructor(props) {
    super(props);
    this.prevSearchText = '';
  }

  componentDidUpdate() {
    const { searchText, getTopicMessages } = this.props;
    if (!searchText) return;
    if (searchText === this.prevSearchText) return;
    this.prevSearchText = searchText;
    getTopicMessages({
      query: {
        $text: { $search: searchText }
      }
    });
  }

  // Re-run the filter whenever the list array or filter text changes:
  _filter = memoize((list, filterText = '') => {
    if (!filterText) return list;
    return (list || []).filter(item => {
      const regex = new RegExp(filterText, 'i');
      if ((item.text || '').match(regex))
        return item;
    })
  });

  render() {
    const { classes, searchText, scrollToLastMessage, highlightCurrentTopicMessage } = this.props;
    let { messages } = this.props;
    messages = this._filter(messages, searchText);

    return (
      <React.Fragment>
        {!messages.length &&
          <Paper className={classes.noMessage}>
            <Typography
              className={classes.blankPagePadding}
              variant={'headline'}
            >
              {'Found no such topic messages.'}
            </Typography>
          </Paper>}

        {!!messages.length &&
          messages.map((message, i) => {
            const scrollToMessage = !highlightCurrentTopicMessage && scrollToLastMessage && (i == messages.length - 1);
            return <TopicMessage key={i} message={message} showRepliesCount={true} scrollToMessage={scrollToMessage} />
          })}
      </React.Fragment>
    );
  }
}

const styles = theme => ({
  flexColumn: {
    flex: '1 1 auto',
    display: 'flex',
    flexDirection: 'column'
  },
  flex: {
    flex: '1 1 auto'
  },
  noMessage: {
    flex: '1 1 auto',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    boxShadow: 'unset'
  },
  blankPagePadding: {
    padding: '10%'
  }
});

TopicMessages.propTypes = {
  classes: PropTypes.object.isRequired,
  messages: PropTypes.array.isRequired,
  searchText: PropTypes.string.isRequired,
  invalidTopicMessage: PropTypes.string,
  scrollToLastMessage: PropTypes.bool
};

const mapStateToProps = (state, props) => {
  const { highlightCurrentTopicMessage } = state;
  return {
    highlightCurrentTopicMessage
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getTopicMessages: (queryObj, options) => dispatch(getTopicMessages(queryObj, options))
  };
};

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  withStyles(styles, { withTheme: true })
)(TopicMessages);
