import React, { Component } from 'react';
import * as ReactDOM from 'react-dom';

import { compose } from 'redux';
import { connect } from 'react-redux';

import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import Divider from '@material-ui/core/Divider';
import LoadingSpinner from '@material-ui/core/CircularProgress';

import TopicMessages from './topic-messages';
import SendMessageBox from '../send-message-box';
// import TopicView from "../infinite-scrollable-list";
import TopicView from "react-chatview";
import api, { service } from '../../lib/api';
import { getTopicMessages, addTopicMessagesToReduxStore } from '../../redux/actions';
import OptionsBar from './options-bar';

class TopicRoom extends Component {
  constructor(props) {
    super(props);

    this.state = {
      searchText: '',
      applyPinFilter: false
    };

    // Subscribe to topic-message events from server
    api.service(service.topicMessages).on('created', topicMessage => {
      props.dispatch(getTopicMessages({ query: { uuid: topicMessage.uuid, fetchPlan: 'sender,topic' } }));
    });
    api.service(service.topicMessages).on('updated', topicMessage => {
      props.dispatch(addTopicMessagesToReduxStore(topicMessage));
    });
    api.service(service.topicMessages).on('patched', topicMessage => {
      props.dispatch(addTopicMessagesToReduxStore(topicMessage));
    });
    api.service(service.topicMessages).on('removed', topicMessage => {
    });
  }

  componentDidMount() {
    const { highlightCurrentTopicMessage } = this.props;
    if (!highlightCurrentTopicMessage) this._scrollToBottom();
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    const { topicViewDOM } = this;
    if (!topicViewDOM) return;
    if (!snapshot) return;
    const domNode = ReactDOM.findDOMNode(topicViewDOM);
    // If we are not at bottom of topic room, then keep the scroll bar at current position to maintain user experience.
    if ((!snapshot.isScrollAtBottom) && snapshot.scrollTop)
      domNode.scrollTop = snapshot.scrollTop;
    else
      this._scrollToBottom();
  }

  getSnapshotBeforeUpdate(nextProps, nextState) {
    const { topicViewDOM } = this;
    if (!topicViewDOM) return null;
    return {
      scrollTop: topicViewDOM.scrollTop,
      isScrollAtBottom: topicViewDOM.scrollHeight - (topicViewDOM.scrollTop + topicViewDOM.clientHeight) < 10
    };
  }

  _scrollToBottom = () => {
    const { topicViewDOM } = this;
    if (!topicViewDOM) return;
    const domNode = ReactDOM.findDOMNode(topicViewDOM);
    domNode.scrollTop = domNode.scrollHeight - domNode.clientHeight;
  }

  _handlePinFilter = (applyPinFilter) => {
    this.setState({ applyPinFilter: applyPinFilter });
  }

  _handleSearchTextChange = searchText => this.setState({ searchText });

  _fetchOlderTopicMessages = async () => {
    const { topic, topicMessages } = this.props;
    let { skip, limit } = topicMessages;
    if (skip <= 0) return;
    skip = skip - limit;
    if (skip < 0) skip = 0;
    limit = topicMessages.skip - skip;
    this.props.getTopicMessages(
      { // query obj
        query: {
          topicUUID: topic.uuid,
          $skip: skip,
          $limit: limit
        }
      },
      { // options obj
        metaContext: {
          modelUUID: topic.uuid,
          modelType: 'topic'
        }
      }
    );
  };

  render() {
    const { classes, topic, topicMessages, invalidTopicMessage, loggedInUserUUID } = this.props;
    let { applyPinFilter } = this.state;
    applyPinFilter = applyPinFilter || this.props.applyPinFilter;
    let { searchText } = this.props;
    searchText = searchText || this.state.searchText;
    let { data } = topicMessages;
    const showLoadingSpinner = (topicMessages.skip > 0);
    let emptyListMessage = 'No messages posted in this topic.'
    if (applyPinFilter) {
      emptyListMessage = 'No messages are pinned in this topic.'
      data = data.filter((message) => message.pinnedByUsersUUID[loggedInUserUUID])
    }

    return (
      <div className={classes.root}>
        {!data && (
          <Paper className={classes.noMessage}>
            <Typography
              className={classes.blankPagePadding}
              variant={'subtitle1'}
            >
              {invalidTopicMessage ||
                'Select the topic to see the related messages.'}
            </Typography>
          </Paper>
        )}

        {data &&
          !data.length && (
            <React.Fragment>
              <OptionsBar topic={topic} messages={data} pinFilter={applyPinFilter} onPinFilterClick={this._handlePinFilter} />
              <Paper className={classes.noMessage}>
                <Typography
                  className={classes.blankPagePadding}
                  variant={'subtitle1'}
                >
                  {emptyListMessage}
                </Typography>
              </Paper>
              <SendMessageBox
                sendToUUID={topic.uuid}
                sendToEntity={'topic'}
              />
            </React.Fragment>
          )}

        {data &&
          !!data.length && (
            <React.Fragment>
              <OptionsBar topic={topic} pinFilter={applyPinFilter} handleSearchTextChange={this._handleSearchTextChange} onPinFilterClick={this._handlePinFilter} />
              <TopicView
                className={classes.topicView}
                ref={r => (this.topicViewDOM = r && r.scrollable)}
                flipped={true}
                reversed={true}
                scrollLoadThreshold={0}
                // shouldTriggerLoad={() => false}
                // listLength={data.length}
                onInfiniteLoad={this._fetchOlderTopicMessages}
              // onScroll={this._fetchOlderTopicMessages}
              >
                {showLoadingSpinner &&
                  <React.Fragment>
                    <center className={classes.loaderContainer}>
                      <LoadingSpinner size={20} className={classes.loadingSpinner} />
                    </center>
                    <Divider />
                  </React.Fragment>
                }

                <TopicMessages messages={data} searchText={searchText} />
              </TopicView>
              <SendMessageBox
                sendToUUID={topic.uuid}
                sendToEntity={'topic'}
              />
            </React.Fragment>
          )}
      </div>
    );
  }
}

const styles = theme => ({
  root: {
    flex: '1 1 auto',
    height: `calc(100vh - ${theme.mixins.toolbar.minHeight}px)`,
    display: 'flex',
    flexDirection: 'column',
    backgroundColor: theme.palette.background.paper
  },
  noMessage: {
    flex: '1 1 auto',
    display: 'flex',
    alignItems: 'center',
    alignItems: 'stretch',
    justifyContent: 'center',
    boxShadow: 'unset'
  },
  topicView: {
    flex: '1 1 auto'
  },
  blankPagePadding: {
    textAlign: '-webkit-center',
    alignSelf: 'center'
  },
  loaderContainer: {
    backgroundColor: '#f7f9fa',
    padding: '15px'
  },
  loadingSpinner: {
    margin: theme.spacing.unit,
  }
});

const mapStateToProps = (state, props) => {
  const { highlightCurrentTopicMessage, loggedInUserUUID } = state;
  return {
    topic: props.topic || {},
    topicMessages: props.topicMessages || {},
    highlightCurrentTopicMessage,
    loggedInUserUUID
  };
};

const mapDispatchToProps = dispatch => {
  return {
    dispatch,
    getTopicMessages: (query, options) => dispatch(getTopicMessages(query, options)),
    addTopicMessagesToReduxStore: topicMessages => dispatch(addTopicMessagesToReduxStore(topicMessages))
  };
};

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  withStyles(styles, { withTheme: true })
)(TopicRoom);
