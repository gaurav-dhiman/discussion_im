import React from 'react';
import { compose } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import FormControl from '@material-ui/core/FormControl';
import InputBase from '@material-ui/core/InputBase';
import InputAdornment from '@material-ui/core/InputAdornment';
import CloseIcon from '@material-ui/icons/Close';
import Hidden from '@material-ui/core/Hidden';
import Divider from '@material-ui/core/Divider';
import Tooltip from '@material-ui/core/Tooltip';
import IconButton from '@material-ui/core/IconButton';
import SearchIcon from '@material-ui/icons/Search';
import StarBorderIcon from '@material-ui/icons/StarBorder';
import StarFilledIcon from '@material-ui/icons/StarOutlined';
import { BellRing as NotificationIcon, BellOutline as NotificationOffIcon, Pin as PinIcon, PinOutline as PinOffIcon } from 'mdi-material-ui'

import { Typography } from '@material-ui/core';
import { starTopic, unstarTopic, subscribeToTopic, unsubscribeFromTopic } from '../../redux/actions';

class OptionsBar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showSearchField: false,
      searchText: ''
    };
  }

  _handleSearchTextChange = prop => event => {
    const { value } = event.target;
    this.setState({ ...this.state, [prop]: value });
    this.props.handleSearchTextChange(value);
  };

  _clearSearchText = () => {
    this.setState({
      searchText: ''
    });
    this.props.handleSearchTextChange('');
  };

  _removeSearchBar = async () => {
    await this._clearSearchText();
    this.setState({ ...this.state, showSearchField: false });
  }

  _starTopic = () => {
    this.props.topic && this.props.starTopic(this.props.topic.uuid);
  }

  _unstarTopic = () => {
    this.props.topic && this.props.unstarTopic(this.props.topic.uuid);
  }

  _subscribeToTopic = () => {
    this.props.topic && this.props.subscribe(this.props.topic.uuid);
  }

  _unsubscribeFromTopic = () => {
    this.props.topic && this.props.unsubscribe(this.props.topic.uuid);
  }

  render() {
    const { classes, topic, loggedInUserUUID, pinFilter, onPinFilterClick } = this.props;
    const { showSearchField } = this.state;
    let { totalMessageCount = 0 } = topic;
    topic.staredByUsersUUIDs = topic.staredByUsersUUIDs || {};
    topic.webPushSubscriptionIds = topic.webPushSubscriptionIds || {};
    const stared = topic.staredByUsersUUIDs[loggedInUserUUID];
    const subscribed = topic.webPushSubscriptionIds[this.props.pushSubscriptionId] || false;
    let numOfParticipants = Object.keys(topic.postCountByUsersUUIDs || {}).length;
    numOfParticipants = (numOfParticipants > 1) ? `${numOfParticipants} Participants` : `${numOfParticipants} Participant`;
    totalMessageCount = (totalMessageCount > 1) ? `${totalMessageCount} Messages` : `${totalMessageCount} Message`;

    return (
      <Hidden smDown implementation="css">
        {!showSearchField &&
          <div className={classes.headerIconDiv}>
            {!stared &&
              <Tooltip title="Star to add to your list of topics">
                <IconButton
                  color="inherit"
                  size="small"
                  aria-label="Star Topic"
                  onClick={this._starTopic}
                  style={{ backgroundColor: 'transparent' }}
                >
                  <StarBorderIcon fontSize="small" />
                </IconButton>
              </Tooltip>
            }

            {!!stared &&
              <Tooltip title="Unstar to remove from your list of topics">
                <IconButton
                  color="secondary"
                  size="small"
                  aria-label="Unstar Topic"
                  onClick={this._unstarTopic}
                  style={{ backgroundColor: 'transparent' }}
                >
                  <StarFilledIcon fontSize="small" />
                </IconButton>
              </Tooltip>
            }

            {!subscribed &&
              <Tooltip title="Subscribe to get notifications about this topic">
                <IconButton
                  color="inherit"
                  size="small"
                  aria-label="Subscribe"
                  onClick={this._subscribeToTopic}
                  style={{ backgroundColor: 'transparent' }}
                >
                  <NotificationOffIcon fontSize="small" />
                </IconButton>
              </Tooltip>
            }

            {!!subscribed &&
              <Tooltip title="Unsubscribe to stop notifications about this topic">
                <IconButton
                  color="secondary"
                  size="small"
                  aria-label="Unsubscribe"
                  onClick={this._unsubscribeFromTopic}
                  style={{ backgroundColor: 'transparent' }}
                >
                  <NotificationIcon fontSize="small" />
                </IconButton>
              </Tooltip>
            }

            {!pinFilter &&
              <Tooltip title="Show only pinned messages">
                <IconButton
                  color="inherit"
                  size="small"
                  aria-label="show pinned messages"
                  onClick={() => onPinFilterClick(true)}
                  style={{ backgroundColor: 'transparent' }}
                >
                  <PinOffIcon fontSize="small" />
                </IconButton>
              </Tooltip>
            }

            {pinFilter &&
              <Tooltip title="Remove pin filter to show all messages">
                <IconButton
                  color="secondary"
                  size="small"
                  aria-label="show all messages"
                  onClick={() => onPinFilterClick(false)}
                  style={{ backgroundColor: 'transparent' }}
                >
                  <PinIcon fontSize="small" />
                </IconButton>
              </Tooltip>
            }

            <Tooltip title="Number of participants in this topic">
              <IconButton
                color="inherit"
                aria-label="Number of participants"
                style={{ backgroundColor: 'transparent' }}
              >
                <Typography component='p'>{numOfParticipants}</Typography>
              </IconButton>
            </Tooltip>

            <Tooltip title="Total number of messages in this topic">
              <IconButton
                color="inherit"
                aria-label="Total messages"
                style={{ backgroundColor: 'transparent' }}
              >
                <Typography component='p'>{totalMessageCount}</Typography>
              </IconButton>
            </Tooltip>

            <div className={classes.grow} />

            <IconButton
              className={classes.rightMostAppBarButton}
              color="inherit"
              aria-label="Search"
              onClick={() => this.setState({ ...this.state, showSearchField: true })}
            >
              <SearchIcon fontSize='small' />
            </IconButton>
          </div>
        }

        {!!showSearchField &&
          <div className={classes.headerIconDiv}>
            <FormControl fullWidth className={classes.searchField}>
              <InputBase
                autoFocus
                className={classes.inputField}
                value={this.state.searchText}
                onChange={this._handleSearchTextChange('searchText')}
                placeholder={this.props.searchPlaceholder || 'Search topic messages'}
                endAdornment={
                  <InputAdornment position="start">
                    <CloseIcon fontSize='small' color='primary' onClick={this._removeSearchBar} />
                  </InputAdornment>
                }
              />
            </FormControl>
          </div>
        }
        <Divider />
      </Hidden>
    );
  }
}

OptionsBar.propTypes = {
  classes: PropTypes.object.isRequired,
  searchPlaceholder: PropTypes.string,
  pinFilter: PropTypes.bool,
  onPinFilterClick: PropTypes.func
};

const styles = theme => ({
  searchField: {
    margin: '6px',
    position: 'relative',
    top: '0',
    width: '100%',
    color: theme.palette.primary.main
  },
  inputField: {
    color: theme.palette.primary.main
  },
  headerIconDiv: {
    display: "flex",
    justifyContent: "space-between",
    backgroundColor: "#a4a4a4",
    borderRight: "1px solid #dddddd"
  },
  grow: {
    flexGrow: 1,
  }
});

const mapStateToProps = (state, props) => {
  return {
    loggedInUserUUID: state.loggedInUserUUID,
    pushSubscriptionId: state.pushSubscriptionId
  };
};

const mapDispatchToProps = dispatch => {
  return {
    starTopic: topicUUID => dispatch(starTopic(topicUUID)),
    unstarTopic: topicUUID => dispatch(unstarTopic(topicUUID)),
    subscribe: (topicUUID) => dispatch(subscribeToTopic(dispatch, topicUUID)),
    unsubscribe: (topicUUID) => dispatch(unsubscribeFromTopic(dispatch, topicUUID))
  };
};

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  withStyles(styles, { withTheme: true })
)(OptionsBar);
