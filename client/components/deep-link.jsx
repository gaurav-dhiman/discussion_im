import React, { Component } from 'react';
import { compose } from 'redux';
import { connect } from 'react-redux';

import Link from 'next/link';

class DeepLink extends Component {

  render() {
    let asPath = '/topics'
    let href = '/topics?';
    let { val, topicUUID } = this.props;

    if (!this.props.type) return <div>{this.props.children}</div>

    if (this.props.type === 'topic') {
      val = val || topicUUID;
      href += `topicUUID=${val}`;
      asPath += `/${val}`
    } else if (topicUUID) {
      href += `topicUUID=${topicUUID}`;
      asPath += `/${topicUUID}`
    }

    switch (this.props.type) {
      case 'user':
        href += `&userUUID=${val}`;
        asPath += `/users/${val}`
        break;
      case 'topic-message':
        href += `&topicMessageUUID=${val}`;
        asPath += `/messages/${val}`
        break;
      default:
        break;
    }

    return (
      <Link as={asPath} href={href} prefetch passHref>
        {this.props.children}
      </Link>
    );
  }
}

const mapStateToProps = state => {
  return {
    topicUUID: state.currentTopicUUID
  };
};

const mapDispatchToProps = dispatch => {
  return {};
};

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
)(DeepLink);