import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';

class Notification extends Component {
  render() {
    const { classes } = this.props;
    let notificationClass;
    switch (this.props.type) {
      case 'error':
        notificationClass = classes.error;
        break;
      case 'success':
        notificationClass = classes.success;
        break;
      default:
        notificationClass = classes.notify;
        break;
    }
    return (
      <Typography component="p" className={notificationClass}>
        {this.props.message}
      </Typography>
    );
  }
}

const styles = theme => ({
  error: {
    backgroundColor: '#f5c8c8',
    padding: '10px',
    borderRadius: '2px'
  },
  success: {
    backgroundColor: '#d5f5d5',
    padding: '10px',
    borderRadius: '2px'
  },
  notify: {
    backgroundColor: '#e4e4e4',
    padding: '10px',
    borderRadius: '2px'
  }
});

export default withStyles(styles)(Notification);
