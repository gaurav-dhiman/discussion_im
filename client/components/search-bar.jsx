import React, { Component } from 'react';
import { compose } from 'redux';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

import AppBar from '@material-ui/core/AppBar';
import FormControl from '@material-ui/core/FormControl';
import InputBase from '@material-ui/core/InputBase';
import InputAdornment from '@material-ui/core/InputAdornment';
import SearchIcon from '@material-ui/icons/Search';
import CloseIcon from '@material-ui/icons/Close';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';

class SearchBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      searchText: ''
    };
  }

  _handleSearchTextChange = prop => event => {
    const { value } = event.target;
    this.setState({ ...this.state, [prop]: value });
    this.props.handleSearchTextChange(value);
  };

  _clearSearchText = () => {
    this.setState({
      searchText: ''
    });
    this.props.handleSearchTextChange('');
  };

  _removeSearchBar = async () => {
    await this._clearSearchText();
    await this.props.onBackButtonClick();
  }

  render() {
    const { classes } = this.props;
    return (
      <AppBar>
        <FormControl fullWidth className={classes.searchField}>
          <InputBase
            autoFocus
            className={classes.inputField}
            value={this.state.searchText}
            onChange={this._handleSearchTextChange('searchText')}
            placeholder={this.props.searchPlaceholder || 'Search'}
            startAdornment={
              <InputAdornment position="start">
                <ArrowBackIcon color='secondary' onClick={this._removeSearchBar} />
              </InputAdornment>
            } 
            endAdornment={
              <InputAdornment position="start">
                {this.state.searchText && (
                  <CloseIcon color='secondary' onClick={this._clearSearchText} />
                )}
                {!this.state.searchText && <SearchIcon />}
              </InputAdornment>
            }
          />
        </FormControl>
      </AppBar>
    );
  }
}

const styles = theme => ({
  searchField: {
    margin: theme.spacing.unit,
    marginTop: theme.spacing.unit * 2,
    position: 'relative',
    top: '0',
    width: '95%',
    color: theme.palette.secondary.main
  },
  inputField: {
    color: theme.palette.secondary.main
  }
});

SearchBar.propTypes = {
  classes: PropTypes.object.isRequired,
  handleSearchTextChange: PropTypes.func.isRequired,
  onBackButtonClick: PropTypes.func.isRequired,
  searchPlaceholder: PropTypes.string
};

export default compose(
  withStyles(styles, { withTheme: true })
)(SearchBar);
