import React, { Component } from 'react';
import { compose } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Link from 'next/link';

import Button from '@material-ui/core/Button';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Page from './page';
import { Typography } from '@material-ui/core';

// TODO: Make 404 page more interesting.
class PageNotFound404 extends Component {

  render() {
    const { classes } = this.props;
    return (
      <Page title={`Discussion.im - Resource not available (404)`}>
        <div className={classes.container}>
          <div className={classes.infoBox}>
            <DialogTitle>404 Error - Page Not Found</DialogTitle>
            <DialogContent>
              <Typography>
                May be the link you clicked is not valid.
          </Typography>
              <Typography>
                Click below button to see the list of your discussion topics.
          </Typography>
            </DialogContent>

            <DialogActions>
              <Link as={`/topics`} href={`/topics`} prefetch passHref>
                <Button variant="raised" style={{ margin: 'auto' }} color="primary">
                  Show My Topics
          </Button>
              </Link>
            </DialogActions>
          </div>
        </div>
      </Page>
    );
  }
}

const styles = theme => ({
  container: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    height: '100vh',
    backgroundColor: theme.palette.primary.main
  },
  infoBox: {
    maxWidth: '496px',
    borderRadius: '15px',
    background: 'white',
    padding: '25px'
  }
});

const mapStateToProps = state => {
  return {};
};

const mapDispatchToProps = dispatch => {
  return {};
};

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  withStyles(styles, { withTheme: true })
)(PageNotFound404);
