import React, { Component } from 'react';
import * as ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

class InfiniteScrollableList extends Component {
  constructor(props) {
    super(props);
    this.childrenItemPropName = this.props.childrenItemPropName || 'items';
    this.scrollAtBottom = true;
  }

  componentDidMount() {
    this._scrollToBottom();
  }

  getSnapshotBeforeUpdate(nextProps) {
    // const newHOC = React.Children.only(nextProps.children);
    // const oldHOC = React.Children.only(this.props.children);
    // // nextProps.children and this.props.children will have only one child that is a
    // // HOC (wrapper components) created with 'withStyles()'
    // const newChildren = newHOC.props[this.childrenItemPropName];
    // const oldChildren = oldHOC.props[this.childrenItemPropName];
    // this.historyChanged = newChildren.length != oldChildren.length;
    this.historyChanged = nextProps.listLength != this.props.listLength;
    const { listContainerDOM } = this;
    if (this.historyChanged) {
      const scrollPos = listContainerDOM.scrollTop;
      const scrollBottom =
        listContainerDOM.scrollHeight - listContainerDOM.clientHeight;
      this.scrollAtBottom = scrollBottom <= 0 || scrollPos === scrollBottom;
    }
    /* We should keep the focus to currently visible message if we are not at botton of list + the first message
    is not same. If first message is same, that means someone has posted a new topic message. */
    if (!this.scrollAtBottom && newChildren[0] != oldChildren[0]) {
      this.indexOfMessageToFocus = newChildren.length - oldChildren.length;
    }
  }

  componentDidUpdate() {
    const { listContainerDOM } = this;
    if (this.historyChanged) {
      if (this.scrollAtBottom) {
        this._scrollToBottom();
      }
      const messageToFocus =
        this.indexOfMessageToFocus >= 0
          ? listContainerDOM.childNodes[this.indexOfMessageToFocus]
          : null;
      if (messageToFocus) {
        ReactDOM.findDOMNode(messageToFocus).scrollIntoView();
      }
    }
  }

  _onScroll = async () => {
    const scrollTop = this.listContainerDOM.scrollTop;
    if (scrollTop === 0) {
      this.props.onScroll && (await this.props.onScroll());
    }
  };

  _scrollToBottom = () => {
    const { listContainerDOM } = this;
    const scrollHeight = listContainerDOM.scrollHeight;
    const height = listContainerDOM.clientHeight;
    const maxScrollTop = scrollHeight - height;
    ReactDOM.findDOMNode(listContainerDOM).scrollTop =
      maxScrollTop > 0 ? maxScrollTop : 0;
  };

  render() {
    const { classes } = this.props;
    return (
      <div
        className={classes.messageList}
        ref={r => (this.listContainerDOM = r)}
        onTouchMove={this._onScroll}
        onScroll={this._onScroll}
      >
        {this.props.children}
      </div>
    );
  }
}

const styles = theme => ({
  messageList: {
    flex: '1 1 auto',
    padding: '0px',
    overflowY: 'auto',
    overflowX: 'hidden'
  }
});

export default withStyles(styles)(InfiniteScrollableList);
