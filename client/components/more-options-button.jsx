import React from 'react';
import PropTypes from 'prop-types';
import IconButton from '@material-ui/core/IconButton';
import ClickAwayListener from '@material-ui/core/ClickAwayListener';
import Grow from '@material-ui/core/Grow';
import Paper from '@material-ui/core/Paper';
import Popper from '@material-ui/core/Popper';
import MenuList from '@material-ui/core/MenuList';
import MoreIcon from '@material-ui/icons/MoreVert';
import { withStyles } from '@material-ui/core/styles';

class MoreOptionsButton extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
    };
  }

  _handleToggle = () => {
    this.setState(state => ({ open: !state.open }));
  };

  _handleClose = event => {
    if (this.anchorEl.contains(event.target)) {
      return;
    }

    this.setState({ open: false });
  };

  render() {
    const { open } = this.state;
    let { children } = this.props;
    children = children.filter(child => child);

    const childrenWithOnClick = React.Children.map(children,
      child => React.cloneElement(child, {
        onClick: (event) => {
          child.props.onClick();
          this._handleClose(event);
        }
      })
    );

    return (
      <div>
        <IconButton
          buttonRef={node => {
            this.anchorEl = node;
          }}
          color="inherit"
          onClick={this._handleToggle}
        >
          <MoreIcon />
        </IconButton>
        <Popper open={open} anchorEl={this.anchorEl} transition disablePortal>
          {({ TransitionProps, placement }) => (
            <Grow
              {...TransitionProps}
              id="menu-list-grow"
              style={{ transformOrigin: placement === 'bottom' ? 'center top' : 'center bottom' }}
            >
              <Paper>
                <ClickAwayListener onClickAway={this._handleClose}>
                  <MenuList>
                    {childrenWithOnClick}
                  </MenuList>
                </ClickAwayListener>
              </Paper>
            </Grow>
          )}
        </Popper>
      </div>
    );
  }
}

const styles = theme => ({});

MoreOptionsButton.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(MoreOptionsButton);