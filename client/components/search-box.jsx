import React, { Component } from 'react';
import { compose } from 'redux';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

import FormControl from '@material-ui/core/FormControl';
import InputBase from '@material-ui/core/InputBase';
import InputAdornment from '@material-ui/core/InputAdornment';
import SearchIcon from '@material-ui/icons/Search';
import CloseIcon from '@material-ui/icons/Close';

class SearchBox extends Component {
  constructor(props) {
    super(props);
    this.state = {
      searchText: ''
    };
  }

  _timer = '';

  _handleSearchTextChange = prop => event => {
    if (this._timer) clearTimeout(this._timer);
    const { value } = event.target;
    this.setState({ ...this.state, [prop]: value });
    this._timer = setTimeout(() => {
      this.props.handleSearchTextChange(value);
    }, 1000);
  };

  _clearSearchText = () => {
    this.setState({
      searchText: ''
    });
    this.props.handleSearchTextChange('');
  };

  _removeSearchBox = async () => {
    await this._clearSearchText();
    await this.props.onBackButtonClick();
  }

  render() {
    const { classes } = this.props;
    return (
      <FormControl fullWidth className={classes.searchField}>
        <InputBase
          autoFocus
          placeholder="Search Google Maps"
          className={classes.inputField}
          value={this.state.searchText}
          onChange={this._handleSearchTextChange('searchText')}
          placeholder={this.props.searchPlaceholder || 'Search'}
          endAdornment={
            <InputAdornment position="start">
              {this.state.searchText && (
                <CloseIcon color='secondary' onClick={this._clearSearchText} />
              )}
              {!this.state.searchText && <SearchIcon />}
            </InputAdornment>
          }
        />
      </FormControl>
    );
  }
}

const styles = theme => ({
  searchBox: {
    borderRight: '1px solid rgba(0, 0, 0, 0.12)'
  },
  searchField: {
    margin: theme.spacing.unit,
    marginTop: theme.spacing.unit * 2,
    position: 'relative',
    top: '0',
    width: '80%'
  },
  inputField: {
    color: theme.palette.secondary.main
  }
});

SearchBox.propTypes = {
  classes: PropTypes.object.isRequired,
  handleSearchTextChange: PropTypes.func.isRequired
};

export default compose(
  withStyles(styles, { withTheme: true })
)(SearchBox);
