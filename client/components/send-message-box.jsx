import React, { Component } from 'react';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';

import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import FormControl from '@material-ui/core/FormControl';
import Input from '@material-ui/core/Input';
import InputAdornment from '@material-ui/core/InputAdornment';
import Button from '@material-ui/core/Button';
import SendIcon from '@material-ui/icons/Send';
import { userSelector } from '../redux/selectors';
import { showActionModal, showAuthModal, showSnackBarNotification, sendTopicMessage } from '../redux/actions';
import emailTypes from '../../server/services/auth-management/notification-email-types';
import { getModalDetails } from '../lib/modal-messages';

import { withRouter } from 'next/router'

class SendMessageBox extends Component {
  constructor(props) {
    super(props);
    this.state = {
      messageText: ''
    };
  }

  _handleTextChange = prop => event => {
    this.setState({ [prop]: event.target.value });
  };

  _sendMessage = async () => {
    const { router } = this.props;

    if (!this.state.messageText) return;
    if (!this.props.loggedInUserUUID) return this.props.showSnackBarNotification('No active login session. Please login to post message.', { type: 'info' });
    const message = {
      text: this.state.messageText,
      senderUUID: this.props.loggedInUserUUID
    };
    switch (this.props.sendToEntity.toUpperCase()) {
      case 'TOPIC':
        message.topicUUID = this.props.sendToUUID;
        break;
      case 'TOPICMESSAGE':
        message.parentTopicMessageUUID = this.props.sendToUUID;
        message.topicUUID = router.query.topicUUID;
        break;
      default:
        this.props.showSnackBarNotification('Not sure what to do. Invalid sendToEntity prop. Please contact support team.', { type: 'error' });
        return;
    }
    this.props.sendMessage(message);
    this.setState({ messageText: '' });
  };

  render() {
    const { classes, loggedInUserUUID, loggedInUser, searchPlaceholder, showVerifyEmailMessage, showAuthModal } = this.props;
    return (
      <List className={classes.root}>
        <ListItem className={classes.listItem}>
          {!!loggedInUserUUID && (loggedInUser.isVerified || loggedInUser.googleId || loggedInUser.facebookId || loggedInUser.twitterId || loggedInUser.githubId) &&
            <FormControl fullWidth className={classes.messageBox}>
              <Input
                id="adornment-amount"
                value={this.state.messageText}
                multiline={true}
                onChange={this._handleTextChange('messageText')}
                placeholder={
                  searchPlaceholder ||
                  'Type a message (multi-line allowed)'
                }
                // startAdornment={
                //   <InputAdornment position="start">
                //     <svg
                //       xmlns="http://www.w3.org/2000/svg"
                //       width="18"
                //       height="18"
                //       viewBox="0 0 18 18"
                //     >
                //       <path d="M6 8c.55 0 1-.45 1-1s-.45-1-1-1-1 .45-1 1 .45 1 1 1zm6 0c.55 0 1-.45 1-1s-.45-1-1-1-1 .45-1 1 .45 1 1 1zm-3 5.5c2.14 0 3.92-1.5 4.38-3.5H4.62c.46 2 2.24 3.5 4.38 3.5zM9 1C4.57 1 1 4.58 1 9s3.57 8 8 8 8-3.58 8-8-3.58-8-8-8zm0 14.5c-3.59 0-6.5-2.91-6.5-6.5S5.41 2.5 9 2.5s6.5 2.91 6.5 6.5-2.91 6.5-6.5 6.5z" />
                //     </svg>
                //   </InputAdornment>
                // }
                endAdornment={
                  <InputAdornment position="start" onClick={this._sendMessage}>
                    {<SendIcon />}
                  </InputAdornment>
                }
              />
            </FormControl>
          }

          {/*
            If the user is not loggedin using social login, dont let him/her post unless he/she verifies the identity.
            For social logins, we do not need to verify identity, as its self-validated.
          */}
          {!!loggedInUserUUID && !loggedInUser.isVerified && !loggedInUser.googleId && !loggedInUser.facebookId &&
            <Button
              className={classes.rightMostAppBarButton}
              onClick={() => { showVerifyEmailMessage(loggedInUser) }}
              fullWidth={true}
            >
              Verifiy your email address
            </Button>
          }

          {!loggedInUserUUID &&
            <Button
              className={classes.rightMostAppBarButton}
              onClick={showAuthModal}
              fullWidth={true}
            >
              Login to post messages
            </Button>
          }
        </ListItem>
      </List>
    );
  }
}

const styles = theme => ({
  root: {
    position: 'sticky',
    bottom: '0',
    padding: '0px',
    borderRight: '1px solid #dddddd'
  },
  listItem: {
    backgroundColor: '#eeeeee'//'#d8f7ff'
  },
  messageBox: {
    margin: theme.spacing.unit,
    marginTop: theme.spacing.unit * 3,
    position: 'relative',
    top: '0',
    width: '95%'
    // minHeight: '62%'
  }
});

const mapStateToProps = state => {
  const loggedInUser = userSelector(state, { uuid: state.loggedInUserUUID });
  return {
    loggedInUserUUID: state.loggedInUserUUID,
    loggedInUser
  };
};

const mapDispatchToProps = dispatch => {
  return {
    sendMessage: topicMessage => dispatch(sendTopicMessage(topicMessage)),
    showVerifyEmailMessage: (user) => dispatch(showActionModal(getModalDetails(emailTypes.SEND_VERIFY_ACCOUNT, { user }))),
    showAuthModal: () => dispatch(showAuthModal()),
    showSnackBarNotification: (displayMessage, options) => dispatch(showSnackBarNotification(displayMessage, options))
  };
};

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  withRouter,
  withStyles(styles, { withTheme: true })
)(SendMessageBox);
