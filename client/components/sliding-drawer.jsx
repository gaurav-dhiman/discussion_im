import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';

import Hidden from '@material-ui/core/Hidden';
import Drawer from '@material-ui/core/Drawer';

class SlidingDrawer extends Component {
  render() {
    const { classes } = this.props;

    return (
      <div>
        <Hidden mdUp implementation="css">
          <Drawer
            variant="temporary"
            anchor={this.props.anchor || 'left'}
            open={this.props.drawerOpen}
            onClose={this.props.handleDrawerToggle}
            classes={{ paper: classes.drawerPaper }}
            ModalProps={
              { keepMounted: true } // Better open performance on mobile.
            }
          >
            {this.props.children}
          </Drawer>
        </Hidden>

        <Hidden smDown implementation="css">
          <Drawer
            variant={this.props.variant || "permanent"}
            classes={{
              paper: classNames(
                classes.drawerPaper,
                !this.props.drawerOpen && classes.drawerPaperClose,
                !this.props.drawerOpen && this.props.anchor === 'right' && classes.rightDrawerPaperClose
              )
            }}
            open={this.props.drawerOpen}
          >
            {this.props.children}
          </Drawer>
        </Hidden>
      </div>
    );
  }
}

export const DRAWER_WIDTH = 340;
const styles = theme => ({
  drawerPaper: {
    float: 'left',
    width: DRAWER_WIDTH,
    overflow: 'auto',
    [theme.breakpoints.down('sm')]: {
      width: 240
    },
    [theme.breakpoints.up('md')]: {
      position: 'relative'
    },
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen
    })
  },
  drawerPaperClose: {
    overflowX: 'hidden',
    overflowY: 'auto',
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen
    }),
    width: theme.spacing.unit * 7,
    [theme.breakpoints.up('sm')]: {
      width: theme.spacing.unit * 9
    }
  },
  rightDrawerPaperClose: {
    overflowX: 'hidden',
    overflowY: 'auto',
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen
    }),
    width: 0,
    [theme.breakpoints.up('sm')]: {
      width: 0
    }
  }
});

export default withStyles(styles, { withTheme: true })(SlidingDrawer);
