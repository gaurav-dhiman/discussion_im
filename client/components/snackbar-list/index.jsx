import React, { Component } from 'react';
import { compose } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { withSnackbar } from 'notistack';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';

import { removeSnackbarNotification } from '../../redux/actions';

class SnackbarList extends React.Component {


  _dismiss = (key, notificationId) => {
    const { closeSnackbar, removeSnackbarNotification } = this.props;
    removeSnackbarNotification(notificationId);
    closeSnackbar(key)
  };

  render() {
    const { notifications, enqueueSnackbar, removeSnackbarNotification } = this.props;
    notifications.map(notification => {
      enqueueSnackbar(notification.displayMessage, {
        variant: notification.type,
        action: (key) => {
          return (
            <Button onClick={() => this._dismiss(key, notification.id)}>
              {'Dismiss'}
            </Button>
          )
        }
      });
      removeSnackbarNotification(notification.id);
    });
    return <div />;
  }
}

const styles = theme => ({
  margin: {
    margin: theme.spacing.unit,
  },
});

SnackbarList.propTypes = {
  classes: PropTypes.object.isRequired,
};

const mapStateToProps = (state, props) => {
  return {
    notifications: state.ui.snackbarNotifications
  };
};

const mapDispatchToProps = dispatch => {
  return {
    removeSnackbarNotification: (id) => dispatch(removeSnackbarNotification(id))
  };
};

let SnackbarListComp = compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  withStyles(styles, { withTheme: true })
)(SnackbarList);

export default withSnackbar(SnackbarListComp)