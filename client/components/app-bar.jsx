import React, { Component } from 'react';
import { compose } from 'redux';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';

import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';

import MainNav from '../components/main-nav';
import SlidingDrawer, { DRAWER_WIDTH } from '../components/sliding-drawer';

class Appbar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showDrawer: false
    };
  }

  _handleDrawerToggle = () => {
    this.setState({ showDrawer: !this.state.showDrawer });
  };

  render() {
    const { classes, hideMainNav } = this.props;
    const { showDrawer } = this.state;
    return (
      <React.Fragment>
        <AppBar
          className={classNames(
            classes.appBar,
            showDrawer && classes.appBarShift
          )}
        >
          <Toolbar>
            {!hideMainNav && !showDrawer &&
              <IconButton
                className={classes.leftMostAppBarButton}
                color="inherit"
                aria-label="Nav"
                onClick={this._handleDrawerToggle}
              >
                <MenuIcon />
              </IconButton>}
            {this.props.children}
          </Toolbar>
        </AppBar>

        {/* Have main navigation in left side drawer */}
        {!hideMainNav &&
          <SlidingDrawer
            drawerOpen={showDrawer}
            handleDrawerToggle={this._handleDrawerToggle}
            drawerWidth={DRAWER_WIDTH}
          >
            <MainNav handleDrawerToggle={this._handleDrawerToggle} />
          </SlidingDrawer>
        }
      </React.Fragment>
    );
  }
}

const styles = theme => ({
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen
    })
  },
  appBarShift: {
    [theme.breakpoints.up('md')]: {
      marginLeft: DRAWER_WIDTH,
      width: `calc(100% - ${DRAWER_WIDTH}px)`,
      transition: theme.transitions.create(['width', 'margin'], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.enteringScreen
      })
    }
  },
  leftMostAppBarButton: {
    marginLeft: -12,
    marginRight: 20
  }
});

export default compose(
  withStyles(styles, { withTheme: true })
)(Appbar);
