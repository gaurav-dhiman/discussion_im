import React, { Component } from 'react';
import { compose } from 'redux';
import { connect } from 'react-redux';
import memoize from 'memoize-one';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Dotdotdot from 'react-dotdotdot'

import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';

import Link from 'next/link';
import { withRouter } from 'next/router'

import api, { service } from '../lib/api';

import {
  getTopics,
  addTopicsToReduxStore
} from '../redux/actions';

class TopicList extends Component {
  constructor(props) {
    super(props);
    this.prevSearchText = '';
    // Subscribe to Topic events from server.
    api.service(service.topics).on('created', topic => {
      this.props.addTopicsToReduxStore(topic);
    });
    api.service(service.topics).on('updated', topic => {
      this.props.addTopicsToReduxStore(topic);
    });
    api.service(service.topics).on('patched', topic => {
      this.props.addTopicsToReduxStore(topic);
    });
    api.service(service.topics).on('removed', topic => {
      // this.props.addTopicsToReduxStore(topic);
    });
  }

  componentDidUpdate() {
    const { searchText, getTopics } = this.props;
    if (!searchText) return;
    if (searchText === this.prevSearchText) return;
    this.prevSearchText = searchText;
    getTopics({
      query: {
        $text: { $search: searchText }
      }
    });
  }

  // Re-run the filter whenever the list array or filter text changes:
  _filter = memoize((list, filterText = '') => {
    if (!filterText) return list;
    return (list || []).filter(item => {
      const regex = new RegExp(filterText, 'i');
      if ((item.title || '').match(regex) || (item.description || '').match(regex))
        return item;
    })
  });

  render() {
    const { classes, topics, searchText, router } = this.props;
    const data = this._filter(topics.data, searchText);
    let NoTopicMessage;

    if (!data.length) {
      if (searchText) {
        NoTopicMessage = `No topics that matches your search.`
      } else {
        NoTopicMessage = `You don't have any topics in your list. To add topics to your list or create new topics, click add button. `
      }
      return (
        <div className={classes.noMessage}>
          <Typography
            className={classes.blankPagePadding}
            variant={'subtitle1'}
          >
            {NoTopicMessage}
          </Typography>
        </div>
      )
    }
    return data.map((item, i) => {
      const itemStyle = (router.query.topicUUID === item.uuid) ? classes.selectedItem : classes.listItem;
      const description = <Dotdotdot clamp={2} tagName='span'>{item.description}</Dotdotdot>
      return (
        <div key={i} className={itemStyle}>
          <Link as={`/topics/${item.uuid}`} href={`/topics?topicUUID=${item.uuid}`} prefetch passHref>
            <ListItem
              button
              key={i}
            >
              <ListItemText primary={item.title} secondary={description} />
            </ListItem>
          </Link>
          <Divider />
        </div>
      );
    });
  }
}

const styles = theme => ({
  selectedItem: {
    backgroundColor: '#eeeeee'
  },
  listItem: {
  },
  noMessage: {
    flex: '1 1 auto',
    height: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    boxShadow: 'unset'
  },
  blankPagePadding: {
    padding: '10%'
  }
});

TopicList.propTypes = {
  classes: PropTypes.object.isRequired
};

const mapStateToProps = (state, props) => {
  return {};
};

const mapDispatchToProps = dispatch => {
  return {
    getTopics: (queryObj, options) => dispatch(getTopics(queryObj, options)),
    addTopicsToReduxStore: topic => dispatch(addTopicsToReduxStore(topic))
  };
};

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  withRouter,
  withStyles(styles, { withTheme: true })
)(TopicList);
