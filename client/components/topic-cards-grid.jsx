import React, { Component } from 'react';
import { compose } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import withRoot from '../lib/with-root';
import * as moment from 'moment';
import classNames from 'classnames';
import Typography from '@material-ui/core/Typography';
import Tooltip from '@material-ui/core/Tooltip';
import Grid from '@material-ui/core/Grid';

import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import StarFilledIcon from '@material-ui/icons/StarOutlined';
import Dotdotdot from 'react-dotdotdot'
import { starTopic, unstarTopic } from '../redux/actions';

class TopicCardsGrid extends Component {
  _starTopic = (topicUUID) => () => this.props.starTopic(topicUUID);

  _unstarTopic = (topicUUID) => () => this.props.unstarTopic(topicUUID);

  _getTopicCard = (topic) => {
    const { classes, loggedInUserUUID } = this.props;
    topic.staredByUsersUUIDs = topic.staredByUsersUUIDs || {};
    const stared = topic.staredByUsersUUIDs[loggedInUserUUID];
    const numOfParticipants = Object.keys(topic.postCountByUsersUUIDs || {}).length;
    let lastMessageTimestamp = topic.lastMessageTimestamp || topic.updatedAt;
    lastMessageTimestamp = moment(lastMessageTimestamp).fromNow();

    return (
      <Grid key={topic} item>
        <Card className={classes.card}>
          <CardActionArea>
            <CardMedia
              className={classes.media}
              // image="http://gytis.ramiuveidu.lt/wp-content/uploads/2018/03/cropped-html-color-codes-color-tutorials-hero-00e10b1f-1.jpg"
              title=""
            />
            <CardContent>
              <Tooltip placement='right' title={topic.title}>
                <Typography gutterBottom variant="headline" component="h2">
                  <Dotdotdot clamp={2} tagName='span'>
                    {topic.title}
                  </Dotdotdot>
                </Typography>
              </Tooltip>
              <Tooltip placement='right' title={topic.description}>
                <Typography component="p">
                  <Dotdotdot clamp={4} tagName='span'>
                    {topic.description}
                  </Dotdotdot>
                </Typography>
              </Tooltip>
              <br />
              <Typography component="p" className={classes.keyDetails}>
                Total message: {topic.totalMessageCount}
              </Typography>
              <Typography component="p" className={classes.keyDetails}>
                Paticipants: {numOfParticipants}
              </Typography>
              <Typography component="p" className={classes.keyDetails}>
                Last posted message: {lastMessageTimestamp}
              </Typography>
            </CardContent>
          </CardActionArea>
          <CardActions>
            {!stared &&
              <Button size="small" onClick={this._starTopic(topic.uuid)}>
                <StarFilledIcon fontSize="small" color={'primary'} className={classNames(classes.leftIcon, classes.iconSmall)} />
                Add to my list
            </Button>
            }
            {!!stared &&
              <Button size="small" onClick={this._unstarTopic(topic.uuid)}>
                <StarFilledIcon fontSize="small" color={'secondary'} className={classNames(classes.leftIcon, classes.iconSmall)} />
                Remove from my list
            </Button>
            }
          </CardActions>
        </Card>
      </Grid>
    );
  }

  render() {
    const {
      classes,
      topics
    } = this.props;

    return (
      <React.Fragment>
        <Grid container className={classes.gridContainer} justify="center" spacing={Number(32)}>
          {(topics || []).map(topic => this._getTopicCard(topic))}
        </Grid>
      </React.Fragment>
    );
  }
}

const styles = theme => ({
  gridContainer: {
    width: '100%',
    margin: '0px'
  },
  card: {
    maxWidth: '350px',
  },
  keyDetails: {
    fontSize: '12px',
    color: '#8f8f8f'
  },
  media: {
    height: 20,
    backgroundColor: theme.palette.primary.main
  },
  leftIcon: {
    marginRight: theme.spacing.unit,
  },
  iconSmall: {
    fontSize: 20,
  }
})

const mapStateToProps = (state, props) => {
  return {
    loggedInUserUUID: state.loggedInUserUUID
  };
};

const mapDispatchToProps = dispatch => {
  return {
    starTopic: topicUUID => dispatch(starTopic(topicUUID)),
    unstarTopic: topicUUID => dispatch(unstarTopic(topicUUID)),
  };
};

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  withRoot,
  withStyles(styles, { withTheme: true })
)(TopicCardsGrid);
