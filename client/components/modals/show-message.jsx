import React, { Component } from 'react';
import { compose } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import { withStyles } from '@material-ui/core/styles';

import { login } from '../../../redux/actions';
import { Typography } from '@material-ui/core';

class ShowMessageModal extends Component {
  state = {};

  // _login = async () => {
  //   let { loginCred } = this.state;
  //   if (!loginCred) return;
  //   loginCred.strategy = 'local';
  //   this.props.login(loginCred);
  // };

  // _handleTextChange = (form, field) => event => {
  //   if (!form || !field) return;
  //   const { value } = event.target;
  //   this.setState({ [form]: { ...this.state[form], [field]: value }, loginErrorMsg: '' });
  // };

  // _closeModal = () => {
  //   this.setState({
  //     loginCred: undefined
  //   });
  //   this.props.closeModal();
  // }

  render() {
    const { classes } = this.props;

    return (
      <div className={classes.form}>
        <DialogTitle id="form-dialog-title">Login to your account</DialogTitle>
        <DialogContent>
          <Typography>{this.props.message}</Typography>
        </DialogContent>
        <DialogActions>
          <Button variant="raised" color="primary" onClick={this.props.closeModal}>
            OK
          </Button>
        </DialogActions>
        {/* <hr className={classes.lineLength} />
        <DialogContent>
          <DialogContentText>
            <span className={classes.link} onClick={() => this.props.showForm({ showForgetPasswordForm: true })}>Forgot password</span> |
            <span className={classes.link} onClick={() => this.props.showForm({ showSignupForm: true })}> Create free account</span>
          </DialogContentText>
        </DialogContent> */}
      </div>
    );
  }
}

const styles = theme => ({
  form: {
    padding: '25px'
  },
  lineLength: {
    width: '100%'
  },
  link: {
    color: '#00bcd4',
    fontSize: '13px',
    cursor: 'pointer'
  }
});

const mapStateToProps = state => {
  return {};
};

const mapDispatchToProps = dispatch => {
  return {
    login: loginCred => dispatch(login(loginCred)),
  };
};

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  withStyles(styles, { withTheme: true })
)(ShowMessageModal);
