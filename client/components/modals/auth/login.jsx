import React, { Component } from 'react';
import { compose } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import { Google as GoogleIcon, Facebook as FacebookIcon, TwitterBox as TwitterIcon, GithubBox as GitHubIcon } from 'mdi-material-ui'
import TextField from '@material-ui/core/TextField';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import CardHeader from '@material-ui/core/CardHeader';
import Avatar from 'react-avatar';
import { withStyles } from '@material-ui/core/styles';

import { login, hideAuthModal, sendResetPassword, showActionModal } from '../../../redux/actions';
import emailTypes from '../../../../server/services/auth-management/notification-email-types';
import { getModalDetails } from '../../../lib/modal-messages';

class LoginForm extends Component {
  state = {};

  _login = async () => {
    let { loginCred } = this.state;
    if (!loginCred) return;
    loginCred.email = this.props.email;
    loginCred.strategy = 'local';
    this.props.login(loginCred);
  };

  _sendResetPasswordEmail = () => {
    const { email, user, closeAuthModal, sendResetPasswordEmail, showVerifyEmailMessage } = this.props;
    if (user.verifyToken) {
      closeAuthModal();
      return showVerifyEmailMessage(user);
    }

    sendResetPasswordEmail({
      action: emailTypes.SEND_RESET_PASSWORD,
      value: { email }
    });
    closeAuthModal();
  };

  _handleTextChange = (form, field) => event => {
    if (!form || !field) return;
    const { value } = event.target;
    this.setState({ [form]: { ...this.state[form], [field]: value }, loginErrorMsg: '' });
  };

  render() {
    const { firstName, profileImageURL, googleId, facebookId, twitterId, githubId, showLoginForm, classes, closeAuthModal } = this.props;
    const isSocial = googleId || facebookId || twitterId || githubId;
    let socialPlatform = googleId ? 'Google' : '';
    socialPlatform = facebookId ? 'Facebook' : '';
    socialPlatform = twitterId ? 'Twitter' : '';
    socialPlatform = githubId ? 'GitHub' : '';
    const enableLoginButton = this.state.loginCred && this.state.loginCred.password;

    if (!showLoginForm) return <div />
    return (
      <div className={classes.form}>
        <DialogTitle id="signin-title">Login</DialogTitle>

        {!!isSocial &&
          <DialogContent>
            <CardHeader
              className={classes.profile}
              avatar={<Avatar name={firstName} src={profileImageURL} size="40" round={true} color='#f44336' />}
              title={`Hi ${firstName}`}
              subheader={`We identified that you created account using ${socialPlatform}.`}
            />

            {!!facebookId &&
              <DialogActions>
                <Button variant="raised" fullWidth className={classes.facebookLoginButton} onClick={this._login}>
                  <FacebookIcon className={classes.leftIcon} />
                  <a className={classes.socialLink} href="/auth/facebook">Login with Facebook</a>
                </Button>
              </DialogActions>
            }

            {!!googleId &&
              <DialogActions>
                <Button variant="raised" fullWidth className={classes.googleLoginButton} onClick={this._login}>
                  <GoogleIcon className={classes.leftIcon} />
                  <a className={classes.socialLink} href="/auth/google">Login with Google</a>
                </Button>
              </DialogActions>
            }

            {!!twitterId &&
              <DialogActions>
                <Button variant="raised" fullWidth className={classes.twitterLoginButton}>
                  <TwitterIcon className={classes.leftIcon} />
                  <a className={classes.socialLink} href="/auth/twitter">Login with Twitter</a>
                </Button>
              </DialogActions>
            }

            {!!githubId &&
              <DialogActions>
                <Button variant="raised" fullWidth className={classes.githubLoginButton}>
                  <GitHubIcon className={classes.leftIcon} />
                  <a className={classes.socialLink} href="/auth/github">Login with GitHub</a>
                </Button>
              </DialogActions>
            }
            <DialogActions>
              <Button color="primary" variant="outlined" fullWidth onClick={closeAuthModal}>
                Cancel
              </Button>
            </DialogActions>
          </DialogContent>
        }

        {!isSocial &&
          <DialogContent>
            <CardHeader
              className={classes.profile}
              avatar={<Avatar name={firstName} src={profileImageURL} size="40" round={true} color='#f44336' />}
              title={`Hi ${firstName}`}
              subheader={`Please enter your password.`}
            />

            <TextField
              margin="dense"
              required
              onChange={this._handleTextChange('loginCred', 'password')}
              id="password"
              label="Password"
              type="password"
              fullWidth
            />

            <DialogActions>
              <Button color="primary" variant="outlined" onClick={closeAuthModal}>
                Cancel
              </Button>
              <Button variant="raised" disabled={!enableLoginButton} color="primary" onClick={this._login}>
                Sign in
              </Button>
            </DialogActions>

            <DialogContentText>
              <span className={classes.link} onClick={this._sendResetPasswordEmail}>Forgot password ?</span>
            </DialogContentText>
          </DialogContent>
        }

      </div>
    );
  }
}

const styles = theme => ({
  form: {
    padding: '25px',
    [theme.breakpoints.up('md')]: {
      width: '600px'
    }
  },
  profile: {
    paddingTop: '5px',
    paddingBottom: '30px',
    paddingLeft: '0px'
  },
  link: {
    color: '#00bcd4',
    fontSize: '13px',
    cursor: 'pointer'
  },
  leftIcon: {
    marginRight: theme.spacing.unit,
  },
  socialLink: {
    textDecoration: 'none',
    color: '#ffffff'
  },
  googleLoginButton: {
    color: theme.palette.getContrastText('#DB4437'),
    backgroundColor: '#DB4437',
    '&:hover': {
      backgroundColor: '#DB4437',
    }
  },
  facebookLoginButton: {
    color: theme.palette.getContrastText('#3C5A99'),
    backgroundColor: '#3C5A99',
    '&:hover': {
      backgroundColor: '#3C5A99',
    }
  },
  twitterLoginButton: {
    color: '#fff',
    backgroundColor: '#38A1F3',
    '&:hover': {
      backgroundColor: '#38A1F3',
    }
  },
  githubLoginButton: {
    color: theme.palette.getContrastText('#24292e'),
    backgroundColor: '#24292e',
    '&:hover': {
      backgroundColor: '#24292e',
    }
  }
});

const mapStateToProps = state => {
  const { firstName, email, profileImageURL, googleId, facebookId, twitterId, githubId } = state.auth.user || {};
  const { showLoginForm, user } = state.auth;
  return {
    showLoginForm,
    firstName,
    email,
    profileImageURL,
    googleId,
    facebookId,
    twitterId,
    githubId,
    user
  };
};

const mapDispatchToProps = dispatch => {

  return {
    login: loginCred => dispatch(login(loginCred)),
    closeAuthModal: () => dispatch(hideAuthModal()),
    sendResetPasswordEmail: payload => dispatch(sendResetPassword(payload)),
    showVerifyEmailMessage: () => {
      const modal = getModalDetails(emailTypes.SEND_VERIFY_ACCOUNT);
      modal.message = 'As your account is not verified yet, we can not send you an email to reset password. Please verify your account first, before you can reset the password.';

      dispatch(showActionModal(modal))
    }
  };
};

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  withStyles(styles, { withTheme: true })
)(LoginForm);
