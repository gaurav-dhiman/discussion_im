import React, { Component } from 'react';
import { compose } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import { Google as GoogleIcon, FacebookBox as FacebookIcon, TwitterBox as TwitterIcon, GithubBox as GitHubIcon } from 'mdi-material-ui'
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import { withStyles } from '@material-ui/core/styles';

import { signUp, hideAuthModal } from '../../../redux/actions';

class SignupForm extends Component {
  state = {};

  _signUp = () => {
    const { newUser } = this.state;
    newUser.email = this.props.email;
    this.props.signUp(newUser);
  }

  _handleTextChange = (form, field) => event => {
    if (!form || !field) return;
    const { value } = event.target;
    this.setState({ [form]: { ...this.state[form], [field]: value } });
  };

  render() {
    const { classes, showSignupForm, email, closeAuthModal } = this.props;
    const { newUser = {} } = this.state;
    const enableSignupButton = newUser.firstName && newUser.lastName && newUser.password;

    if (!showSignupForm) return <div />
    return (
      <div className={classes.form}>
        <DialogTitle id="signup-title">Login</DialogTitle>

        <DialogActions>
          <Button variant="raised" fullWidth className={classes.googleLoginButton}>
            <GoogleIcon className={classes.leftIcon} />
            <a className={classes.socialLink} href="/auth/google">Login with Google</a>
          </Button>

          <Button variant="raised" fullWidth className={classes.facebookLoginButton}>
            <FacebookIcon className={classes.leftIcon} />
            <a className={classes.socialLink} href="/auth/facebook">Login with Facebook</a>
          </Button>
        </DialogActions>

        <DialogActions>
          <Button variant="raised" fullWidth className={classes.twitterLoginButton}>
            <TwitterIcon className={classes.leftIcon} />
            <a className={classes.socialLink} href="/auth/twitter">Login with Twitter</a>
          </Button>

          <Button variant="raised" fullWidth className={classes.githubLoginButton}>
            <GitHubIcon className={classes.leftIcon} />
            <a className={classes.socialLink} href="/auth/github">Login with GitHub</a>
          </Button>
        </DialogActions>

        <Typography className={classes.centerText} >--- OR ---</Typography>

        <DialogTitle id="signup-title" className={classes.signupLabel}>Sign up</DialogTitle>

        <DialogContent>
          <TextField
            autoFocus
            margin="dense"
            required
            onChange={this._handleTextChange('newUser', 'firstName')}
            id="firstName"
            label="First Name"
            type="text"
            fullWidth
          />
          <TextField
            margin="dense"
            required
            onChange={this._handleTextChange('newUser', 'lastName')}
            id="lastName"
            label="Last Name"
            type="text"
            fullWidth
          />
          <TextField
            margin="dense"
            required
            value={email}
            disabled
            id="email"
            label="Email Address"
            type="email"
            fullWidth
          />
          <TextField
            margin="dense"
            required
            onChange={this._handleTextChange('newUser', 'password')}
            id="password"
            label="Password"
            type="password"
            fullWidth
          />
        </DialogContent>

        <DialogActions>
          <Button color="primary" variant="outlined" onClick={closeAuthModal}>
            Cancel
          </Button>
          <Button variant="raised" disabled={!enableSignupButton} color="primary" onClick={this._signUp}>
            Signup
          </Button>
        </DialogActions>

      </div >
    );
  }
}

const styles = theme => ({
  form: {
    padding: '25px'
  },
  centerText: {
    textAlign: 'center',
    marginTop: '20px',
    marginBottom: '20px',
  },
  leftIcon: {
    marginRight: theme.spacing.unit,
  },
  signupLabel: {
    paddingTop: '0px'
  },
  socialLink: {
    textDecoration: 'none',
    color: '#ffffff'
  },
  googleLoginButton: {
    color: theme.palette.getContrastText('#DB4437'),
    backgroundColor: '#DB4437',
    '&:hover': {
      backgroundColor: '#DB4437',
    }
  },
  facebookLoginButton: {
    color: theme.palette.getContrastText('#4267b2'),
    backgroundColor: '#4267b2',
    '&:hover': {
      backgroundColor: '#4267b2',
    }
  },
  twitterLoginButton: {
    color: '#fff',
    backgroundColor: '#38A1F3',
    '&:hover': {
      backgroundColor: '#38A1F3',
    }
  },
  githubLoginButton: {
    color: theme.palette.getContrastText('#24292e'),
    backgroundColor: '#24292e',
    '&:hover': {
      backgroundColor: '#24292e',
    }
  }
});

const mapStateToProps = state => {
  const { email } = state.auth.user || {};
  const { showSignupForm } = state.auth;
  return {
    showSignupForm,
    email
  };
};

const mapDispatchToProps = dispatch => {
  return {
    signUp: newUser => dispatch(signUp(newUser)),
    closeAuthModal: () => dispatch(hideAuthModal())
  };
};

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  withStyles(styles, { withTheme: true })
)(SignupForm);
