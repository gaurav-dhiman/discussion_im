import React, { Component } from 'react';
import { compose } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Dialog from '@material-ui/core/Dialog';
import withMobileDialog from '@material-ui/core/withMobileDialog';
import { withStyles } from '@material-ui/core/styles';

import AskEmailIdForm from './ask-email-id';
import LoginForm from './login';
import SignupForm from './sign-up';

class AuthModal extends Component {
  render() {
    const { fullScreen, classes } = this.props;

    return (
      <div>
        <Dialog
          fullScreen={fullScreen}
          open={this.props.showAuthModal}
          onClose={this._closeModal}
          aria-labelledby="form-dialog-title"
        >
          {/* First thing - ask email id */}
          {<AskEmailIdForm />}

          {/* sign-in section */}
          {<LoginForm />}

          {/* create account section */}
          {<SignupForm />}
        </Dialog>
      </div>
    );
  }
}

AuthModal.propTypes = {
  fullScreen: PropTypes.bool.isRequired
};

const styles = theme => ({});

const mapStateToProps = state => {
  return {
    showAuthModal: state.ui.showAuthModal
  };
};

const mapDispatchToProps = dispatch => {
  return {};
};

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  withStyles(styles, { withTheme: true }),
  withMobileDialog()
)(AuthModal);
