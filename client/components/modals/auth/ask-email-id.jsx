import React, { Component } from 'react';
import { compose } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import withMobileDialog from '@material-ui/core/withMobileDialog';
import { withStyles } from '@material-ui/core/styles';

import { getUserByEmailId, hideAuthModal } from '../../../redux/actions';

class AskEmailIdForm extends Component {
  state = {};

  _submit = () => {
    this.props.getUser(this.state.user.email);
  };

  _handleTextChange = (form, field) => event => {
    if (!form || !field) return;
    const { value } = event.target;
    this.setState({ [form]: { ...this.state[form], [field]: value } });
  };

  render() {
    const { classes, showAskEmailIdForm, closeAuthModal } = this.props;
    const emailRegex = /^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/g;
    const enableContinueButton = this.state.user && this.state.user.email && this.state.user.email.match(emailRegex);

    if (!showAskEmailIdForm) return <div />
    return (
      <div className={classes.form}>
        <DialogContent>
          <Typography variant='headline' className={classes.header} >Enter your email id.</Typography>

          <TextField
            autoFocus
            margin="dense"
            required
            onChange={this._handleTextChange('user', 'email')}
            id="emailId"
            label="Email ID"
            type="email"
            fullWidth
          />
        </DialogContent>
        <DialogActions>
          <Button color="primary" onClick={closeAuthModal}>
            Cancel
          </Button>
          <Button variant="raised" disabled={!enableContinueButton} color="primary" onClick={this._submit}>
            Continue
          </Button>
        </DialogActions>
      </div>
    );
  }
}

const styles = theme => ({
  form: {
    padding: '25px',
    [theme.breakpoints.up('md')]: {
      width: '600px'
    }
  },
  header: {
    paddingBottom: '20px',
  }
});

const mapStateToProps = state => {
  const { showAskEmailIdForm } = state.auth;
  return {
    showAskEmailIdForm
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getUser: email => dispatch(getUserByEmailId(email)),
    closeAuthModal: () => dispatch(hideAuthModal())
  };
};

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  withStyles(styles, { withTheme: true }),
  withMobileDialog()
)(AskEmailIdForm);
