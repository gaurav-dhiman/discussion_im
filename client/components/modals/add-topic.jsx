import React, { Component } from 'react';
import { compose } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import withRoot from '../../lib/with-root';
import Typography from '@material-ui/core/Typography';

import Dialog from '@material-ui/core/Dialog';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import TextField from '@material-ui/core/TextField';
import Paper from '@material-ui/core/Paper';
import Slide from '@material-ui/core/Slide';

import { hideAddTopicModal, createTopic } from '../../redux/actions';

class AddTopicModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: props.addTopicModal.searchText || '',
      description: ''
    };
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.addTopicModal.searchText !== this.props.addTopicModal.searchText)
      this.setState({ title: this.props.addTopicModal.searchText });
  }


  _handleTextChange = name => event => {
    this.setState({
      [name]: event.target.value
    });
  };

  _addNewTopic = async () => {
    const topic = this.state;
    this.props.addNewTopic(topic);
    this.props.closeAddTopicModal();
  };

  _getSlideTransition(props) {
    return <Slide direction="up" {...props} />;
  }

  render() {
    const { classes, addTopicModal, closeAddTopicModal } = this.props;
    const enableSaveButton = this.state.title.length > 3;
    return (
      <Dialog
        fullScreen
        open={addTopicModal.show}
        onClose={closeAddTopicModal}
        TransitionComponent={this._getSlideTransition}
      >
        <AppBar className={classes.appBar}>
          <Toolbar>
            <Typography
              variant="subheading"
              color="inherit"
              className={classes.flex}
            >
              Add New Topic
            </Typography>
            <IconButton
              color="inherit"
              onClick={closeAddTopicModal}
              className={classes.rightMostAppBarButton}
              aria-label="Close"
            >
              <CloseIcon />
            </IconButton>
          </Toolbar>
        </AppBar>
        <Paper className={classes.paper} elevation={0}>
          <Typography variant="headline" component="h5">
            Please avoid creating duplicate or similar topics:
          </Typography>
          <Typography component="p">
            We hope you tried searching the topic of your interest, before you continue to create it.
          </Typography>
        </Paper>

        <form className={classes.formContainer} noValidate autoComplete="off">
          <TextField
            id="title"
            label="Title"
            placeholder="E.g: I-485 filers of March 2017"
            className={classes.textField}
            value={this.state.title}
            onChange={this._handleTextChange('title')}
            margin="normal"
          />
          <TextField
            id="description"
            label="Details"
            placeholder="Max 1000 char, multiple lines allowed"
            multiline
            value={this.state.description}
            onChange={this._handleTextChange('description')}
            className={classes.textField}
            margin="normal"
          />
          <Button variant="contained" className={classes.saveButton} disabled={!enableSaveButton} color="primary" onClick={this._addNewTopic}>
            Save Topic
        </Button>
        </form>

      </Dialog>
    );
  }
}

const styles = theme => ({
  appBar: {
    position: 'relative'
  },
  flex: {
    flex: '1 1 auto'
  },
  leftMostAppBarButton: {
    marginLeft: -12,
    marginRight: 20
  },
  rightMostAppBarButton: {
    marginLeft: 20,
    marginRight: -12
  },
  formContainer: {
    maxWidth: '95%',
    textAlign: 'center'
  },
  textField: {
    marginLeft: theme.spacing.unit * 3,
    marginRight: theme.spacing.unit * 3,
    width: '90%'
  },
  paper: theme.mixins.gutters({
    marginTop: theme.spacing.unit * 3
  }),
  saveButton: {
    margin: theme.spacing.unit,
    marginTop: theme.spacing.unit * 5
  }
});

const mapStateToProps = state => {
  return {
    addTopicModal: state.ui.addTopicModal
  };
};

const mapDispatchToProps = dispatch => {
  return {
    closeAddTopicModal: () => dispatch(hideAddTopicModal()),
    addNewTopic: topic => dispatch(createTopic(topic))
  };
};

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  withRoot,
  withStyles(styles, { withTheme: true })
)(AddTopicModal);
