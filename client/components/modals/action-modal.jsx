import React, { Component } from 'react';
import { compose } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogActions from '@material-ui/core/DialogActions';
import Button from '@material-ui/core/Button';
import withMobileDialog from '@material-ui/core/withMobileDialog';
import { withStyles } from '@material-ui/core/styles';

class ActionModal extends Component {
  render() {
    const { fullScreen, modal, dispatch } = this.props;

    return (
      <div>
        <Dialog
          fullScreen={fullScreen}
          open={modal.show}
          onClose={this._closeModal}
          aria-labelledby="action-modal"
        >
          <DialogTitle id="modal-title">{modal.title}</DialogTitle>
          <DialogContent>
            <DialogContentText id="modal-message">
              {modal.message}
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            {(modal.buttons || []).map((button={}, i) => {
              return (
                <Button key={i} variant={button.variant || 'outlined'} onClick={() => dispatch(button.action)} color={button.color || 'primary'}>
                  {button.label}
                </Button>
              );
            })}
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}

ActionModal.propTypes = {
  fullScreen: PropTypes.bool.isRequired
};

const styles = theme => ({});

const mapStateToProps = state => {
  return {
    modal: state.ui.actionModal || {}
  };
};

const mapDispatchToProps = dispatch => {
  return {
    dispatch
  };
};

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  withStyles(styles, { withTheme: true }),
  withMobileDialog()
)(ActionModal);
