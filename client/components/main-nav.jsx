import React, { Component } from 'react';
import { compose } from 'redux';
import { connect } from 'react-redux';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import Link from 'next/link';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import {
  Home as HomeIcon,
  LockOpen as LoginIcon,
  Lock as LogoutIcon,
  ChevronLeft as ChevronLeftIcon
} from '@material-ui/icons';
import { AndroidMessages as ChatIcon, FileLock as PrivacyIcon, FileDocumentBoxCheck as TermsIcon } from 'mdi-material-ui'
import Divider from '@material-ui/core/Divider';
import Tooltip from '@material-ui/core/Tooltip';
import Badge from '@material-ui/core/Badge';
import Avatar from 'react-avatar';
import IconButton from '@material-ui/core/IconButton';
import { userSelector } from '../redux/selectors';
import { showAuthModal, logout } from '../redux/actions';
import DeepLink from '../components/deep-link';
import StarFilledIcon from '@material-ui/icons/StarOutlined';

class MainNav extends Component {
  render() {
    const { classes, loggedInUserUUID, loggedInUser, handleDrawerToggle, login, logout } = this.props;
    return (
      <div>
        <List className={classes.fullHeight}>

          <div className={classNames(classes.toolbar, classes.toolbarBackground)} >
            <IconButton className={classes.chevronButton} onClick={handleDrawerToggle}>
              <ChevronLeftIcon color='secondary' />
            </IconButton>
          </div>
          <Divider />

          <Tooltip title="Home - Landing Page">
            <div>
              <Link prefetch passHref href="/">
                <a target="_blank">
                  <ListItem button className={classes.menuItem}>
                    <ListItemIcon>
                      <HomeIcon />
                    </ListItemIcon>
                    <ListItemText primary="Home" />
                  </ListItem>
                </a>
              </Link>
              <Divider />
            </div>
          </Tooltip>

          {!loggedInUserUUID &&
            <Tooltip title={`Signup / Login`}>
              <div>
                <ListItem button className={classes.menuItem} onClick={login}>
                  <ListItemIcon>
                    <LoginIcon />
                  </ListItemIcon>
                  <ListItemText primary="Signup / Login" />
                </ListItem>
                <Divider />
              </div>
            </Tooltip>
          }

          {!!loggedInUserUUID &&
            <Tooltip title={`View profile`}>
              <div>
                <DeepLink type='user' val={loggedInUser.uuid}>
                  <ListItem button className={classes.menuItem}>
                    <ListItemIcon>
                      <Avatar name={loggedInUser.displayName} src={loggedInUser.profileImageURL} size="40" round={true} color='#f44336' />
                    </ListItemIcon>
                    <ListItemText primary={loggedInUser.displayName} />
                  </ListItem>
                </DeepLink>
                <Divider />
              </div>
            </Tooltip>
          }

          <Tooltip title="My topics of interest">
            <div>
              <Link prefetch passHref href="/topics">
                <ListItem className={classes.menuItem} button>
                  <ListItemIcon>
                    <Badge badgeContent={<StarFilledIcon fontSize="small" color="secondary" />}>
                      <ChatIcon />
                    </Badge>
                  </ListItemIcon>
                  <ListItemText primary="My topics of interest" />
                </ListItem>
              </Link>
              <Divider />
            </div>
          </Tooltip>

          {loggedInUserUUID &&
            <Tooltip title="Logout">
              <div>
                <ListItem button className={classes.menuItem} onClick={logout}>
                  <ListItemIcon>
                    <LogoutIcon />
                  </ListItemIcon>
                  <ListItemText primary="Logout" />
                </ListItem>
                <Divider />
              </div>
            </Tooltip>
          }

          <Tooltip title="Terms of service">
            <div>
              <Link prefetch passHref href="/terms-of-service">
                <ListItem button className={classes.menuItem}>
                  <ListItemIcon>
                    <TermsIcon />
                  </ListItemIcon>
                  <ListItemText primary="Terms of service" />
                </ListItem>
              </Link>
              <Divider />
            </div>
          </Tooltip>

          <Tooltip title="Privacy policy">
            <div>
              <Link prefetch passHref href="/privacy-policy">
                <ListItem button className={classes.menuItem}>
                  <ListItemIcon>
                    <PrivacyIcon />
                  </ListItemIcon>
                  <ListItemText primary="Privacy policy" />
                </ListItem>
              </Link>
              <Divider />
            </div>
          </Tooltip>
        </List>
      </div >);
  }
}

const styles = theme => ({
  toolbar: theme.mixins.toolbar,
  toolbarBackground: {
    backgroundColor: theme.palette.primary.main
  },
  menuItem: {
    height: '71px'
  },
  chevronButton: {
    marginTop: '10px',
    marginLeft: '10px'
  },
  fullHeight: {
    height: '100vh',
    padding: 0
  }
});

const mapStateToProps = state => {
  const loggedInUser = userSelector(state, { uuid: state.loggedInUserUUID });
  return {
    loggedInUserUUID: state.loggedInUserUUID,
    loggedInUser
  };
};

const mapDispatchToProps = dispatch => {
  return {
    login: () => dispatch(showAuthModal()),
    logout: () => dispatch(logout())
  };
};

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  withStyles(styles, { withTheme: true })
)(MainNav);

// export default withStyles(styles, { withTheme: true })(MainNav);
