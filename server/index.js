/* eslint-disable no-console */
const logger = require('winston');
const app = require('./app');
const port = app.get('port');
const { nextApp } = require('./nextApp');
const vapid = require('./vapid');

nextApp.prepare().then(() => {
  const server = app.listen(port);

  process.on('unhandledRejection', (reason, p) =>
    logger.error('Unhandled Rejection at: Promise ', p, reason)
  );

  server.on('listening', async () => {
    await vapid.setVapidKeys(app);
    logger.info(
      'Application started on https://%s:%d',
      app.get('host'),
      port
    )
  });
});
