const routes = require('next-routes')();

routes
.add('identity-management', '/identity/:action/:token', 'identity-management')
.add('only-topics', '/topics', 'topics')
.add('topic-messages', '/topics/:topicUUID', 'topics') // same as topic-all-messages
.add('topic-all-messages', '/topics/:topicUUID/messages', 'topics')
.add('topic-message-thread', '/topics/:topicUUID/messages/:topicMessageUUID', 'topics')
.add('topics-messages-with-user-profile', '/topics/:topicUUID/users/:userUUID', 'topics')
.add('topic-with-user-profile', '/topics/users/:userUUID', 'topics')

  module.exports = routes;