const { handle, isFeathersService } = require('../nextApp');

module.exports = function (options = {}) {
  return function next(req, res, next) {
    if (isFeathersService(req.originalUrl)) return next();
    return handle(req, res);
  };
};
