const next = require('./next');
const sw = require('./sw');
// eslint-disable-next-line no-unused-vars
module.exports = function (app) {
  app.get('/service-worker.js', sw());
  app.get('*', next());
};
