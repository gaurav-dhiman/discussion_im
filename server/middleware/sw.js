const { nextApp } = require('../nextApp');
const { parse } = require('url');
const { join } = require('path');

module.exports = function (options = {}) {
  return function next(req, res, next) {
    const parsedUrl = parse(req.url, true);
    const { pathname } = parsedUrl;

    res.set("Cache-Control", "no-store, no-cache, must-revalidate, proxy-revalidate");
    res.set("Content-Type", "application/javascript");
    const filePath = join(__dirname, '../../client/.next', pathname);
    nextApp.serveStatic(req, res, filePath);
  };
};
