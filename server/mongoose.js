const mongoose = require('mongoose');

module.exports = function (app) {
  const mongoURL = (process.env.NODE_ENV == 'production') ? process.env.MONGO_URL : app.get('mongodb')
  mongoose.connect(mongoURL, { useCreateIndex: true, useNewUrlParser: true });
  mongoose.Promise = global.Promise;

  app.set('mongooseClient', mongoose);
};
