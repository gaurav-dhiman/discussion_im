const authentication = require('@feathersjs/authentication');
const jwt = require('@feathersjs/authentication-jwt');
const local = require('@feathersjs/authentication-local');
const oauth1 = require('@feathersjs/authentication-oauth1');
const oauth2 = require('@feathersjs/authentication-oauth2');
const GoogleStrategy = require('passport-google-oauth20');
const FacebookStrategy = require('passport-facebook');
const TwitterStrategy = require('passport-twitter');
const GitHubStrategy = require('passport-github');
const session = require('express-session');
const MongoStore = require('connect-mongo')(session);

const authHooks = require('./authentication.hooks');

module.exports = function (app) {
  const config = app.get('authentication');
  const mongooseConnection = app.get('mongooseClient').connection;
  // Setup in memory session
  app.use(session({
    store: new MongoStore({ mongooseConnection }),
    secret: config.secret,
    resave: true,
    saveUninitialized: false
  }));

  // Set up authentication with the secret
  app.configure(authentication(config));
  app.configure(jwt());
  app.configure(local());

  app.configure(oauth2(Object.assign({
    name: 'google',
    Strategy: GoogleStrategy
  }, config.google)));

  app.configure(oauth2(Object.assign({
    name: 'facebook',
    Strategy: FacebookStrategy
  }, config.facebook)));

  app.configure(oauth1(Object.assign({
    name: 'twitter',
    Strategy: TwitterStrategy
  }, config.twitter)));

  app.configure(oauth2(Object.assign({
    name: 'github',
    Strategy: GitHubStrategy
  }, config.github)));

  // The `authentication` service is used to create a JWT.
  // The before `create` hook registers strategies that can be used
  // to create a new valid JWT (e.g. local or oauth2)
  app.service('authentication').hooks({
    before: {
      // create: [authHooks.dishonorJWTForLocalStrategy],
      create: [
        authHooks.diregardJWTForLocalStrategy(),
        authentication.hooks.authenticate(config.strategies)
      ],
      remove: [
        authentication.hooks.authenticate('jwt')
      ]
    },
    after: {
      create: [authHooks.avoidJWTRegeneration()],
      remove: []
    }
  });
};
