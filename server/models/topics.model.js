// topics-model.js - A mongoose model
//
// See http://mongoosejs.com/docs/models.html
// for more of what you can do here.
module.exports = function (app) {
  const mongooseClient = app.get('mongooseClient');
  const { Schema } = mongooseClient;
  const topics = new Schema(
    {
      uuid: { type: String, required: true, uniqe: true },
      creatorUUID: { type: String, required: true },
      title: { type: String, required: true },
      description: { type: String, default: '' },
      totalMessageCount: { type: Number, default: 0 },
      lastMessageTimestamp: { type: Date },
      postCountByUsersUUIDs: { type: Object, default: {} },
      staredByUsersUUIDs: { type: Object, default: {} },
      webPushSubscriptionIds: { type: Object, default: {} }
    },
    { timestamps: true }
  );
  topics.index({ title: 'text', description: 'text' });
  return mongooseClient.model('topics', topics);
};
