// WebPushSubscriptions-model.js - A mongoose model
//
// See http://mongoosejs.com/docs/models.html
// for more of what you can do here.
module.exports = function (app) {
  const mongooseClient = app.get('mongooseClient');
  const { Schema } = mongooseClient;
  const webPush = new Schema(
    {
      uuid: { type: String, required: true, uniqe: true },
      userUUID: { type: String, required: false },
      subscriptionId: { type: String, required: true },
      subscription: { type: Object, required: true },
      subscribeTo: {
        type: { type: String, required: true },
        uuid: { type: String, required: true }
      }
    },
    { timestamps: true }
  );
  return mongooseClient.model('WebPushSubscriptions', webPush);
};
