// users-model.js - A mongoose model
//
// See http://mongoosejs.com/docs/models.html
// for more of what you can do here.
module.exports = function (app) {
  const mongooseClient = app.get('mongooseClient');
  const users = new mongooseClient.Schema(
    {
      uuid: { type: String, required: true, uniqe: true },
      firstName: { type: String },
      lastName: { type: String },
      displayName: { type: String },
      handle: { type: String },
      status: { type: String },
      profileImagePublicId: { type: String },
      profileImageURL: { type: String },
      email: { type: String, unique: true },
      phone: { type: String },
      password: { type: String },
      googleId: { type: String },
      facebookId: { type: String },
      twitterId: { type: String },
      githubId: { type: String },
      isVerified: { type: Boolean },
      verifyToken: { type: String },
      verifyExpires: { type: Date },
      verifyChanges: { type: Object },
      resetToken: { type: String },
      resetExpires: { type: Date },
      staredTopicsUUIDs: { type: Object, default: {} },
      likedTopicMessagesUUIDs: { type: Object, default: {} },
      pinnedTopicMessagesUUIDs: { type: Object, default: {} },
      subscribedTo: { type: Object, default: {} }
    },
    { timestamps: true }
  );
  users.index({ email: 'text', firstName: 'text', lastName: 'text', displayName: 'text' });

  return mongooseClient.model('users', users);
};
