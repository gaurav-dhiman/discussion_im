// topic-messages-model.js - A mongoose model
//
// See http://mongoosejs.com/docs/models.html
// for more of what you can do here.
module.exports = function (app) {
  const mongooseClient = app.get('mongooseClient');
  const { Schema } = mongooseClient;
  const topicMessages = new Schema(
    {
      uuid: { type: String, required: true, uniqe: true },
      senderUUID: { type: String, required: true },
      text: { type: String, required: true },
      topicUUID: { type: String, required: false },
      parentTopicMessageUUID: { type: String, required: false },
      totalRepliesCount: { type: Number, default: 0, required: false },
      likedByUsersUUID: { type: Object, required: false },
      pinnedByUsersUUID: { type: Object, required: false },
    },
    { timestamps: true }
  );
  topicMessages.index({ text: 'text' });
  return mongooseClient.model('topicMessages', topicMessages);
};
