const webpush = require('web-push');
const fs = require('fs')
const service = require('./services/definitions');

module.exports = {
  setVapidKeys: async (app) => {
    if (!app || (typeof (app.get) != 'function') || (typeof (app.set) != 'function')) return;

    // get vapidKeys from DB config
    let config = await app.service(service.config).find();
    config = (config && config.data) || [];
    config = config[0];
    if (config && config.vapidKeys && config.vapidKeys.publicKey && config.vapidKeys.privateKey) {
      app.set('vapidKeys', config.vapidKeys);
    } else if (!app.get('vapidKeys')) {
      const vapidKeys = webpush.generateVAPIDKeys();
      // store valid keys in DB, to make web notification work across deployments.
      if (config) {
        await app.service(service.config).patch(config._id, { vapidKeys });
      } else {
        await app.service(service.config).create({ vapidKeys });
      }
      app.set('vapidKeys', vapidKeys);
    }
  }
}
