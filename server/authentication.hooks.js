const diregardJWTForLocalStrategy = () => (ctx) => {
  // Workaround: For local stretegy, do not honor JWT accessToken and force re-authentication.
  if (ctx.data && ctx.data.strategy === 'local') {
    delete ctx.params.headers.Authorization;
    delete ctx.params.authenticated;
  }
  return ctx;
}

const avoidJWTRegeneration = () => (ctx) => {
  // Avoid regenerating accessToken if already exiting one is still valid
  if (ctx.data && ctx.data.strategy === 'jwt' && ctx.result.accessToken !== ctx.data.accessToken) {
    ctx.result.accessToken = ctx.data.accessToken;
  }
  return ctx;
}

module.exports = {
  diregardJWTForLocalStrategy,
  avoidJWTRegeneration
};