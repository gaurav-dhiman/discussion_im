const topics = require('./topics/topics.service.js');
const topicMessages = require('./topic-messages/topic-messages.service.js');
const users = require('./users/users.service.js');
const emails = require('./emails/emails.service.js');
const authManagement = require('./auth-management/auth-management.service.js');
const webPushSubscribe = require('./web-push-subscribe/web-push-subscribe.service.js');
const webPushNotify = require('./web-push-notify/web-push-notify.service.js');
const profileImage = require('./profile-image/profile-image.service.js');
const vapidKeys = require('./vapid-keys/vapid-keys.service.js');
const config = require('./config/config.service.js');
// eslint-disable-next-line no-unused-vars
module.exports = function (app) {
  app.configure(topics);
  app.configure(topicMessages);
  app.configure(users);
  app.configure(emails);
  app.configure(authManagement);
  app.configure(webPushSubscribe);
  app.configure(webPushNotify);
  app.configure(profileImage);
  app.configure(vapidKeys);
  app.configure(config);
};
