// Initializes the `web-push` service on path `/api/v1/web-push`
const createService = require('feathers-mongoose');
const createModel = require('../../models/web-push-subscribe.model');
const hooks = require('./web-push-subscribe.hooks');
const registerEvents = require('./web-push-subscribe.events');
const services = require('../definitions');

module.exports = function (app) {
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    name: 'web-push-subscribe',
    Model,
    paginate
  };

  // Initialize our service with any options it requires
  app.use(services.webPushSubscribe, createService(options));

  // Get our initialized service so that we can register hooks and filters
  const service = app.service(services.webPushSubscribe);

  service.hooks(hooks);
  registerEvents(service);
};
