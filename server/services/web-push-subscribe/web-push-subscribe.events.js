const { addSubscriptionToTopic, addSubscriptionToUser, removeSubscriptionFromTopic, removeSubscriptionFromUser } = require('./web-push-subscribe.event.methods');

const events = [
  {
    name: 'created',
    handler: (subscription, ctx) => {
      addSubscriptionToTopic(subscription, ctx);
      addSubscriptionToUser(subscription, ctx);
      console.log('web push subscription created: ', subscription);
    }
  },
  {
    name: 'updated',
    handler: (subscription, ctx) => {
      console.log('web push subscription updated: ', subscription);
    }
  },
  {
    name: 'patched',
    handler: (subscription, ctx) => {
      console.log('web push subscription patched: ', subscription);
    }
  },
  {
    name: 'removed',
    handler: (subscription, ctx) => {
      removeSubscriptionFromTopic(subscription, ctx);
      removeSubscriptionFromUser(subscription, ctx);
      console.log('web push subscription removed: ', subscription);
    }
  }
];

function registerEvents(service) {
  events.forEach(event => {
    service.on(event.name, event.handler);
  });
}

module.exports = registerEvents;
