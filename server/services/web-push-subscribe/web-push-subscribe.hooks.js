const { protect } = require('@feathersjs/authentication-local').hooks;
const { fastJoin } = require('feathers-hooks-common');
const { mapCreateToUpsert, blockMethods } = require('../common.hook.methods');
const { addUUID, getWebPushSubscribeResolvers } = require('./web-push-subscribe.hook.methods');

module.exports = {
  before: {
    all: [
      blockMethods(['update']), // 'patch'should not be blocked, as we map create to patch for upsert.
    ],
    find: [],
    get: [],
    create: [
      addUUID()
      // mapCreateToUpsert(ctx => { uuid: ctx.data.uuid })
    ],
    update: [],
    patch: [],
    remove: []
  },

  after: {
    all: [
      protect('_id'),
      fastJoin(getWebPushSubscribeResolvers())
    ],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
