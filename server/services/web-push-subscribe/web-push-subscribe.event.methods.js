const services = require('../definitions');

async function addSubscriptionToTopic(subscriptionObj, ctx) {
  const { subscribeTo = {}, subscriptionId } = subscriptionObj;

  if (subscribeTo.type != 'topic' || !subscribeTo.uuid) return ctx;

  let topic = await ctx.app.service(services.topics).find({ query: { uuid: subscribeTo.uuid } });
  topic = topic.data || [];
  topic = topic[0];

  if (!topic) return ctx;

  topic.webPushSubscriptionIds = topic.webPushSubscriptionIds || {};
  topic.webPushSubscriptionIds[subscriptionId] = true;
  await ctx.app.service(services.topics).patch(topic._id, { webPushSubscriptionIds: topic.webPushSubscriptionIds });

  return ctx;
}

async function addSubscriptionToUser(subscriptionObj, ctx) {
  const { userUUID = '', subscribeTo = {} } = subscriptionObj;

  if (!userUUID) return ctx;
  if (!subscribeTo.type || !subscribeTo.uuid) return ctx;

  let user = await ctx.app.service(services.users).find({ query: { uuid: userUUID } });
  user = user.data || [];
  user = user[0];

  if (!user) return ctx;

  user.subscribedTo = user.subscribedTo || {};
  user.subscribedTo[subscribeTo.type] = user.subscribedTo[subscribeTo.type] || {};
  user.subscribedTo[subscribeTo.type][subscribeTo.uuid] = true;
  await ctx.app.service(services.users).patch(user._id, { subscribedTo: user.subscribedTo });

  return ctx;
}

async function removeSubscriptionFromTopic(subscriptionObj, ctx) {
  const { subscribeTo = {}, subscriptionId = '' } = subscriptionObj || {};

  if (subscribeTo.type != 'topic' || !subscribeTo.uuid) return ctx;

  let topic = await ctx.app.service(services.topics).find({ query: { uuid: subscribeTo.uuid } });
  topic = topic.data || [];
  topic = topic[0];

  if (!topic) return ctx;

  topic.webPushSubscriptionIds = topic.webPushSubscriptionIds || {};
  // we wont delete topic.webPushSubscriptionIds[subscriptionId] - as 'false' is an indication that unsubscription happend after subscription
  // where as if topic.webPushSubscriptionIds[subscriptionId] is undefined that means no subscription has ever happend for given subscription obj (browser)
  if (topic.webPushSubscriptionIds[subscriptionId]) {
    topic.webPushSubscriptionIds[subscriptionId] = undefined;
    await ctx.app.service(services.topics).patch(topic._id, { webPushSubscriptionIds: topic.webPushSubscriptionIds });
  }

  return ctx;
}

async function removeSubscriptionFromUser(subscriptionObj, ctx) {
  const { userUUID = '', subscribeTo = {} } = subscriptionObj;

  if (!userUUID) return ctx;
  if (!subscribeTo.type || !subscribeTo.uuid) return ctx;

  let user = await ctx.app.service(services.users).find({ query: { uuid: userUUID } });
  user = user.data || [];
  user = user[0];

  if (!user) return ctx;

  user.subscribedTo = user.subscribedTo || {};
  user.subscribedTo[subscribeTo.type] = user.subscribedTo[subscribeTo.type] || {};
  user.subscribedTo[subscribeTo.type][subscribeTo.uuid] = undefined;
  await ctx.app.service(services.users).patch(user._id, { subscribedTo: user.subscribedTo });

  return ctx;
}

module.exports = {
  addSubscriptionToTopic,
  addSubscriptionToUser,
  removeSubscriptionFromTopic,
  removeSubscriptionFromUser
}