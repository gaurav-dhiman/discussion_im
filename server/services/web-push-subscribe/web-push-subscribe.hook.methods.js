const crypto = require('crypto');
const services = require('../definitions');

const addUUID = () => async (ctx) => {
  const { subscription = {}, subscribeTo = {}, userUUID = '' } = ctx.data;

  if (!subscription.endpoint || !subscribeTo.uuid) return ctx;

  // generate unique ID bsaed on subscription (device browser), entity which has been subscribed and user who subscribed.
  const idString = subscription.endpoint + subscribeTo.uuid + userUUID;
  ctx.data.uuid = crypto.createHash('sha256').update(idString).digest('hex');
  ctx.data.subscriptionId = crypto.createHash('sha256').update(subscription.endpoint).digest('hex');
  return ctx;
}

const getWebPushSubscribeResolvers = () => ctx => {
  ctx.params = ctx.params || {};
  const { provider } = ctx.params;
  let { resolvers, } = ctx.params;
  const userSrvc = ctx.app.service(services.users);
  resolvers = resolvers || {};

  return {
    joins: {
      user: $select => async subscription => subscription.user = resolvers.user && (await userSrvc.find({
        query: { uuid: subscription.userUUID, $select: $select },
        paginate: false,
        provider
      }))[0] || undefined
    }
  };
}

module.exports = {
  addUUID,
  getWebPushSubscribeResolvers,
};