// Initializes the `config` service on path `/api/v1/config`
const createService = require('feathers-mongoose');
const createModel = require('../../models/config.model');
const hooks = require('./config.hooks');
const services = require('../definitions');

module.exports = function (app) {
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    Model,
    paginate
  };

  // Initialize our service with any options it requires
  app.use(services.config, createService(options));

  // Get our initialized service so that we can register hooks
  const service = app.service(services.config);

  service.hooks(hooks);
};
