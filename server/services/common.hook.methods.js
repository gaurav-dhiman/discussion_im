const uuid = require('uuid/v5');
var randomString = require("randomstring");
const errors = require('@feathersjs/errors');

const processLatestRecordsQueryParam = () => async (ctx) => {
  if (!ctx.params.query) return ctx;
  if (!ctx.params.query.latestRecords) return ctx;

  const limit = ctx.params.query['$limit'] || ctx.service.paginate.default;
  ctx.params.latestRecords = ctx.params.query.latestRecords;
  delete ctx.params.query.latestRecords;
  // query with $limit:0 to get fast records count without fetching actual records
  let params = { query: { ...ctx.params.query, $limit: 0 } };
  const allRecords = await ctx.service.find(params);
  const skip = allRecords.total - limit;
  if (skip >= 0) ctx.params.query['$skip'] = skip;
  else ctx.params.query['$skip'] = 0;

  return ctx;
}

/**
 * Client can send fetchPlan query param to do join and fetch deep linkage to other collections
 * 'fetchPlan' query param will be string that have parts in below formats / examples:
 * 1. message:title;descriptions --> this will get'message' field and each message will include 'title' and 'description' fields.
 * 2. sender.address --> 
  col1:subCol1;subCol2,col2.subcol3.subcol4,col3
  E.g: messages.sender,creator,messages.replies
 */
const parseFetchPlanQueryParam = () => async (ctx) => {
  if (!ctx.params.query) return ctx;
  for (let key in ctx.params.query) {
    switch (ctx.params.query[key]) {
      case null:
      case undefined:
      case '':
        delete ctx.params.query[key];
    }
  }

  if (ctx.params.query.fetchPlan === '') delete ctx.params.query.fetchPlan;
  if (!ctx.params.query.fetchPlan) return ctx;
  ctx.params.query.fetchPlan = ctx.params.query.fetchPlan.trim();

  let fp = ctx.params.query.fetchPlan;
  delete ctx.params.query.fetchPlan;
  let resolvers = {};
  let subFetchPlan = [];
  fp = fp.trim().split(',');
  fp.forEach((plan) => {
    plan = plan.trim().split('.');
    const field = plan.shift();
    if (field) resolvers[field] = true;
    plan = plan.join('.');
    if (plan) subFetchPlan.push(plan);
  })
  ctx.params.resolvers = resolvers
  ctx.params.fetchPlan = subFetchPlan.length && subFetchPlan.join(',') || undefined;

  return ctx;
}

const addUUID = () => async (ctx) => {
  if (!(ctx.data.uuid))
    // TODO: Consume the below random key from env variable
    ctx.data.uuid = await uuid(randomString.generate(), '1b671a64-40d5-491e-99b0-da01ff1f3341');
  return ctx;
}


const mapToLoggedInUserUUID = (field) => async ctx => {
  const { params = {}, data = {} } = ctx;
  const { user = {} } = params;

  if(!field) return ctx;
  
  data[field] = user.uuid;
  return ctx;
}

const mapCreateToUpsert = (upsertQuery) => async (ctx) => {
  const { service, data, params } = ctx;

  upsertQuery = params.upsertQuery || upsertQuery
  if (typeof upsertQuery !== 'function') {
    throw new errors.BadRequest('No `upsertQuery` argument passed to the mapCreateToUpsert hook must be a function. Please set params.upsertQuery in the hook context to dynamically declare the function.');
  }

  params.mongoose = { ...params.mongoose, upsert: true };
  params.query = upsertQuery(ctx);
  ctx.result = await service.patch(null, data, params);
  return ctx;
}

const blockMethods = (arr = []) => (ctx) => {
  if (!Array.isArray(arr)) throw new errors.BadRequest('An array of method names should be passed as argument to `blockMethods` hook.');
  if (arr.includes(ctx.method)) throw new errors.BadRequest('Method `' + ctx.method + '` is not allowed for this service.');
  return ctx;
}

module.exports = {
  processLatestRecordsQueryParam,
  parseFetchPlanQueryParam,
  addUUID,
  mapToLoggedInUserUUID,
  mapCreateToUpsert,
  blockMethods
};
