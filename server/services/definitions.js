// Service path are used as service definition across client / server.
module.exports = {
  oAuthGoogle: '/auth/google',
  oAuthFacebook: '/auth/facebook',
  authManagement: 'api/v1/auth-management',
  users: 'api/v1/users',
  topics: 'api/v1/topics',
  topicMessages: 'api/v1/topic-messages',
  emails: 'api/v1/emails',
  webPushSubscribe: '/api/v1/web-push/subscribe',
  webPushNotify: '/api/v1/web-push/notify/:entity/:val',
  profileImage: '/api/v1/profile-image',
  vapidKeys: '/api/v1/vapid-keys',
  config: '/api/v1/config'
};