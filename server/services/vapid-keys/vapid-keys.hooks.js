const { blockMethods } = require('../common.hook.methods');
const { getPublicVapidKey } = require('./vapid-keys.hook.methods');

module.exports = {
  before: {
    all: [
      blockMethods([
        'get',
        'create',
        'update',
        'patch',
        'remove',
      ])
    ],
    find: [
      getPublicVapidKey()
    ],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
