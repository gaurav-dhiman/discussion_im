const getPublicVapidKey = () => async (ctx) => {
  const { publicKey = '' } = ctx.app.get('vapidKeys') || {};
  ctx.result = publicKey;

  return ctx;
}


module.exports = {
  getPublicVapidKey,
};
