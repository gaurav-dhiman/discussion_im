// Initializes the `vapid-keys` service on path `/api/v1/vapid-keys`
// const createService = require('feathers-mongoose');
// const createModel = require('../../models/vapid-keys.model');
const createService = require('./vapid-keys.class');
const services = require('../definitions');
const hooks = require('./vapid-keys.hooks');

module.exports = function (app) {
  // const Model = createModel(app);
  // const paginate = app.get('paginate');

  // const options = {
  //   Model,
  //   paginate
  // };

  // Initialize our service with any options it requires
  // app.use('/api/v1/vapid-keys', createService(options));

  const paginate = app.get('paginate');

  const options = {
    paginate
  };

  // Initialize our service with any options it requires
  app.use(services.vapidKeys, createService(options));

  // Get our initialized service so that we can register hooks
  const service = app.service(services.vapidKeys);

  service.hooks(hooks);
};
