const services = require('../definitions');
const { authenticate } = require('@feathersjs/authentication').hooks;
const {
  mapOAuthGoogleUser,
  mapOAuthFacebookUser,
  mapOAuthbTwitterUser,
  mapOAuthGitHubUser,
  doesUserExist,
  fillDisplayNameField,
  sendNotificationToVerifyUser,
  setUserIdentifier,
} = require('./users.hook.methods');
const verifyHooks = require('feathers-authentication-management').hooks;

const {
  processLatestRecordsQueryParam,
  parseFetchPlanQueryParam,
  addUUID
} = require('../common.hook.methods');

const {
  hashPassword,
  protect
} = require('@feathersjs/authentication-local').hooks;

module.exports = {
  before: {
    all: [],
    find: [
      processLatestRecordsQueryParam(),
      parseFetchPlanQueryParam()
    ],
    get: [],
    create: [
      mapOAuthGoogleUser(),
      mapOAuthFacebookUser(),
      mapOAuthbTwitterUser(),
      mapOAuthGitHubUser(),
      doesUserExist(),
      hashPassword(),
      addUUID(),
      fillDisplayNameField(),
      verifyHooks.addVerification(services.authManagement)
    ],
    update: [
      authenticate('jwt'),
      mapOAuthGoogleUser(),
      mapOAuthFacebookUser(),
      mapOAuthbTwitterUser(),
      mapOAuthGitHubUser(),
      setUserIdentifier(),
      hashPassword()
    ],
    patch: [
      authenticate('jwt'),
      mapOAuthGoogleUser(),
      mapOAuthFacebookUser(),
      mapOAuthbTwitterUser(),
      mapOAuthGitHubUser(),
      setUserIdentifier()
    ],
    remove: [authenticate('jwt')]
  },

  after: {
    all: [
      // Make sure the password and _id field is never sent to the client
      // Always must be the last hook
      protect('_id', 'password')
    ],
    find: [],
    get: [],
    create: [
      sendNotificationToVerifyUser(),
      verifyHooks.removeVerification()
    ],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
