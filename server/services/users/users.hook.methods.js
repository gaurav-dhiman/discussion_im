const errors = require('@feathersjs/errors');
const accountService = require('../auth-management/notifier');
const emailTypes = require('../auth-management/notification-email-types');

const mapOAuthGoogleUser = () => async (ctx) => {
  const { oauth = {} } = ctx.params;
  if (oauth.provider !== 'google') return ctx;

  const { google = {} } = ctx.data;
  const { profile = {} } = google;
  const { name = {}, emails = [], photos = [] } = profile;
  const email = emails[0] || {};
  const photo = photos[0] || {};

  ctx.data = ctx.data || {};
  ctx.data.firstName = name.givenName;
  ctx.data.lastName = name.familyName;
  ctx.data.displayName = (ctx.data.firstName + ' ' + ctx.data.lastName).trim();
  ctx.data.email = email.value;
  ctx.data.profileImageURL = (photo.value || '').split('?')[0];
  return ctx;
}

const mapOAuthFacebookUser = () => async (ctx) => {
  const { oauth = {} } = ctx.params;
  if (oauth.provider !== 'facebook') return ctx;

  const { facebook = {} } = ctx.data;
  const { profile = {} } = facebook;
  const { name = {}, emails = [], photos = [] } = profile;
  const email = emails[0] || {};
  const photo = photos[0] || {};

  ctx.data = ctx.data || {};
  ctx.data.firstName = name.givenName;
  ctx.data.lastName = name.familyName;
  ctx.data.displayName = (ctx.data.firstName + ' ' + ctx.data.lastName).trim();
  ctx.data.email = email.value;
  ctx.data.profileImageURL = photo.value || '';
  return ctx;
}

const mapOAuthbTwitterUser = () => async (ctx) => {
  const { oauth = {} } = ctx.params;
  if (oauth.provider !== 'twitter') return ctx;

  const { twitter = {} } = ctx.data;
  const { profile = {} } = twitter;
  const { name = {}, emails = [], photos = [] } = profile;
  const email = emails[0] || {};
  const photo = photos[0] || {};

  ctx.data = ctx.data || {};
  ctx.data.firstName = name.givenName;
  ctx.data.lastName = name.familyName;
  ctx.data.displayName = (ctx.data.firstName + ' ' + ctx.data.lastName).trim();
  ctx.data.email = email.value;
  ctx.data.profileImageURL = photo.value || '';
  return ctx;
}

const mapOAuthGitHubUser = () => async (ctx) => {
  const { oauth = {} } = ctx.params;
  if (oauth.provider !== 'github') return ctx;

  const { github = {} } = ctx.data;
  const { profile = {} } = github;
  const { displayName = '', emails = [], photos = [] } = profile;
  const email = emails[0] || {};
  const photo = photos[0] || {};

  const name = displayName.split(' ') || [];
  ctx.data = ctx.data || {};
  ctx.data.firstName = (name[0] || '').trim();
  ctx.data.lastName = (name[1] || '').trim();
  ctx.data.displayName = displayName.trim();
  ctx.data.email = email.value;
  ctx.data.profileImageURL = photo.value || '';
  return ctx;
}

const doesUserExist = () => async (ctx) => {
  let user, errMessage = 'Account with these credentials already exists. Try login or resetting password.';
  let userId =
    (ctx.data && ctx.data.email) ||
    (ctx.params && ctx.params.query && (ctx.params.query.email || ctx.id));
  if (!userId) {
    throw new errors.NotAcceptable('No user identifier found.', {
      errors: {
        userId:
          'No user identifier passed. Please check request or report it to admin.'
      }
    });
  }

  // check if id is email or _id
  if (userId.indexOf('@') > 0) {
    user = await ctx.service.find({
      query: { email: userId }
    });
    user = user.data[0];
  } else {
    user = await ctx.service.get(userId);
  }

  if (user) {
    if (!ctx.data.password) {  // User tried login using Social login.
      ctx.result = user;
      return ctx;
    }
    // User tried to signup using email, but we already have user with that email, so prompt user to login existing social login or credentials.
    if (user.googleId) errMessage = 'Account with these credentials already exists. Try logging with Google account.';
    if (user.facebookId) errMessage = 'Account with these credentials already exist. Try logging with Facebook account.';
    if (user.twitterId) errMessage = 'Account with these credentials already exist. Try logging with Twitter account.';
    if (user.githubId) errMessage = 'Account with these credentials already exist. Try logging with GitHub account.';
    throw new errors.NotAcceptable(errMessage, {
      errSubType: 'user-already-exists'
    });
  }
  return ctx;
}

const fillDisplayNameField = () => async (ctx) => {
  ctx.data.displayName = (ctx.data.firstName + ' ' + ctx.data.lastName).trim();
  return ctx;
}

const sendNotificationToVerifyUser = () => async (ctx) => {
  const user = ctx.result;
  // If user got created using social login, we need not to verify user's email address, as its self-verified.
  // if (user.googleId || user.facebookId || user.twitterId || user.githubId) return ctx;

  await accountService(ctx.app).notifier(emailTypes.SEND_VERIFY_ACCOUNT, user);
  return ctx;
}

const setUserIdentifier = () => ctx => {
  if (!ctx.id && !ctx.params.query) {
    ctx.id = ctx.params.user._id;
  }
  return ctx;
}

module.exports = {
  mapOAuthGoogleUser,
  mapOAuthFacebookUser,
  mapOAuthbTwitterUser,
  mapOAuthGitHubUser,
  doesUserExist,
  fillDisplayNameField,
  sendNotificationToVerifyUser,
  setUserIdentifier
};
