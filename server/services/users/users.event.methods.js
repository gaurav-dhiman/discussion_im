const path = require('path');
const pug = require('pug');
const services = require('../definitions');
const isProd = process.env.NODE_ENV === 'production';

async function sendWebPushToAdmins(user, ctx) {
  if (!user) return;

  const webPushData = {
    payload: {
      title: `New user signed-up`,
      body: 'Name:' + user.displayName + '\nEmail: ' + user.email,
      url: `${ctx.app.get('protocol')}://${ctx.app.get('host')}/topics/users/${user.uuid}`
    }
  };

  const params = {
    route: {
      entity: 'users',
      exclude: [user.uuid],
      val: 'admins'
    }
  }

  ctx.app.service(services.webPushNotify).create(webPushData, params);
}

async function sendEmailToAdmins(user, ctx) {
  let email;
  let emailAccountTemplatesPath = path.join(ctx.app.get('serverDir'), 'email-templates', 'user');
  let templatePath = path.join(emailAccountTemplatesPath, 'new-user-created.pug');
  let compiledHTML;

  let admins = await ctx.app.service(services.users).find({ query: { email: ctx.app.get('appAdminEmail') } });
  admins = admins.data || [];

  let port = ((ctx.app.get('httpsPort') === '443') || (ctx.app.get('httpsPort') === undefined) || isProd) ? '' : ':' + ctx.app.get('httpsPort')
  let host = (ctx.app.get('host') === undefined) ? 'localhost' : ctx.app.get('host')
  let protocol = (ctx.app.get('protocol') === undefined) ? 'http' : ctx.app.get('protocol')
  let logoLink = `${protocol}://${host}${port}/logo.png`

  admins.forEach(async (admin) => {
    compiledHTML = pug.compileFile(templatePath)({
      logo: logoLink,
      websiteName: ctx.app.get('websiteName'),
      name: admin.displayName.trim(),  // Admin name
      userDisplayName: user.displayName.trim(),   // Topic creator's name
      userProfileLink: `${ctx.app.get('protocol')}://${ctx.app.get('host')}/topics/users/${user.uuid}`,
      userEmail: user.email
    });

    email = {
      from: ctx.app.get('doNotReplyEmail') || process.env.GMAIL,
      to: admin.email,
      subject: `${ctx.app.get('websiteName')}: New user has signed-up.`,
      html: compiledHTML
    };

    try {
      // Send email.
      await ctx.app.service(services.emails).create(email);
    } catch (err) {
      console.log('Error sending email notification about user sign-up. Error: ', err)
    }
  });
}

module.exports = {
  sendWebPushToAdmins,
  sendEmailToAdmins
}