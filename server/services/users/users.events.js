const { sendWebPushToAdmins, sendEmailToAdmins } = require('./users.event.methods');

const events = [
  {
    name: 'created',
    handler: (user, ctx) => {
      sendWebPushToAdmins(user, ctx);
      sendEmailToAdmins(user, ctx);
      console.log('user created: ', user);
    }
  },
  {
    name: 'updated',
    handler: (user, ctx) => {
      console.log('user updated: ', user);
    }
  },
  {
    name: 'patched',
    handler: (user, ctx) => {
      console.log('user patched: ', user);
    }
  },
  {
    name: 'removed',
    handler: (user, ctx) => {
      console.log('user removed: ', user);
    }
  }
];

function registerEvents(service) {
  events.forEach(event => {
    service.on(event.name, event.handler);
  });
}

module.exports = registerEvents;
