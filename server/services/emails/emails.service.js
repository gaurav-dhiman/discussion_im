// Initializes the `emails` service on path `/api/v1/emails`
const hooks = require('./emails.hooks');
const services = require('../definitions');
const Mailer = require('feathers-mailer');
const mg = require('nodemailer-mailgun-transport');
const isProd = process.env.NODE_ENV === 'production';
let transporter;

module.exports = function (app) {
  if (isProd) {
    transporter = mg({
      auth: {
        api_key: process.env.MAILGUN_API_KEY || '',
        domain: app.get('emailServer').domain
      }
    });
  } else {
    transporter = {
      host: 'smtp.ethereal.email',
      port: 587,
      auth: {
        user: process.env.SMTP_USER,
        pass: process.env.SMTP_PASS
      }
    }
  }

  // Initialize our service with any options it requires
  app.use(services.emails, Mailer(transporter));

  // Get our initialized service so that we can register hooks
  const service = app.service(services.emails);

  service.hooks(hooks);
};
