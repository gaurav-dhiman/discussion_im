const errors = require('@feathersjs/errors');
const { join } = require('path');
const { unlink } = require('fs');
const cloudinary = require('cloudinary');
const srvc = require('../definitions');

const uploadImageToCloudinary = () => async (ctx) => {
  let { user } = ctx.params;
  const imgPath = join(__dirname, '../../../uploads', ctx.result.id);
  try {
    if (user.profileImagePublicId && user.profileImageURL.includes('cloudinary.com'))
      await cloudinary.v2.uploader.destroy(user.profileImagePublicId);

    const cloudinaryResponse = await cloudinary.v2.uploader.upload(imgPath);
    await ctx.app.service(srvc.users).patch(ctx.params.user._id, {
      profileImageURL: cloudinaryResponse.secure_url,
      profileImagePublicId: cloudinaryResponse.public_id
    });
    user = await ctx.app.service(srvc.users).get(ctx.params.user._id);
    ctx.result = user; //{ ...user, profileImageURL: cloudinaryResponse.secure_url };
    unlink(imgPath);
  } catch (error) {
    throw new errors.BadRequest('Error occured while uploading profile image to Cloudinary.' + JSON.stringify(error));
  }
  return ctx;
}

module.exports = {
  uploadImageToCloudinary
}