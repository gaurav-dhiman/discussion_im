// Initializes the `profile-image` service on path `/api/v1/profile-image`
const hooks = require('./profile-image.hooks');
const services = require('../definitions');
const blobService = require('feathers-blob');
const fs = require('fs-blob-store');
const blobStorage = fs('./uploads');

module.exports = function (app) {

  const paginate = app.get('paginate');

  const options = {
    Model: blobStorage,
    paginate
  };

  // Initialize our service with any options it requires
  app.use(services.profileImage, blobService(options));

  // Get our initialized service so that we can register hooks
  const service = app.service('api/v1/profile-image');

  service.hooks(hooks);
};
