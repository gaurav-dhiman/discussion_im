const {
  processLatestRecordsQueryParam,
  parseFetchPlanQueryParam,
  addUUID,
  mapToLoggedInUserUUID
} = require('../common.hook.methods');
const {
  getTopicResolvers,
  beforeStarSubscribeTopic,
  afterStarSubscribeTopic
} = require('./topics.hook.methods');
const { authenticate } = require('@feathersjs/authentication').hooks;
const { protect } = require('@feathersjs/authentication-local').hooks;
const { fastJoin } = require('feathers-hooks-common');

module.exports = {
  before: {
    all: [],
    find: [
      processLatestRecordsQueryParam(),
      parseFetchPlanQueryParam()
    ],
    get: [],
    create: [
      authenticate('jwt'),
      mapToLoggedInUserUUID('creatorUUID'),
      addUUID()
    ],
    update: [authenticate('jwt')],
    patch: [
      authenticate('jwt'),
      beforeStarSubscribeTopic()
    ],
    remove: [authenticate('jwt')]
  },

  after: {
    all: [
      protect('_id'),
      fastJoin(getTopicResolvers())
    ],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [
      afterStarSubscribeTopic()
    ],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
