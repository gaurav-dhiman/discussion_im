const { sendWebPushToAdmins, sendEmailToAdmins } = require('./topics.event.methods');

const events = [
  {
    name: 'created',
    handler: (topic, ctx) => {
      console.log('topic created: ', topic);
      sendWebPushToAdmins(topic, ctx);
      sendEmailToAdmins(topic, ctx);
    },
    publisher: (topic, ctx) => {
      return ctx.app.channel('anonymous', 'authenticated');
    }
  },
  {
    name: 'updated',
    handler: (topic, ctx) => {
      console.log('topic updated: ', topic);
    },
    publisher: (topic, ctx) => {
      return ctx.app.channel('anonymous', 'authenticated');
    }
  },
  {
    name: 'patched',
    handler: (topic, ctx) => {
      console.log('topic patched: ', topic);
    },
    publisher: (topic, ctx) => {
      return ctx.app.channel('anonymous', 'authenticated');
    }
  },
  {
    name: 'removed',
    handler: (topic, ctx) => {
      console.log('topic removed: ', topic);
    },
    publisher: (topic, ctx) => {
      return ctx.app.channel('anonymous', 'authenticated');
    }
  }
];

function registerEvents(service) {
  events.forEach(event => {
    service.on(event.name, event.handler);  // Use event.handler for logging etc.
    service.publish(event.name, event.publisher); // Use event.publisher() for publishing to channels for real-time socket communication.
  });
}

module.exports = registerEvents;
