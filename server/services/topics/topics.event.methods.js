const path = require('path');
const pug = require('pug');
const services = require('../definitions');
const isProd = process.env.NODE_ENV === 'production';

async function sendWebPushToAdmins(topic, ctx) {
  topic = await ctx.app.service(services.topics).find({ query: { uuid: topic.uuid, fetchPlan: 'creator' } });
  topic = topic.data || [];
  topic = topic[0];
  if (!topic) return;

  const webPushData = {
    payload: {
      title: `App: New topic creted by ${topic.creator.firstName}`,
      body: 'Title:' + topic.title + '\nDescription: ' + topic.description,
      url: `${ctx.app.get('protocol')}://${ctx.app.get('host')}/topics/${topic.uuid}`
    }
  };

  const params = {
    route: {
      entity: 'users',
      exclude: [topic.creator.uuid],
      val: 'admins'
    }
  }

  ctx.app.service(services.webPushNotify).create(webPushData, params);
}

async function sendEmailToAdmins(topic, ctx) {
  let email;
  let emailAccountTemplatesPath = path.join(ctx.app.get('serverDir'), 'email-templates', 'topic');
  let templatePath = path.join(emailAccountTemplatesPath, 'new-topic-created.pug');
  let compiledHTML;

  topic = await ctx.app.service(services.topics).find({ query: { uuid: topic.uuid, fetchPlan: 'creator' } });
  topic = topic.data || [];
  topic = topic[0];
  if (!topic) return ctx;
  // TODO: Have roles to get all Admins, rather than hard-coding a particular email id.
  let admins = await ctx.app.service(services.users).find({ query: { email: ctx.app.get('appAdminEmail') } });
  admins = admins.data || [];

  let port = ((ctx.app.get('httpsPort') === '443') || (ctx.app.get('httpsPort') === undefined) || isProd) ? '' : ':' + ctx.app.get('httpsPort')
  let host = (ctx.app.get('host') === undefined) ? 'localhost' : ctx.app.get('host')
  let protocol = (ctx.app.get('protocol') === undefined) ? 'http' : ctx.app.get('protocol')
  let logoLink = `${protocol}://${host}${port}/logo.png`

  admins.forEach(async (admin) => {
    compiledHTML = pug.compileFile(templatePath)({
      logo: logoLink,
      websiteName: ctx.app.get('websiteName'),
      name: admin.displayName.trim(),  // Admin name
      creator: topic.creator.displayName.trim(),   // Topic creator's name
      creatorProfileLink: `${ctx.app.get('protocol')}://${ctx.app.get('host')}/topics/users/${topic.creator.uuid}`,
      topicLink: `${ctx.app.get('protocol')}://${ctx.app.get('host')}/topics/${topic.uuid}`,
      topicTitle: topic.title,
      topicDescription: topic.description
    });

    email = {
      from: ctx.app.get('doNotReplyEmail') || process.env.GMAIL,
      to: admin.email,
      subject: `${ctx.app.get('websiteName')}: New topic created.`,
      html: compiledHTML
    };

    try {
      // Send email.
      await ctx.app.service(services.emails).create(email);
    } catch (err) {
      console.log('Error sending email notification about topic creation. Error: ', err)
    }
  });
}

module.exports = {
  sendWebPushToAdmins,
  sendEmailToAdmins
}