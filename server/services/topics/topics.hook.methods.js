const srvc = require('../definitions');

const getTopicResolvers = () => ctx => {
  ctx.params = ctx.params || {};
  const { latestRecords, fetchPlan, provider } = ctx.params;
  let { resolvers, } = ctx.params;
  const topicMessagesSrvc = ctx.app.service(srvc.topicMessages);
  const userSrvc = ctx.app.service(srvc.users);
  resolvers = resolvers || {};

  return {
    joins: {
      creator: $select => async topic => topic.creator = resolvers.creator && (await userSrvc.find({
        query: { uuid: topic.creatorUUID, $select: $select },
        paginate: false,
        provider
      }))[0] || undefined,

      messages: () => async topic => topic.messages = resolvers.messages && await topicMessagesSrvc.find({
        query: {
          topicUUID: topic.uuid,
          latestRecords,
          fetchPlan
        },
        provider
      }) || undefined
    }
  };
}

/**
 * This hook method set the ctx.data correctly for star, unstar, subscribe and unsubscribe logged-in user to given topic.
 * If params.query.operation is not there or not matching, it will let the normal patch do its job.
 */
const beforeStarSubscribeTopic = () => async ctx => {
  const { service, params = {}, data = {} } = ctx;
  const { query = {}, user = {} } = params;

  if (!query.uuid || !data.operation) return ctx;

  let topic = await service.find(params) || {};
  topic = topic.data && topic.data[0] || {};
  if (!topic) return ctx;

  switch (data.operation.toLowerCase()) {
    case 'star':
      topic.staredByUsersUUIDs = topic.staredByUsersUUIDs || {};
      topic.staredByUsersUUIDs[user.uuid] = true;

      ctx.data = { staredByUsersUUIDs: topic.staredByUsersUUIDs };
      ctx.params.query = { uuid: topic.uuid };
      break;
    case 'unstar':
      topic.staredByUsersUUIDs = topic.staredByUsersUUIDs || {};
      delete topic.staredByUsersUUIDs[user.uuid];

      ctx.data = { staredByUsersUUIDs: topic.staredByUsersUUIDs };
      ctx.params.query = { uuid: topic.uuid };
      break;
    case 'subscribe':
      break;
    case 'unsubscribe':
      break;
  }
  return ctx;
}

/**
 * This hook method will star, unstar, subscribe and unsubscribe the given topic from user record.
 */
const afterStarSubscribeTopic = () => async ctx => {
  const { app, service, params = {}, data = {} } = ctx;
  const { query = {}, user = {} } = params;

  if (!query.uuid || !data.operation) return ctx;

  let topic = await service.find(params) || {};
  topic = topic.data && topic.data[0] || {};
  if (!topic) return ctx;

  switch (data.operation.toLowerCase()) {
    case 'star':
      user.staredTopicsUUIDs = user.staredTopicsUUIDs || {};
      user.staredTopicsUUIDs[topic.uuid] = true;
      await app.service(srvc.users).patch(user._id, { staredTopicsUUIDs: user.staredTopicsUUIDs });
      break;
    case 'unstar':
      user.staredTopicsUUIDs = user.staredTopicsUUIDs || {};
      delete user.staredTopicsUUIDs[topic.uuid];
      await app.service(srvc.users).patch(user._id, { staredTopicsUUIDs: user.staredTopicsUUIDs });
      break;
    case 'subscribe':
      break;
    case 'unsubscribe':
      break;
  }
  return ctx;
}

module.exports = {
  getTopicResolvers,
  beforeStarSubscribeTopic,
  afterStarSubscribeTopic
};