// Initializes the `topics` service on path `/api/v1/topics`
const createService = require('feathers-mongoose');
const createModel = require('../../models/topics.model');
const hooks = require('./topics.hooks');
const registerEvents = require('./topics.events');
const services = require('../definitions');

module.exports = function (app) {
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    name: 'topics',
    Model,
    paginate
  };

  // Initialize our service with any options it requires
  app.use(services.topics, createService(options));

  // Get our initialized service so that we can register hooks and filters
  const service = app.service(services.topics);

  service.hooks(hooks);
  registerEvents(service);
};
