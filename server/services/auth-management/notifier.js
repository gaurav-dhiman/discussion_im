const isProd = process.env.NODE_ENV === 'production';
const path = require('path');
const pug = require('pug');
const services = require('../definitions');
const emailTypes = require('./notification-email-types');

module.exports = function (app) {

  const returnEmail = app.get('complaintEmail') || process.env.COMPLAINT_EMAIL

  function getLogoLink() {
    var port = ((app.get('httpsPort') === '443') || (app.get('httpsPort') === undefined) || isProd) ? '' : ':' + app.get('httpsPort')
    var host = (app.get('host') === undefined) ? 'localhost' : app.get('host')
    var protocol = (app.get('protocol') === undefined) ? 'http' : app.get('protocol')
    return `${protocol}://${host}${port}/logo.png`
  }

  function getLink(type, hash) {
    var port = ((app.get('httpsPort') === '443') || (app.get('httpsPort') === undefined) || isProd) ? '' : ':' + app.get('httpsPort')
    var host = (app.get('host') === undefined) ? 'localhost' : app.get('host')
    var protocol = (app.get('protocol') === undefined) ? 'http' : app.get('protocol')
    return `${protocol}://${host}${port}/identity/${type}/${hash}`
  }

  function sendEmail(email) {
    return app.service(services.emails).create(email).then((result) => { }).catch(err => {
      console.log('Error sending email', err)
    })
  }

  return {
    notifier: function (type, user, notifierOptions) {
      var hashLink;
      var email;
      var emailAccountTemplatesPath = path.join(app.get('serverDir'), 'email-templates', 'auth');
      var templatePath;
      var compiledHTML;
      switch (type) {
        case emailTypes.SEND_VERIFY_ACCOUNT: // send an email with link for verifying user's email address
          hashLink = getLink('verify', user.verifyToken);
          templatePath = path.join(emailAccountTemplatesPath, 'verify-account.pug');

          compiledHTML = pug.compileFile(templatePath)({
            logo: getLogoLink(),
            name: user.firstName || user.email,
            hashLink,
            returnEmail
          });

          email = {
            from: app.get('doNotReplyEmail') || process.env.GMAIL,
            to: user.email,
            subject:app.get('websiteName') + ': Verify Details',
            html: compiledHTML
          };

          return sendEmail(email);
        case emailTypes.VERIFY_ACCOUNT: // inform user through email, that identity (email) is now verified.
          templatePath = path.join(emailAccountTemplatesPath, 'account-verified.pug');

          compiledHTML = pug.compileFile(templatePath)({
            logo: getLogoLink(),
            name: user.firstName || user.email,
            returnEmail
          });

          email = {
            from: app.get('doNotReplyEmail') || process.env.GMAIL,
            to: user.email,
            subject: app.get('websiteName') + ': Thank you, your email has been verified',
            html: compiledHTML
          };

          return sendEmail(email);
        case emailTypes.SEND_RESET_PASSWORD: // send email to user to reset password.
          hashLink = getLink('reset', user.resetToken);
          templatePath = path.join(emailAccountTemplatesPath, 'reset-password.pug');

          compiledHTML = pug.compileFile(templatePath)({
            logo: getLogoLink(),
            name: user.firstName || user.email,
            hashLink,
            returnEmail
          });

          email = {
            from: app.get('doNotReplyEmail') || process.env.GMAIL,
            to: user.email,
            subject: app.get('websiteName') + ': Reset Password',
            html: compiledHTML
          };

          return sendEmail(email);
        case emailTypes.RESET_PASSWORD: // inform user through email, that password has been reset.
          templatePath = path.join(emailAccountTemplatesPath, 'password-got-reset.pug');

          compiledHTML = pug.compileFile(templatePath)({
            logo: getLogoLink(),
            name: user.firstName || user.email,
            returnEmail
          });

          email = {
            from: app.get('doNotReplyEmail') || process.env.GMAIL,
            to: user.email,
            subject: app.get('websiteName') + ': Your password has been reset',
            html: compiledHTML
          };

          return sendEmail(email);
        case emailTypes.PASSWORD_CHANGED: // inform user through email that the password has been changed.
          templatePath = path.join(emailAccountTemplatesPath, 'password-changed.pug');

          compiledHTML = pug.compileFile(templatePath)({
            logo: getLogoLink(),
            name: user.firstName || user.email,
            returnEmail
          });

          email = {
            from: app.get('doNotReplyEmail') || process.env.GMAIL,
            to: user.email,
            subject: app.get('websiteName') + ': Your password was changed',
            html: compiledHTML
          };

          return sendEmail(email);
        case emailTypes.IDENTITY_CHANGE:  // inform user through email that an identity (email) has been changed on profile.
          hashLink = getLink('verify-changes', user.verifyToken);
          templatePath = path.join(emailAccountTemplatesPath, 'identity-change.pug');

          compiledHTML = pug.compileFile(templatePath)({
            logo: getLogoLink(),
            name: user.firstName || user.email,
            hashLink,
            returnEmail,
            changes: user.verifyChanges
          });

          email = {
            from: app.get('doNotReplyEmail') || process.env.GMAIL,
            to: user.email,
            subject: app.get('websiteName') + ': Your account was changed. Please verify the changes',
            html: compiledHTML
          };

          return sendEmail(email);
        default:
          break
      }
    }
  }
}