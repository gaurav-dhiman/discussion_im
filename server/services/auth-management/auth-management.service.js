// Initializes the `authmanagement` service on path `/api/v1/auth-management`
const authManagement = require('feathers-authentication-management');
const hooks = require('./auth-management.hooks');
const notifier = require('./notifier');
const services = require('../definitions');

module.exports = function (app) {
  // Initialize our service with any options it requires
  let options = notifier(app);
  options = {
    path: services.authManagement,
    service: services.users,
    ...options
  }
  app.configure(authManagement(options));

  // Get our initialized service so that we can register hooks
  const service = app.service(services.authManagement);
  service.timeout = 10000;

  service.hooks(hooks);
};
