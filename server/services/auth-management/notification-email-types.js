// The values in this object needs to e as it is as these are used by feathersjs/auth-management plugin, so don't change them.
module.exports = {
  SEND_VERIFY_ACCOUNT: 'resendVerifySignup', // Send an email with link to verify identity (email)
  VERIFY_ACCOUNT: 'verifySignup',  // Send an email confirming the successful verification of identity (email)
  SEND_RESET_PASSWORD: 'sendResetPwd',    // Send an email with resend password link
  RESET_PASSWORD: 'resetPwd',     // Send an email confirming the password has been reset (Old forgotton, new password set)
  PASSWORD_CHANGED: 'passwordChange',     // Send an email confirming the password has been changes (old and new password given to make change)
  IDENTITY_CHANGE: 'identityChange',      // Send an email confirming the change of identity (email)
}