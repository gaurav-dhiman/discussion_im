const { sendWebPushToTopicSubscribers, updateTopicAttributes, updateTopicMessageRepliesCount } = require('./topic-messages.event.methods');

const events = [
  {
    name: 'created',
    handler: async (topicMessage, ctx) => {
      updateTopicAttributes(topicMessage, ctx);
      updateTopicMessageRepliesCount(topicMessage, ctx);
      sendWebPushToTopicSubscribers(topicMessage, ctx);
      console.log('topicMessage created: ', topicMessage);
    },
    publisher: (topicMessage, ctx) => {
      return ctx.app.channel('anonymous', 'authenticated');
    }
  },
  {
    name: 'updated',
    handler: (topicMessage, ctx) => {
      console.log('topicMessage updated: ', topicMessage);
    },
    publisher: (topicMessage, ctx) => {
      return ctx.app.channel('anonymous', 'authenticated');
    }
  },
  {
    name: 'patched',
    handler: (topicMessage, ctx) => {
      console.log('topicMessage patched: ', topicMessage);
    },
    publisher: (topicMessage, ctx) => {
      return ctx.app.channel('anonymous', 'authenticated');
    }
  },
  {
    name: 'removed',
    handler: (topicMessage, ctx) => {
      console.log('topicMessage removed: ', topicMessage);
    },
    publisher: (topicMessage, ctx) => {
      return ctx.app.channel('anonymous', 'authenticated');
    }
  }
];

function registerEvents(service) {
  events.forEach(event => {
    service.on(event.name, event.handler);  // Use event.handler for logging etc.
    service.publish(event.name, event.publisher); // Use event.publisher() for publishing to channels for real-time socket communication.
  });
}

module.exports = registerEvents;
