const services = require('../definitions');

async function sendWebPushToTopicSubscribers(topicMessage, ctx) {
  let topicMsg = await ctx.app.service(services.topicMessages).find({ query: { uuid: topicMessage.uuid, fetchPlan: 'topic,sender' } });
  topicMsg = topicMsg.data || [];
  topicMsg = topicMsg[0];
  if (!topicMsg) return;

  const webPushData = {
    payload: {
      title: 'New Message in ' + topicMsg.topic.title,
      body: topicMsg.sender.firstName + ': ' + topicMessage.text,
      url: `${ctx.app.get('protocol')}://${ctx.app.get('host')}/topics/${topicMsg.topic.uuid}/messages/${topicMessage.uuid}`
    }
  };

  const params = {
    route: {
      entity: 'topics',
      exclude: [topicMsg.sender.uuid],
      val: topicMessage.topicUUID
    }
  }

  ctx.app.service(services.webPushNotify).create(webPushData, params);
}

async function updateTopicAttributes(topicMessage, ctx) {
  const { topicUUID, senderUUID } = topicMessage;
  if (!topicUUID) return ctx;
  let topic = await ctx.app.service(services.topics).find({ query: { uuid: topicUUID } });
  topic = topic.data || [];
  topic = topic[0];

  if (!topic) return ctx;

  // Update topic's participant count
  topic.postCountByUsersUUIDs = topic.postCountByUsersUUIDs || {};
  topic.postCountByUsersUUIDs[senderUUID] = (topic.postCountByUsersUUIDs[senderUUID] || 0) + 1;

  // Update total count of messages in topic
  topic.totalMessageCount = (topic.totalMessageCount || 0) + 1;

  // Update last message timestamp
  topic.lastMessageTimestamp = topicMessage.createdAt;

  await ctx.app.service(services.topics).patch(topic._id, {
    totalMessageCount: topic.totalMessageCount,
    postCountByUsersUUIDs: topic.postCountByUsersUUIDs,
    lastMessageTimestamp: topic.lastMessageTimestamp
  });
  return ctx;
}

async function updateTopicMessageRepliesCount(topicMessage, ctx) {
  const { parentTopicMessageUUID } = topicMessage;
  if (!parentTopicMessageUUID) return ctx;

  topicMessage = await ctx.app.service(services.topicMessages).find({ query: { uuid: parentTopicMessageUUID } });
  topicMessage = topicMessage.data || [];
  topicMessage = topicMessage[0];

  if (!topicMessage) return ctx;

  topicMessage.totalRepliesCount = (topicMessage.totalRepliesCount || 0) + 1;

  await ctx.app.service(services.topicMessages).patch(topicMessage._id, { totalRepliesCount: topicMessage.totalRepliesCount });
  return ctx;
}

module.exports = {
  sendWebPushToTopicSubscribers,
  updateTopicAttributes,
  updateTopicMessageRepliesCount
}