const services = require('../definitions');

function getTopicMessageResolvers(ctx) {
  const { latestRecords, fetchPlan } = ctx.params;
  let { resolvers, provider } = ctx.params;
  resolvers = resolvers || {};

  const topicMessagesSrv = ctx.service;
  const topicSrv = ctx.app.service(services.topics);
  const userSrv = ctx.app.service(services.users);

  return {
    joins: {
      sender: $select => async message => message.sender = resolvers.sender && (await userSrv.find({
        query: {
          uuid: message.senderUUID,
          $select: $select,
          latestRecords,
          fetchPlan
        },
        paginate: false,
        provider
      }))[0] || undefined,

      topic: $select => async message => message.topic = resolvers.topic && (await topicSrv.find({
        query: {
          uuid: message.topicUUID,
          $select: $select,
          latestRecords,
          fetchPlan
        },
        paginate: false,
        provider
      }))[0] || undefined,

      replies: $select => async message => message.replies = resolvers.replies && await topicMessagesSrv.find({
        query: {
          parentTopicMessageUUID: message.uuid,
          $select: $select,
          latestRecords,
          fetchPlan
        },
        provider
      }) || undefined,
    }
  };
}

/**
 * This hook method set the ctx.data correctly for pin and unpin logged-in user to given topic message.
 * If params.query.operation is not there or not matching, it will let the normal patch do its job.
 */
const beforePinTopicMessage = () => async ctx => {
  const { service, params = {}, data = {} } = ctx;
  const { query = {}, user = {} } = params;

  if (!query.uuid || !data.operation) return ctx;

  let topicMessage = await service.find(params) || {};
  topicMessage = topicMessage.data && topicMessage.data[0] || {};
  if (!topicMessage) return ctx;

  switch (data.operation.toLowerCase()) {
    case 'pin':
      topicMessage.pinnedByUsersUUID = topicMessage.pinnedByUsersUUID || {};
      topicMessage.pinnedByUsersUUID[user.uuid] = true;

      ctx.data = { pinnedByUsersUUID: topicMessage.pinnedByUsersUUID };
      ctx.params.query = { uuid: topicMessage.uuid };
      break;
    case 'unpin':
      topicMessage.pinnedByUsersUUID = topicMessage.pinnedByUsersUUID || {};
      delete topicMessage.pinnedByUsersUUID[user.uuid];

      ctx.data = { pinnedByUsersUUID: topicMessage.pinnedByUsersUUID };
      ctx.params.query = { uuid: topicMessage.uuid };
      break;
    case 'like':
      topicMessage.likedByUsersUUID = topicMessage.likedByUsersUUID || {};
      topicMessage.likedByUsersUUID[user.uuid] = true;

      ctx.data = { likedByUsersUUID: topicMessage.likedByUsersUUID };
      ctx.params.query = { uuid: topicMessage.uuid };
      break;
    case 'unlike':
      topicMessage.likedByUsersUUID = topicMessage.likedByUsersUUID || {};
      delete topicMessage.likedByUsersUUID[user.uuid];

      ctx.data = { likedByUsersUUID: topicMessage.likedByUsersUUID };
      ctx.params.query = { uuid: topicMessage.uuid };
      break;
  }
  return ctx;
}

/**
 * This hook method will star, unstar, subscribe and unsubscribe the given topic from user record.
 */
const afterPinTopicMessage = () => async ctx => {
  const { app, service, params = {}, data = {} } = ctx;
  const { query = {}, user = {} } = params;

  if (!query.uuid || !data.operation) return ctx;

  let topicMessage = await service.find(params) || {};
  topicMessage = topicMessage.data && topicMessage.data[0] || {};
  if (!topicMessage) return ctx;

  switch (data.operation.toLowerCase()) {
    case 'star':
      user.pinnedTopicMessagesUUIDs = user.pinnedTopicMessagesUUIDs || {};
      user.pinnedTopicMessagesUUIDs[topicMessage.uuid] = true;
      await app.service(srvc.users).patch(user._id, { pinnedTopicMessagesUUIDs: user.pinnedTopicMessagesUUIDs });
      break;
    case 'unstar':
      user.pinnedTopicMessagesUUIDs = user.pinnedTopicMessagesUUIDs || {};
      delete user.pinnedTopicMessagesUUIDs[topicMessage.uuid];
      await app.service(srvc.users).patch(user._id, { pinnedTopicMessagesUUIDs: user.pinnedTopicMessagesUUIDs });
      break;
    case 'like':
      user.likedTopicMessagesUUIDs = user.likedTopicMessagesUUIDs || {};
      user.likedTopicMessagesUUIDs[topicMessage.uuid] = true;
      await app.service(srvc.users).patch(user._id, { likedTopicMessagesUUIDs: user.likedTopicMessagesUUIDs });
      break;
    case 'unlike':
      user.likedTopicMessagesUUIDs = user.likedTopicMessagesUUIDs || {};
      delete user.likedTopicMessagesUUIDs[topicMessage.uuid];
      await app.service(srvc.users).patch(user._id, { likedTopicMessagesUUIDs: user.likedTopicMessagesUUIDs });
      break;
  }
  return ctx;
}

module.exports = {
  getTopicMessageResolvers,
  beforePinTopicMessage,
  afterPinTopicMessage
};