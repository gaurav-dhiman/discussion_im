// Initializes the `topic-messages` service on path `/api/v1/topic-messages`
const createService = require('feathers-mongoose');
const createModel = require('../../models/topic-messages.model');
const hooks = require('./topic-messages.hooks');
const registerEvents = require('./topic-messages.events');
const services = require('../definitions');

module.exports = function (app) {
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    name: 'topic-messages',
    Model,
    paginate
  };

  // Initialize our service with any options it requires
  app.use(services.topicMessages, createService(options));

  // Get our initialized service so that we can register hooks and filters
  const service = app.service(services.topicMessages);

  service.hooks(hooks);
  registerEvents(service);
};
