const {
  processLatestRecordsQueryParam,
  parseFetchPlanQueryParam,
  addUUID,
  mapToLoggedInUserUUID
} = require('../common.hook.methods');
const { authenticate } = require('@feathersjs/authentication').hooks;
const { protect } = require('@feathersjs/authentication-local').hooks;
const { getTopicMessageResolvers, beforePinTopicMessage, afterPinTopicMessage } = require('./topic-messages.hook.methods');
const { fastJoin } = require('feathers-hooks-common');

module.exports = {
  before: {
    all: [],
    find: [
      processLatestRecordsQueryParam(),
      parseFetchPlanQueryParam()
    ],
    get: [],
    create: [
      authenticate('jwt'),
      mapToLoggedInUserUUID('senderUUID'),
      addUUID()
    ],
    update: [authenticate('jwt')],
    patch: [
      authenticate('jwt'),
      beforePinTopicMessage()
    ],
    remove: [authenticate('jwt')]
  },

  after: {
    all: [
      protect('_id'),
      fastJoin(getTopicMessageResolvers)
    ],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [
      afterPinTopicMessage()
    ],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
