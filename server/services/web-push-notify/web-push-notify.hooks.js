const { blockMethods } = require('../common.hook.methods');
const { sendWebPush } = require('./web-push-notify.hook.methods');

module.exports = {
  before: {
    all: [
      blockMethods([
        'find',
        'get',
        'update',
        'patch',
        'remove',
      ])
    ],
    find: [],
    get: [],
    create: [
      sendWebPush()
    ],
    update: [],
    patch: [],
    remove: []
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
