const crypto = require('crypto');
const webpush = require('web-push');
const services = require('../definitions');

const sendToDevice = async (ctx, subscription, data) => {
  const { publicKey, privateKey } = ctx.app.get('vapidKeys') || {};
  const options = {
    TTL: 24 * 60 * 60, // 1 day
    vapidDetails: {
      //email url is necessary for a push service to notify you if something is wrong
      subject: 'mailto:' + ctx.app.get('appAdminEmail'),
      publicKey,
      privateKey
    }
  }

  try {
    await webpush.sendNotification(subscription, JSON.stringify(data), options);
  } catch (err) {
    console.log('Error in sending web push message: ', err);
    const uuid = crypto.createHash('sha256').update(JSON.stringify(subscription || '')).digest('hex');
    await ctx.app.service(services.webPushSubscribe).remove(null, { query: { uuid } });
  }
};

const sendToUser = async (ctx, userUUID, data) => {
  let subscriptions = await ctx.app.service(services.webPushSubscribe).find({ query: { userUUID } });
  subscriptions = subscriptions.data || [];

  subscriptions.forEach(async (obj) => {
    await sendToDevice(ctx, obj.subscription, data);
  });
};

const sendToAllUsers = async (ctx, exclude=[], data) => {
  let subscriptions = await ctx.app.service(services.webPushSubscribe).find();
  subscriptions = subscriptions.data || [];

  subscriptions.map(async (obj) => {
    if (exclude.includes(obj.userUUID)) return; // Dont send notifications to users in exclude array.
    await sendToDevice(ctx, obj.subscription, data);
  });
};

const sendToAllAdmins = async (ctx, exclude=[], data) => {
  // TODO: Have roles to get all Admins, rather than hard-coding a particular email id.
  let users = await ctx.app.service(services.users).find({ query: { email: ctx.app.get('appAdminEmail') } });
  users = users.data || [];
  let subscriptions = [];

  // collect all subscription objecs for all admin users.
  users.map(async (user) => {
    if (exclude.includes(user.uuid)) return; // Dont send notifications to users in exclude array.
    userSubscriptions = await ctx.app.service(services.webPushSubscribe).find({ query: { userUUID: user.uuid } });
    userSubscriptions = userSubscriptions.data || [];
    userSubscriptions.forEach((us) => subscriptions.push(us));
  });

  subscriptionIds = {};

  // send web push notification for each subscription object once.
  subscriptions.map(async (obj) => {
    if (exclude.includes(obj.userUUID)) return; // Dont send notifications to users in exclude array.
    if (!subscriptionIds[obj.subscriptionId]) {
      subscriptionIds[obj.subscriptionId] = true;
      await sendToDevice(ctx, obj.subscription, data);
    }
  });
};

const sendToTopicSubscribers = async (ctx, topicUUID, exclude = [], data) => {
  let subscriptions = await ctx.app.service(services.webPushSubscribe).find({ query: { 'subscribeTo.uuid': topicUUID } });
  subscriptions = subscriptions.data || [];

  subscriptions.map(async (obj) => {
    if (exclude.includes(obj.userUUID)) return; // Dont send notifications to users in exclude array.
    await sendToDevice(ctx, obj.subscription, data);
  });
};

module.exports = {
  sendToDevice,
  sendToUser,
  sendToAllUsers,
  sendToAllAdmins,
  sendToTopicSubscribers
}