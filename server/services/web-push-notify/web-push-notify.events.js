const events = [
  {
    name: 'created',
    handler: (topic, ctx) => {
      console.log('web push notification created: ', topic);
    }
  },
  {
    name: 'updated',
    handler: (topic, ctx) => {
      console.log('web push notification updated: ', topic);
    }
  },
  {
    name: 'patched',
    handler: (topic, ctx) => {
      console.log('web push notification patched: ', topic);
    }
  },
  {
    name: 'removed',
    handler: (topic, ctx) => {
      console.log('web push notification removed: ', topic);
    }
  }
];

function registerEvents(service) {
  events.forEach(event => {
    service.on(event.name, event.handler);
  });
}

module.exports = registerEvents;
