// Initializes the `web-push-notify` service on path `/api/v1/web-push/notify/:entity`
const createService = require('./web-push-notify.class');
const hooks = require('./web-push-notify.hooks');
const registerEvents = require('./web-push-notify.events');
const services = require('../definitions');

module.exports = function (app) {
  const paginate = app.get('paginate');

  const options = {
    paginate
  };

  // Initialize our service with any options it requires
  app.use(services.webPushNotify, createService(options));

  // Get our initialized service so that we can register hooks and filters
  const service = app.service(services.webPushNotify);

  service.hooks(hooks);
  registerEvents(service);
};
