const notifier = require('./web-push-notify.notifier');
const errors = require('@feathersjs/errors');

const sendWebPush = () => async (ctx) => {
  switch (ctx.params.route.entity) {
    case 'device':
      await notifier.sendToDevice(ctx, ctx.data.subscription, ctx.data.payload);
      break;
    case 'users':
      if (ctx.params.route.val === 'all')
        await notifier.sendToAllUsers(ctx, ctx.params.route.exclude, ctx.data.payload);
      else if (ctx.params.route.val === 'admins')
        await notifier.sendToAllAdmins(ctx, ctx.params.route.exclude, ctx.data.payload);
      else
        await notifier.sendToUser(ctx, ctx.params.route.val, ctx.data.payload);
      break;
    case 'topics':
      await notifier.sendToTopicSubscribers(ctx, ctx.params.route.val, ctx.params.route.exclude, ctx.data.payload);
      break;
    default:
      throw new errors.BadRequest("Invalid 'entity' param given for 'notify' action under this service method.");
  }
  ctx.result = 'WebPush message has been triggered.';
  return ctx;
}

module.exports = {
  sendWebPush
};