const next = require('next');
const routes = require('./routes');

const dev = process.env.NODE_ENV !== 'production';
const nextApp = next({ dir: './client', dev });
const handle = routes.getRequestHandler(nextApp);

const feathersServices = ['/api/', '/auth/'];
const isFeathersService = path =>
  feathersServices.some(item => path.indexOf(item) > -1);

module.exports = {
  nextApp,
  handle,
  isFeathersService
};
