const assert = require('assert');
const app = require('../../server/app');

describe('\'topic-messages\' service', () => {
  it('registered the service', () => {
    const service = app.service('api/v1/topic-messages');

    assert.ok(service, 'Registered the service');
  });
});
