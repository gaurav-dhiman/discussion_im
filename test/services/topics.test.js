const assert = require('assert');
const app = require('../../server/app');

describe('\'topics\' service', () => {
  it('registered the service', () => {
    const service = app.service('api/v1/topics');

    assert.ok(service, 'Registered the service');
  });
});
