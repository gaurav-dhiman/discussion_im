const assert = require('assert');
const app = require('../../server/app');

describe('\'web-push-subscribe\' service', () => {
  it('registered the service', () => {
    const service = app.service('api/v1/web-push-subscribe');

    assert.ok(service, 'Registered the service');
  });
});
