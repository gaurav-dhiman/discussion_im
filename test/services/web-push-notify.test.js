const assert = require('assert');
const app = require('../../server/app');

describe('\'web-push-notify\' service', () => {
  it('registered the service', () => {
    const service = app.service('api/v1/web-push-notify');

    assert.ok(service, 'Registered the service');
  });
});
