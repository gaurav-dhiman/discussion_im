const assert = require('assert');
const app = require('../../server/app');

describe('\'vapid-keys\' service', () => {
  it('registered the service', () => {
    const service = app.service('api/v1/vapid-keys');

    assert.ok(service, 'Registered the service');
  });
});
