const assert = require('assert');
const app = require('../../server/app');

describe('\'profile-image\' service', () => {
  it('registered the service', () => {
    const service = app.service('api/v1/profile-image');

    assert.ok(service, 'Registered the service');
  });
});
