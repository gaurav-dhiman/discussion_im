# Discussion_im

> Realtime Discussion Forums (PWA) for Immegration Discussions, like trackitt.

## About

This project uses [Feathers](http://feathersjs.com). An open source web framework for building modern real-time applications.

## Getting Started

Getting up and running is as easy as 1, 2, 3.

1. Make sure you have [NodeJS](https://nodejs.org/) and [npm](https://www.npmjs.com/) installed.
2. Install your dependencies

    ```
    cd path/to/discussion_im; npm install
    ```

3. Start your app

    ```
    npm start
    ```

## Testing

Simply run `npm test` and all your tests in the `test/` directory will be run.

## Scaffolding

Feathers has a powerful command line interface. Here are a few things it can do:

```
$ npm install -g @feathersjs/cli          # Install Feathers CLI

$ feathers generate service               # Generate a new Service
$ feathers generate hook                  # Generate a new Hook
$ feathers generate model                 # Generate a new Model
$ feathers help                           # Show all commands
```

## Help

For more information on all the things you can do with Feathers visit [docs.feathersjs.com](http://docs.feathersjs.com).

## Posible Page URL Formats

/topics --> show only topics - dont show messages and details
/topics/123 ---> show messages of topics
/topics/123/messages ---> show messages of topic
<!-- /topics/123/messages/star ---> show stared messages of topic -->
/topics/123/messages/123 --> show messages of topic and specific thread
/topics/123/details --> show messages and details of topic
/topics/123/users --> show messages of topic and list of users in topic
/topics/123/users/123 --> show messages of topic and details of users

## Changelog

__0.1.0__

- Initial release

## License

Copyright (c) 2016

Licensed under the [MIT license](LICENSE).
